/* globals app, Survey, Product */
/* eslint-env jest */
const { createId, getUserToken, destroyEntitiesByIds } = require('../utils')
const s3Utils = require('../../utils/s3Utils')
const { organizations, users } = require('../fixtures')

const organization = organizations[0]

const survey = {
  id: createId(),
  uniqueName: 'survey-2',
  name: 'Survey 2',
  title: 'Survey 2',
  owner: organization.id
}

describe('clone survey', () => {
  let token
  let spyCopyPhoto
  let createdSurveyIds = []
  let createdProductIds = []
  const { BUCKET_URL } = process.env

  beforeAll(() => {
    token = `Bearer ${getUserToken(users[0])}`
  })

  beforeEach(async () => {
    spyCopyPhoto = jest
      .spyOn(s3Utils, 'copyFile')
      .mockImplementation((_key, newKey) => `${BUCKET_URL}/${newKey}`)
  })

  afterEach(async () => {
    spyCopyPhoto.mockRestore()
    await destroyEntitiesByIds(Survey, createdSurveyIds)
    await destroyEntitiesByIds(Product, createdProductIds)
  })

  test('should change "name", "uniqueName" of the new survey while cloning the survey', async () => {
    const { id: createdId } = await Survey.create(survey).fetch()
    createdSurveyIds.push(createdId)

    const res = await app
      .post(`/survey/clone/${survey.id}`)
      .set('Authorization', token)
      .send({})
      .expect(200)

    expect(res.body).toHaveProperty('survey')

    const newSurveyId = res.body.survey
    const newSurvey = await Survey.findOne({ id: newSurveyId })

    expect(newSurvey.name).toBe('copy of Survey 2')
    expect(newSurvey.uniqueName).toMatch(/^copy-of-survey-2/)
  })

  test("should change reupload the product's photos while cloning the survey", async () => {
    const { id: createdSurveyId } = await Survey.create(survey).fetch()
    createdSurveyIds.push(createdSurveyId)
    await Product.create({
      id: createId(),
      name: 'product-1',
      surveys: [survey.id],
      photo:
        'https://s3-eu-west-1.amazonaws.com/flavorwiki-storage/abed14b2-1509-4ce3-86ed-1e4afe6a641f.png'
    })

    const res = await app
      .post(`/survey/clone/${survey.id}`)
      .set('Authorization', token)
      .send({})
      .expect(200)

    const newSurveyId = res.body.survey
    const newSurvey = await Survey.findOne({ id: newSurveyId }).populate(
      'products'
    )

    // copy photo should be called
    const originPhotoKey = spyCopyPhoto.mock.calls[0][0]
    const newPhotoKey = spyCopyPhoto.mock.calls[0][1]

    expect(originPhotoKey).toBe('abed14b2-1509-4ce3-86ed-1e4afe6a641f.png')
    // new photo url = copy-at-{{timestamp}}-of-origin-photo
    expect(newPhotoKey).toMatch(
      /copy-at-[0-9]+-of-abed14b2-1509-4ce3-86ed-1e4afe6a641f\.png/
    )

    // new photo should be point to the new photo
    expect(newSurvey.products[0].photo.endsWith(newPhotoKey)).toBe(true)
  })

  for (let originalState of ['active', 'draft', 'suspended']) {
    test(`should change state to draft on the new survey while cloning the survey that has "${originalState}" state`, async () => {
      const { id: createdSurveyId } = await Survey.create({
        ...survey,
        state: originalState
      }).fetch()
      createdSurveyIds.push(createdSurveyId)

      const res = await app
        .post(`/survey/clone/${survey.id}`)
        .set('Authorization', token)
        .send({})
        .expect(200)

      expect(res.body).toHaveProperty('survey')

      const newSurveyId = res.body.survey
      const newSurvey = await Survey.findOne({ id: newSurveyId }).populate(
        'products'
      )
      expect(newSurvey.state).toBe('draft')
    })
  }
})
