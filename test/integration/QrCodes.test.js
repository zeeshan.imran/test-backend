/* globals app, QrCode */
/* eslint-env jest */
require('dotenv')
const { createId, getUserToken } = require('../utils')
const s3Utils = require('../../utils/s3Utils')
const { organizations, users, surveys } = require('../fixtures')

const organization = organizations[0]

const qrCodes = [
  {
    id: createId(),
    owner: organization.id,
    name: 'Cereal bar survey',
    uniqueName: 'survey-1',
    targetType: 'survey',
    survey: surveys[0].uniqueName,
    qrCodePhoto:
      'https://test.amazonaws.com/flavorwiki-storage/qr-codes/organization-1/survey-1.png'
  },
  {
    id: createId(),
    owner: organization.id,
    name: 'Flavorwiki slack channel',
    uniqueName: 'flavor-wiki-slack',
    targetType: 'link',
    targetLink: 'https://flavor-wiki.slack.com',
    qrCodePhoto:
      'https://test.amazonaws.com/flavorwiki-storage/qr-codes/organization-1/flavor-wiki-slack.png',
    creator: users[0].id
  },
  {
    id: createId(),
    owner: organizations[1].id,
    name: 'Flavorwiki slack channel',
    uniqueName: 'flavor-wiki-slack-2',
    targetType: 'link',
    targetLink: 'https://flavor-wiki.slack.com',
    qrCodePhoto:
      'https://test.amazonaws.com/flavorwiki-storage/qr-codes/organization-2/flavor-wiki-slack-2.png',
    creator: users[0].id
  }
]

describe('Qr Code API', () => {
  let token
  let spyUploadPhoto
  let spyDeletePhoto

  beforeAll(() => {
    token = `Bearer ${getUserToken(users[0])}`
  })

  beforeEach(async () => {
    spyUploadPhoto = jest
      .spyOn(s3Utils, 'uploadFile')
      .mockImplementation(
        key => `https://test.amazonaws.com/flavorwiki-storage/${key}`
      )
    spyDeletePhoto = jest
      .spyOn(s3Utils, 'deleteFile')
      .mockImplementation(() => {})
  })

  afterEach(async () => {
    spyUploadPhoto.mockRestore()

    await QrCode.destroy({})
  })

  test("user can only see her/his organization's surveys", async () => {
    await QrCode.createEach(qrCodes).fetch()

    // user of organization-0 can see two qr-codes
    let res = await app
      .get(`/qrCode`)
      .set('Authorization', token)
      .send({})
      .expect(200)

    expect(res.body).toHaveLength(2)

    // user of organization-1 can NOT see two qr-codes of organization-0
    const user1Token = `Bearer ${getUserToken(users[1])}`
    res = await app
      .get(`/qrCode`)
      .set('Authorization', user1Token)
      .send({})
      .expect(200)

    expect(res.body).toHaveLength(1)
  })

  test('should have 1 qr code when deleting firstQrCode', async () => {
    await QrCode.createEach(qrCodes).fetch()

    await app
      .delete(`/qrCode/${qrCodes[0].id}`)
      .set('Authorization', token)
      .send()
      .expect(200)

    expect(await QrCode.count({ owner: organization.id })).toBe(1)
  })

  test('should delete the Qr Code photo when deleting the Qr Code', async () => {
    await QrCode.createEach(qrCodes).fetch()

    await app
      .delete(`/qrCode/${qrCodes[0].id}`)
      .set('Authorization', token)
      .send()
      .expect(200)

    expect(spyDeletePhoto).toHaveBeenCalledWith(
      'qr-codes/organization-1/survey-1.png'
    )
  })

  test('user should NOT update or delete Qr code of other organization', async () => {
    await QrCode.createEach(qrCodes).fetch()

    let res = await app
      .patch(`/qrCode/${qrCodes[2].id}`)
      .set('Authorization', token)
      .send({
        name: 'Flavorwiki repos',
        uniqueName: 'flavorwiki-repos',
        targetLink: 'https://git.flavorwiki.info'
      })
      .expect(400)

    expect(res.body.code).toBe('UNAUTHORIZED')

    res = await app
      .delete(`/qrCode/${qrCodes[2].id}`)
      .set('Authorization', token)
      .send()
      .expect(400)

    expect(res.body.code).toBe('UNAUTHORIZED')
  })

  test('should get qr code successfully', async () => {
    await QrCode.createEach(qrCodes).fetch()

    const res = await app
      .get(`/qrCode/${qrCodes[0].id}`)
      .set('Authorization', token)
      .send()
      .expect(200)

    expect(res.body).toMatchObject({
      name: 'Cereal bar survey',
      uniqueName: 'survey-1',
      targetType: 'survey'
    })
  })

  test('should update qr successfully', async () => {
    await QrCode.createEach(qrCodes).fetch()

    const res = await app
      .patch(`/qrCode/${qrCodes[1].id}`)
      .set('Authorization', token)
      .send({
        name: 'Flavorwiki repos',
        uniqueName: 'flavorwiki-repos',
        targetLink: 'https://git.flavorwiki.info'
      })
      .expect(200)

    expect(res.body).toMatchObject({
      name: 'Flavorwiki repos',
      uniqueName: 'flavorwiki-repos',
      qrCodePhoto:
        'https://test.amazonaws.com/flavorwiki-storage/qr-code/organization-1/flavorwiki-repos.png',
      targetLink: 'https://git.flavorwiki.info'
    })
  })

  test("should NOT update second qr's uniqueName to survey-1 on the first organization (duplicate uniqueName)", async () => {
    await QrCode.createEach(qrCodes).fetch()

    const res = await app
      .patch(`/qrCode/${qrCodes[1].id}`)
      .set('Authorization', token)
      .send({
        uniqueName: 'survey-1'
      })
      .expect(400)
    expect(res.body.code).toBe('E_QR_CODE_UNIQUE')
  })

  test("should update third qr's uniqueName to flavor-wiki-slack (duplicate uniqueName but different organization)", async () => {
    await QrCode.createEach(qrCodes).fetch()

    const res = await app
      .patch(`/qrCode/${qrCodes[1].id}`)
      .set('Authorization', token)
      .send({
        uniqueName: 'flavor-wiki-slack'
      })
      .expect(200)

    expect(res.body).toMatchObject({
      owner: organization.id,
      uniqueName: 'flavor-wiki-slack'
    })
  })

  test('should create qr successfully', async () => {
    await QrCode.createEach(qrCodes).fetch()

    await app
      .post(`/qrCode`)
      .set('Authorization', token)
      .send({
        name: 'Flavorwiki repos',
        uniqueName: 'flavorwiki-repos',
        targetType: 'link',
        targetLink: 'https://git.flavorwiki.info'
      })
      .expect(200)

    const allQrCodes = await QrCode.find()
    expect(allQrCodes).toHaveLength(4)

    expect(allQrCodes[3]).toMatchObject({
      name: 'Flavorwiki repos',
      uniqueName: 'flavorwiki-repos',
      qrCodePhoto:
        'https://test.amazonaws.com/flavorwiki-storage/qr-code/organization-1/flavorwiki-repos.png',
      targetLink: 'https://git.flavorwiki.info'
    })
  })

  test('should upload the Qr Code when creating a new survey', async () => {
    await QrCode.createEach(qrCodes).fetch()

    await app
      .post(`/qrCode`)
      .set('Authorization', token)
      .send({
        name: 'Flavorwiki repos',
        uniqueName: 'flavorwiki-repos',
        targetType: 'link',
        targetLink: 'https://git.flavorwiki.info'
      })
      .expect(200)

    expect(spyUploadPhoto).toHaveBeenCalled()
    expect(spyUploadPhoto.mock.calls[0][0]).toBe(
      'qr-code/organization-1/flavorwiki-repos.png'
    )
  })

  test('should NOT create uniqueName to survey-1 on the first organization successfully (duplicate uniqueName)', async () => {
    await QrCode.createEach(qrCodes).fetch()

    const res = await app
      .post(`/qrCode`)
      .set('Authorization', token)
      .send({
        name: 'Cereal bar survey (duplicate)',
        uniqueName: 'survey-1',
        targetType: 'link',
        targetLink: 'https://git.flavorwiki.info'
      })
      // added error
      .expect(400)

    expect(res.body.code).toBe('E_QR_CODE_UNIQUE')

    const allQrCodes = await QrCode.find()
    expect(allQrCodes).toHaveLength(3)
  })

  test('should delete the old Qr Code then upload the new one when changing the unique name of survey', async () => {
    await QrCode.createEach(qrCodes).fetch()

    const res = await app
      .patch(`/qrCode/${qrCodes[0].id}`)
      .set('Authorization', token)
      .send({
        uniqueName: 'new-name'
      })
      // added error
      .expect(200)

    expect(res.body).toMatchObject({
      uniqueName: 'new-name',
      qrCodePhoto:
        'https://test.amazonaws.com/flavorwiki-storage/qr-code/organization-1/new-name.png'
    })

    expect(spyUploadPhoto).toHaveBeenCalled()
    expect(spyUploadPhoto.mock.calls[0][0]).toBe(
      'qr-code/organization-1/new-name.png'
    )

    expect(spyDeletePhoto).toHaveBeenCalledWith(
      'qr-codes/organization-1/flavor-wiki-slack.png'
    )
  })

  test('should create uniqueName to survey-1 (duplicate uniqueName but different organization) successfully', async () => {
    await QrCode.createEach(qrCodes).fetch()

    const user1Token = `Bearer ${getUserToken(users[1])}`
    await app
      .post(`/qrCode`)
      .set('Authorization', user1Token)
      .send({
        name: 'Cereal bar survey (duplicate but on organization 2)',
        uniqueName: 'survey-1',
        targetType: 'link',
        targetLink: 'https://git.flavorwiki.info'
      })
      .expect(200)

    // added successfully
    const allQrCodes = await QrCode.find()
    expect(allQrCodes).toHaveLength(4)

    // Two qrCode have same name...
    expect(
      allQrCodes.filter(code => code.uniqueName === 'survey-1')
    ).toHaveLength(2)

    // ...but difference organization
    expect(
      allQrCodes.filter(code => code.uniqueName === 'survey-1')[0]
    ).toMatchObject({
      owner: organization.id
    })

    expect(
      allQrCodes.filter(code => code.uniqueName === 'survey-1')[1]
    ).toMatchObject({
      owner: organizations[1].id
    })
  })

  test('/organization-1/survey-1 should point to /survey/vanilla-cereal-bar (survey link)', async () => {
    await QrCode.createEach(qrCodes).fetch()

    let res = await app
      .get(`/qrCode/getTargetLink?organization=organization-1&qrCode=survey-1`)
      .set('Authorization', token)
      .send()
      .expect(200)

    expect(res.body).toMatchObject({
      type: 'survey',
      url: '/survey/survey-1'
    })
  })

  test('/organization-1/flavor-wiki-slack should point to https://flavor-wiki.slack.com (outer link)', async () => {
    await QrCode.createEach(qrCodes).fetch()

    let res = await app
      .get(
        `/qrCode/getTargetLink?organization=organization-1&qrCode=flavor-wiki-slack`
      )
      .set('Authorization', token)
      .send()
      .expect(200)

    expect(res.body).toMatchObject({
      type: 'link',
      url: 'https://flavor-wiki.slack.com'
    })
  })
})
