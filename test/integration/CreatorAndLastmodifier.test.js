/* globals app, Survey */
/* eslint-env jest */
const { getUserToken, destroyEntitiesByIds } = require('../utils')
const { organizations, users } = require('../fixtures')

const organization = organizations[0]
const creator = users[0]
const modifier = users[3]

const survey = {
  uniqueName: 'survey-2',
  name: 'Survey 2',
  owner: organization.id,
  title: 'Survey 1',
  coverPhoto: '',
  openedAt: Date.now(),
  folder: null
}

describe('creator and last modifier of survey', () => {
  let tokenCreator, tokenModifier
  let createdSurveysIds = []

  beforeAll(async () => {
    tokenCreator = `Bearer ${getUserToken(creator)}`
    tokenModifier = `Bearer ${getUserToken(modifier)}`
  })

  afterEach(async () => {
    await destroyEntitiesByIds(Survey, createdSurveysIds)
  })

  test('should set creator, lastModifier of the survey on creation', async () => {
    const createdSurvey = await app
      .post(`/survey`)
      .set('Authorization', tokenCreator)
      .send(survey)
      .expect(200)

    createdSurveysIds.push(createdSurvey.body.id)

    expect(createdSurvey.body).toHaveProperty('creator', creator.id)
    expect(createdSurvey.body).toHaveProperty('lastModifier', creator.id)
  })

  test('should set creator, lastModifier of the survey on update', async () => {
    const createdSurvey = await app
      .post(`/survey`)
      .set('Authorization', tokenCreator)
      .send(survey)
      .expect(200)

    createdSurveysIds.push(createdSurvey.body.id)

    await app
      .post(`/survey/deprecate/${createdSurvey.body.id}`)
      .set('Authorization', tokenModifier)
      .expect(200)

    const updatedSurvey = await app
      .post(`/survey`)
      .set('Authorization', tokenModifier)
      .send({ predecessorSurvey: createdSurvey.body.id, ...survey })
      .expect(200)

    createdSurveysIds.push(updatedSurvey.body.id)

    expect(updatedSurvey.body).toHaveProperty('creator', creator.id)
    expect(updatedSurvey.body).toHaveProperty('lastModifier', modifier.id)
  })
})
