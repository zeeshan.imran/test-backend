const { createId } = require('../utils')
const organizations = require('./organizations')
const { date } = require('faker')

const surveys = [
  {
    id: createId(),
    uniqueName: 'survey-1',
    name: 'Survey 1',
    title: 'Survey 1',
    coverPhoto: '',
    openedAt: Date.now(),
    folder: null,
    owner: organizations[0].id,
    productRewardsRule: {
      active: false
    }
  }
]
module.exports = surveys
