const { createId } = require('../utils')
const surveys = require('./surveys')

const questions = [
  {
    id: createId(),
    survey: surveys[0].id,
    typeOfQuestion: 'choose-one',
    displayOn: 'begin',
    prompt: 'prompt',
    required: true
  },
  {
    id: createId(),
    survey: surveys[0].id,
    typeOfQuestion: 'choose-multiple',
    displayOn: 'begin',
    prompt: 'prompt',
    required: true
  }
]
module.exports = questions
