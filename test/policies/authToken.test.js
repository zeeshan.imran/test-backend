/* global app, jwToken */
/* eslint-env jest */
const { getUserToken } = require('../utils')
const { users } = require('../fixtures')

describe('isOperator test', () => {
  beforeEach(() => {})

  test('(integration) the super admin can get surveys of operator when impersonating operator', async () => {
    const token = `Bearer ${getUserToken(users[4])}`

    let res = await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .send({})
      .expect(200)

    // admin has no surveys
    expect(res.body).toHaveLength(0)

    res = await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .set('Impersonate', users[0].id)
      .send({})
      .expect(200)

    // user-0 (impersonate) has one survey
    expect(res.body).toHaveLength(1)

    res = await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .set('Impersonate', users[1].id)
      .send({})
      .expect(200)

    // user-1 (impersonate) has no surveys
    expect(res.body).toHaveLength(0)
  })

  test('(integration) the super admin can not invoke operator actions when impersonating user', async () => {
    const token = `Bearer ${getUserToken(users[4])}`

    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .set('Impersonate', users[2].id)
      .send({})
      .expect(403)
  })

  test('(integration) should reject wrong token', async () => {
    const token = `Bearer awrongtoken`

    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .send({})
      .expect(400)
  })

  test('(integration) should reject wrong token format', async () => {
    const token = `Bearerawrongtoken`

    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .send({})
      .expect(400)
  })

  test('(integration) remove token on param if the request does not have the authozation header', async () => {
    await app
      .get(`/survey/getsurveysbyowner?token=asd`)
      .send({})
      .expect(400)
  })

  test('(integration) should reject request without token', async () => {
    await app
      .get(`/survey/getsurveysbyowner`)
      .send({})
      .expect(400)
  })

  test('(integration) should reject request if the user was deleted', async () => {
    const token = `Bearer ${getUserToken({ id: '000000000000000000000000' })}`
    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .send({})
      .expect(400)
  })

  test('(integration) should reject request if the user was deleted', async () => {
    const token = `Bearer ${getUserToken({ id: 'deleted-user' })}`
    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .send({})
      .expect(400)
  })

  test('(integration) should reject token if the api version is wrong', async () => {
    const token = `Bearer ${jwToken.issue({
      ...users[4],
      apiVersion: 'unreal-version'
    })}`

    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .send({})
      .expect(400)
  })

  test('(integration) the super admin can NOT impersonate user who does not exist', async () => {
    const token = `Bearer ${getUserToken(users[4])}`

    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .set('Impersonate', '000000000000000000000000')
      .send({})
      .expect(403)
  })

  test('(integration) the super admin can NOT impersonate user who does not exist', async () => {
    const token = `Bearer ${getUserToken(users[4])}`

    await app
      .get(`/survey/getsurveysbyowner`)
      .set('Authorization', token)
      .set('Impersonate', 'deleted-user')
      .send({})
      .expect(403)
  })
})
