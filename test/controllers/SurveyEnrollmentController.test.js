/* globals app, Survey, Product, SurveyEnrollment, Question, Answer */
/* eslint-env jest */
const { createId, getUserToken, destroyEntitiesByIds } = require('../utils')
const { users, surveys, questions } = require('../fixtures')
const mockedRP = jest.fn()
const rp = require('request-promise')

const creator = users[0]

jest.mock('request-promise', () => args => mockedRP(args))

describe('survey enrollment controller', () => {
  let tokenCreator
  let createdQuestions = []
  let createdProducts = []

  beforeAll(async () => {
    tokenCreator = `Bearer ${getUserToken(creator)}`
  })

  afterEach(async () => {
    await Answer.destroy({})
    await SurveyEnrollment.destroy({})
    destroyEntitiesByIds(Question, createdQuestions)
    destroyEntitiesByIds(Product, createdProducts)
  })

  test('submit answer with different questions', async () => {
    const { id: surveyId } = await Survey.findOne({ id: surveys[0].id })
    const { id: surveyEnrollmentId } = await SurveyEnrollment.create({
      id: createId(),
      survey: surveyId
    }).fetch()

    await app
      .post(`/surveyenrollment/submitanswer`)
      .set('Authorization', tokenCreator)
      .send({
        surveyEnrollment: surveyEnrollmentId,
        startedAt: new Date(),
        question: questions[0].id,
        value: ['value-1']
      })
      .expect(200)

    let answers = await Answer.find({})
    expect(answers.length).toBe(1)
    let enrollment = await SurveyEnrollment.findOne({ id: surveyEnrollmentId })
    expect(enrollment.lastAnsweredQuestion).toBe(questions[0].id)

    await app
      .post(`/surveyenrollment/submitanswer`)
      .set('Authorization', tokenCreator)
      .send({
        surveyEnrollment: surveyEnrollmentId,
        startedAt: new Date(),
        question: questions[1].id,
        value: [{ value: 'value-2', desc: 'desc-2' }, 'value-3']
      })
      .expect(200)

    answers = await Answer.find({})
    expect(answers.length).toBe(2)

    const { id: productId } = await Product.create({
      id: createId(),
      name: 'product-1',
      surveys: [surveys[0].id],
      photo:
        'https://s3-eu-west-1.amazonaws.com/flavorwiki-storage/abed14b2-1509-4ce3-86ed-1e4afe6a641f.png'
    })
    createdProducts.push(productId)

    const questionNotAffectAnswers = ['choose-product', 'profile', 'info']

    for (let qType of questionNotAffectAnswers) {
      let { id: newQuestionId } = await Question.create({
        id: createId(),
        survey: surveys[0].id,
        typeOfQuestion: qType,
        displayOn: 'begin',
        prompt: 'prompt'
      }).fetch()
      createdQuestions.push(newQuestionId)

      await app
        .post(`/surveyenrollment/submitanswer`)
        .set('Authorization', tokenCreator)
        .send({
          surveyEnrollment: surveyEnrollmentId,
          startedAt: new Date(),
          question: newQuestionId,
          value: productId // Used for choose-product
        })
        .expect(200)

      answers = await Answer.find({})
      expect(answers.length).toBe(2)
    }
  })

  test('finish survey should send stats request', async () => {
    const { id: surveyId } = await Survey.findOne({ id: surveys[0].id })
    const { id: surveyEnrollmentId } = await SurveyEnrollment.create({
      id: createId(),
      survey: surveyId
    }).fetch()

    await app
      .post(`/surveyenrollment/submitanswer`)
      .set('Authorization', tokenCreator)
      .send({
        surveyEnrollment: surveyEnrollmentId,
        startedAt: new Date(),
        question: questions[0].id,
        value: ['value-1']
      })
      .expect(200)

    await app
      .post(`/surveyenrollment/submitanswer`)
      .set('Authorization', tokenCreator)
      .send({
        surveyEnrollment: surveyEnrollmentId,
        startedAt: new Date(),
        question: questions[1].id,
        value: [{ value: 'value-2', desc: 'desc-2' }, 'value-3']
      })
      .expect(200)

    await app
      .post(`/surveyenrollment/finishsurvey`)
      .set('Authorization', tokenCreator)
      .send({ surveyEnrollment: surveyEnrollmentId })
      .expect(200)

    const enrollment = await SurveyEnrollment.findOne({
      id: surveyEnrollmentId
    })
    expect(enrollment.state).toBe('finished')
    expect(mockedRP).toHaveBeenLastCalledWith(
      expect.objectContaining({
        method: 'POST',
        body: [
          expect.objectContaining({
            data: [
              expect.objectContaining({
                answer_value: 'value-1',
                question_id: questions[0].id
              }),
              expect.objectContaining({
                answer_value: 'value-2',
                answer_desc: 'desc-2',
                question_id: questions[1].id
              }),
              expect.objectContaining({
                answer_value: 'value-3',
                question_id: questions[1].id
              })
            ]
          })
        ]
      })
    )
  })
})
