/* global jwToken */
/* eslint-env jest */
// eslint-disable-next-line node/no-extraneous-require
const { ObjectId } = require('mongodb')

const createId = () => `${ObjectId()}`

const getUserToken = user => {
  return jwToken.issue(
    Object.assign({}, user, { apiVersion: process.env.USER_API_VERSION })
  )
}

const destroyEntitiesByIds = async (model, idList) => {
  let id
  while ((id = idList.pop())) {
    await model.destroy({ id })
  }
}

module.exports = {
  createId,
  getUserToken,
  destroyEntitiesByIds
}
