// eslint-disable-next-line node/no-unpublished-require, node/no-missing-require
require('dotenv-expand')(require('dotenv').config())

function getBrachVariable (branch, varname) {
  const upperBranch = branch.toUpperCase()
  return process.env[`${varname}_${upperBranch}`] || process.env[varname]
}

const branchVariablesWhiteList = ['PORT', 'APP_URL', 'MONGO_URL', 'STATS_API', 'REACT_APP_THEME']
function generateEnv (branch) {
  return branchVariablesWhiteList.reduce(
    (acc, name) => ({
      ...acc,
      [name]: getBrachVariable(branch, name)
    }),
    {}
  )
}

function generateDeploy (branch) {
  const { SSH_USER, SSH_HOST, DEPLOY_PATH } = process.env
  return {
    user: SSH_USER,
    host: SSH_HOST,
    path: DEPLOY_PATH,
    ref: `origin/${branch}`,
    repo: 'git@git.flavorwiki.info:app/backend.git',
    'post-setup': ['npm install', `npm run seed`].join(' && '),
    'pre-deploy': `git fetch && git reset --hard origin/${branch}`,
    'post-deploy': [
      'ln -sf ~/backend.env .env',
      'npm install',
      `pm2 reload staging.config.js --env ${branch}`
    ].join(' && ')
  }
}

module.exports = {
  apps: [
    {
      name: 'fw-backend',
      env: {
        NODE_ENV: 'production'
      },
      script: 'app.js',
      appendEnvToName: true,
      instances: 1,
      exec_mode: 'cluster',
      env_staging: generateEnv('staging'),
      env_staging1: generateEnv('staging1'),
      env_staging2: generateEnv('staging2'),
      env_staging3: generateEnv('staging3'),
      env_staging4: generateEnv('staging4'),
      env_staging5: generateEnv('staging5'),
      env_staging6: generateEnv('staging6'),
      env_staging7: generateEnv('staging7'),
      env_staging8: generateEnv('staging8'),
      env_staging9: generateEnv('staging9'),
      env_staging10: generateEnv('staging10')
    }
  ],

  deploy: {
    staging: generateDeploy('staging'),
    staging1: generateDeploy('staging1'),
    staging2: generateDeploy('staging2'),
    staging3: generateDeploy('staging3'),
    staging4: generateDeploy('staging4'),
    staging5: generateDeploy('staging5'),
    staging6: generateDeploy('staging6'),
    staging7: generateDeploy('staging7'),
    staging8: generateDeploy('staging8'),
    staging9: generateDeploy('staging9'),
    staging10: generateDeploy('staging10')
  }
}
