const defaultCurrencies = {
  'US': 'USD',
  'GB': 'GBP',
  'DE': 'EUR',
  'FR': 'EUR',
  'BR': 'BRL',
  'PH': 'PHP',
  'CA': 'CAD'
}

module.exports = defaultCurrencies
