/* globals Question */
const orderBy = require('lodash.orderby')
const adaptQuestionToCreation = require('./adaptQuestionToCreation')
const generateLocationQuestionOptions = require('./generateLocationQuestionOptions')
const formatPairedQuestion = require('./formatPairedQuestion')
const updateChooseProductOptions = require('./updateChooseProductOptions')
const cleanPairs = require('./cleanPairs')
const generatePairs = require('./generatePairs')
const imageCreation = require('../imageCreation')
const pairedQuestionIdGenerator = require('./pairedQuestionIdGenerator')
const createLog = require('../createLog')
const { NEVER_REQUIRED_QUESTIONS_TYPES } = require('../constants')

module.exports = async (questionInput, userId) => {
  let question = adaptQuestionToCreation(questionInput)

  if (NEVER_REQUIRED_QUESTIONS_TYPES.has(question.typeOfQuestion)) {
    question.requiredQuestion = false
  } else {
    question.requiredQuestion = questionInput.requiredQuestion || false
  }

  let pairs = []
  if (question.typeOfQuestion === 'paired-questions') {
    // TODO: STAGFW-455 tested
    question.pairs = orderBy(question.pairs, 'optionId', 'asc')
    question.pairs = (question && question.pairs.length && await Promise.all(question.pairs.map(async p => {
      if (p.image800) {
        return {
          ...p,
          optionId: p.optionId ? p.optionId : await pairedQuestionIdGenerator(),
          image800: await imageCreation(p.image800)
        }
      } else {
        return {
          ...p,
          optionId: p.optionId ? p.optionId : await pairedQuestionIdGenerator()
        }
      }
    }))) || question.pairs
    const [formattedQuestion, formattedPairs] = formatPairedQuestion(question)
    question = formattedQuestion
    pairs = formattedPairs
  }

  // TODO: STAGFW-455 tested
  if (question.typeOfQuestion === 'location') {
    question.options = generateLocationQuestionOptions(question.region)
  }

  const oldQuestion = await Question.findOne(question.id)

  let { id: questionId, ...propertiesToUpdate } = question
  let updatedQuestion = await Question.updateOne({ id: questionId }).set(
    propertiesToUpdate
  )

  // NOTE: STAGFW-455 tested
  if (question.typeOfQuestion === 'paired-questions') {
    await cleanPairs(updatedQuestion.id)
    await generatePairs(pairs, updatedQuestion.id)
    updatedQuestion = await Question.findOne(question.id).populate('pairs')
  }

  // NOTE: STAGFW-455 tested
  if (question.typeOfQuestion === 'choose-product') {
    await updateChooseProductOptions(question, userId)
  }

  if (userId && updatedQuestion.survey) {
    const newQuestion = await Question.findOne(question.id)
    createLog.question(userId, updatedQuestion.survey, oldQuestion, newQuestion)
  }

  return updatedQuestion
}
