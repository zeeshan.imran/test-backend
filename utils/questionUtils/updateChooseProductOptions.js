/* globals Survey */
const createLog = require('../createLog')

module.exports = async (question, userId) => {
  if (question.chooseProductOptions) {
    const { minimumProducts, maximumProducts } = question.chooseProductOptions
    const oldSurvey = await Survey.findOne(question.survey)
    await Survey.update({ id: question.survey })
      .set({
        minimumProducts,
        maximumProducts
      })
      .meta({ fetch: false })
    const newSurvey = await Survey.findOne(question.survey)

    if (userId) {
      createLog.survey(userId, question.survey, oldSurvey, newSurvey)
    }
  }
}
