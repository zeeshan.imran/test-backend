const R = require('ramda')

const adaptQuestionToCreation = questionAsInput => ({
  ...R.omit(['type'], questionAsInput),
  typeOfQuestion: questionAsInput.type
})

module.exports = adaptQuestionToCreation
