/* globals Question */
const adaptQuestionToCreation = require('./adaptQuestionToCreation')
const formatPairedQuestion = require('./formatPairedQuestion')
const generateLocationQuestionOptions = require('./generateLocationQuestionOptions')
const updateChooseProductOptions = require('./updateChooseProductOptions')
const generatePairs = require('./generatePairs')
const imageCreation = require('../imageCreation')
const pairedQuestionIdGenerator = require('./pairedQuestionIdGenerator')
const createLog = require('../createLog')
const { NEVER_REQUIRED_QUESTIONS_TYPES } = require('../constants')

module.exports = async (questionInput, userId) => {
  let question = adaptQuestionToCreation(questionInput)
  let pairs = []

  if (NEVER_REQUIRED_QUESTIONS_TYPES.has(question.typeOfQuestion)) {
    question.requiredQuestion = false
  } else {
    question.requiredQuestion = questionInput.requiredQuestion || false
  }

  if (question.typeOfQuestion === 'paired-questions') {
    // TODO: STAGFW-455 tested
    question.pairs =
      (question &&
        question.pairs.length &&
        (await Promise.all(
          question.pairs.map(async p => {
            if (p.image800) {
              return {
                ...p,
                optionId: p.optionId ? p.optionId : await pairedQuestionIdGenerator(),
                image800: await imageCreation(p.image800)
              }
            } else {
              return {
                ...p,
                optionId: p.optionId ? p.optionId : await pairedQuestionIdGenerator()
              }
            }
          })
        ))) ||
      question.pairs
    const [formattedQuestion, formattedPairs] = formatPairedQuestion(question)
    question = formattedQuestion
    pairs = formattedPairs
  }

  // TODO: STAGFW-455 tested
  if (question.typeOfQuestion === 'location') {
    question.options = generateLocationQuestionOptions(question.region)
  }

  let createdQuestion = await Question.create(question).fetch()

  // NOTE: STAGFW-455 tested
  if (question.typeOfQuestion === 'paired-questions') {
    await generatePairs(pairs, createdQuestion.id)
    createdQuestion = await Question.findOne(question.id).populate('pairs')
  }

  // NOTE: STAGFW-455 tested
  if (question.typeOfQuestion === 'choose-product') {
    await updateChooseProductOptions(questionInput, userId)
  }

  if (userId && createdQuestion.survey) {
    createLog.question(userId, createdQuestion.survey, null, createdQuestion)
  }

  return createdQuestion
}
