const { applyColors } = require('./chartSettingsUtils')
const {
  toPptFullwidthLoc,
  toChartMean,
  calcBestSpace,
  dynamicFontSizes
} = require('./utils')

module.exports = ({ slide, settings }, { avg, ...others }, loc) => {
  const chartData = toChartMean(avg)
  const bestSpace = calcBestSpace(avg.length, 1)
  const { chartSettings } = others
  const fontArray = dynamicFontSizes(chartSettings)
  slide.addChart('bar', chartData, {
    showLegend: true,
    legendPos: 'b',
    showValue: true,
    dataLabelPosition: 'outEnd',
    dataLabelFormatCode: '#.0',
    barGapWidthPct: bestSpace,
    chartColors: applyColors(others, settings.chartColors),
    ...fontArray,
    ...toPptFullwidthLoc(loc)
  })
  return slide
}
