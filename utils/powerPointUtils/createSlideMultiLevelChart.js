const createSlideWithChartParts = require('./createSlideWithChartParts')

const ANALYSIS_TITLES = {
  'anova-chart': 'Anova analysis',
  'fisher-chart': 'Fisher analysis',
  'tukey-chart': 'Tukey analysis'
}

module.exports = async (mainParts, ppt, chart, additionalParts = []) => {
  for (const mainPart of mainParts) {
    const loopChildren = mainPart.data.children

    for (const singleChart of loopChildren) {
      await createSlideWithChartParts(
        ppt,
        chart,
        [singleChart, ...additionalParts],
        ANALYSIS_TITLES[mainPart.type]
      )
    }
  }
}
