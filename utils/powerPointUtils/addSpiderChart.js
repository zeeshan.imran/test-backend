const { applyColors } = require('./chartSettingsUtils')
const {
  toPptFullwidthLoc,
  toSpiderChartData,
  dynamicFontSizes
} = require('./utils')

module.exports = ({ slide, settings }, { inputData, ...others }, loc) => {
  const { chartSettings } = others
  const fontArray = dynamicFontSizes(chartSettings)
  slide.addChart('radar', toSpiderChartData(inputData), {
    showLegend: true,
    legendPos: 'b',
    chartColors: applyColors(others, settings.chartColors),
    radarStyle: 'standard',
    title: inputData.title,
    ...fontArray,
    ...toPptFullwidthLoc(loc)
  })
  return slide
}
