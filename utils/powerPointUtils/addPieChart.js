const { applyColors } = require('./chartSettingsUtils')
const { toPptFullwidthLoc, toChartData } = require('./utils')

module.exports = ({ slide, settings }, { results, ...others }, loc) => {
  let { chartSettings: { fontSize_legend: legendFontSize } = 1.2 } = others
  legendFontSize = legendFontSize ? legendFontSize * 10 : 12

  slide.addChart('doughnut', toChartData(results), {
    showLegend: true,
    legendFontSize,
    dataLabelFormatCode: 'General',
    legendPos: 'b',
    dataLabelColor: 'ffffff',
    chartColors: applyColors(others, settings.chartColors),
    ...toPptFullwidthLoc(loc)
  })
  return slide
}
