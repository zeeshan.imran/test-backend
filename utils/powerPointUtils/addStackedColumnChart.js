const { applyColors } = require('./chartSettingsUtils')
const {
  toChartData,
  rotateChartData,
  toPptFullwidthLoc,
  toChartMean,
  toNumberFormatCode,
  calcBestSpace,
  addGrouping,
  dynamicFontSizes
} = require('./utils')
const R = require('ramda')

module.exports = async (
  { slide, settings },
  {
    results,
    avg,
    chart_avg: chartAvg,
    valuesExtent,
    howManyDecimals,
    grouping,
    ...others
  },
  loc,
  statsGrouping
) => {
  let chartData = toChartData(results)
  let totalRespondentsPerProduct = []
  chartData.forEach(item => {
    if (item && item.values && item.values.length) {
      const value = item.values.reduce((a, b) => a + b, 0)
      totalRespondentsPerProduct.push(value)
    }
  })

  chartData = await rotateChartData(chartData)
  const { x, y, w, h } = toPptFullwidthLoc(loc)
  const mainW = chartAvg ? w * 0.6 : w
  const bestSpace = calcBestSpace(results.length, chartAvg ? 2 / 3 : 1)
  const chartColors = applyColors(others, settings.chartColors)
  const { chartSettings } = others
  const {
    catAxisLabelFontSize,
    valAxisLabelFontSize,
    dataLabelFontSize
  } = dynamicFontSizes(chartSettings)
  slide.addChart('bar', chartData, {
    barDir: 'col',
    barGrouping: 'percentStacked',
    barGapWidthPct: bestSpace,
    dataLabelPosition: 'ctr',
    valAxisLabelFontSize,
    catAxisLabelFontSize,
    dataLabelFontSize,
    showValue: true,
    dataLabelFormatCode: '0%',
    valAxisMinVal: 0,
    showPercent: true,
    showLegend: false,
    legendFontSize: 12,
    legendPos: 'r',
    chartColors,
    x,
    y,
    w: mainW,
    h
  })
  if (avg && avg.length) {
    const textBoxSettings = {
      rotate: 0,
      margin: 0.5,
      fontFace: 'Arial',
      fontSize: 11,
      color: '000000',
      bold: true,
      isTextBox: true
    }
    let xValue = mainW / avg.length
    avg.forEach(async (single, index) => {
      await slide.addText(`Mean: ${single.value.toFixed(1)}`, {
        x: `${(index + 0.5) * xValue}`,
        y: 0.95,
        w: '10%',
        ...textBoxSettings
      })
      if (totalRespondentsPerProduct[index]) {
        await slide.addText(`(N=${totalRespondentsPerProduct[index]})`, {
          x: `${(index + 0.5) * xValue}`,
          y: 6.6,
          w: '10%',
          ...textBoxSettings
        })
      }
      return slide
    })
  }
  if (chartAvg) {
    const [min, max] = valuesExtent
    const meanData = await toChartMean(avg)
    slide.addChart('bar', meanData, {
      barDir: 'bar',
      catAxisOrientation: 'maxMin',
      showLabel: true,
      showValue: !statsGrouping,
      dataLabelFormatCode: toNumberFormatCode(howManyDecimals),
      dataLabelPosition: 'outEnd',
      valAxisMinVal: min,
      valAxisMaxVal: max,
      barGapWidthPct: 150,
      showPercent: true,
      showLegend: false,
      legendFontSize: 12,
      legendPos: 'r',
      chartColors: chartColors.map(() => chartColors[0]),
      x: x + mainW,
      y,
      w: x + w - mainW,
      h: 0.7 * avg.length
    })

    if (!statsGrouping) {
      return slide
    }
    let groups = R.path(
      ['0', 'data', 'children', '0', 'data', 'dataSource'],
      statsGrouping
    )
    if (!groups || !groups.length || !meanData || !meanData[0]) {
      return slide
    }
    groups = addGrouping(
      meanData[0],
      groups,
      valuesExtent,
      howManyDecimals,
      x + mainW,
      y,
      x + w - mainW,
      0.7 * avg.length
    )
    if (!groups || !groups.length) {
      return slide
    }
    groups.forEach(group =>
      slide.addText(group.text, {
        x: group.x,
        y: group.y,
        fontSize: 12,
        fontFace: 'Arial'
      })
    )
  }

  return slide
}
