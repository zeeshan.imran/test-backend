module.exports = ({ slide, settings }, { title, totalRespondents }) => {
  const { texts, option } = settings.chartTitle({ title, totalRespondents })
  return slide.addText(texts.filter(t => !!t), option)
}
