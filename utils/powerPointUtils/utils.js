const roundLevel = 1e2
const rowTags = 5
const tagsHeight = 0.3
const round = num => Math.round(num * roundLevel) / roundLevel

const R = require('ramda')

const barTopMargin = 0.6
const barBottomMargin = 0
const barLabelMargin = 1

const formatSeries = ({ name, data }) => ({
  name,
  labels: data.map(R.prop('label')),
  values: data.map(R.prop('value'))
})

module.exports.toPenaltyData = ({ labels, lowPlots, highPlots }) => [
  {
    name: 'X-Axis',
    values: [
      ...lowPlots.map(([_value, percent]) => percent),
      ...highPlots.map(([_value, percent]) => percent)
    ]
  },
  ...labels.map((name, index) => ({
    name,
    values: [
      ...lowPlots.map(([value, _percent], lowIndex) =>
        index === lowIndex ? value : null
      ),
      ...highPlots.map(([value, _percent], lowIndex) =>
        index === lowIndex ? value : null
      )
    ]
  }))
]

module.exports.toChartData = results =>
  results.map ? results.map(formatSeries) : [formatSeries(results)]

module.exports.toSpiderChartData = inputData =>
  inputData.series.map(current => ({
    name: current.name,
    values: current.data,
    labels: inputData.labels
  }))

module.exports.rotateChartData = async chartData => {
  const { labels } = R.head(chartData)

  await chartData.map(async row => {
    const sum = row.values.reduce((a, b) => {
      return a + b
    }, 0)
    let percentageValues = []
    row.values.forEach(item => {
      percentageValues.push(item / sum)
    })
    row.values = percentageValues

    return row
  })

  return labels.map((label, labelIndex) => ({
    name: label,
    labels: chartData.map(row => row.name),
    values: chartData.map(row => row.values[labelIndex])
  }))
}

module.exports.toPptLoc = loc => ({
  x: round(loc.x),
  y: round(loc.y),
  w: round(loc.w),
  h: round(loc.h)
})

module.exports.toPptFullwidthLoc = loc => ({
  x: round(loc.fullwidth.x),
  y: round(loc.y),
  w: round(loc.fullwidth.w),
  h: round(loc.h)
})

module.exports.toPptTagsFullwidthLoc = (loc, index) => ({
  x: round(
    loc.fullwidth.x + ((loc.fullwidth.w + 0.2) / rowTags) * (index % rowTags)
  ),
  y: round(loc.y + (tagsHeight + 0.2) * Math.floor(index / rowTags)),
  w: round(loc.fullwidth.w / rowTags - 0.2),
  h: round(tagsHeight)
})

module.exports.toChartMean = results => {
  const chartMeanData = {
    labels: results.map(R.prop('label')),
    values: results.map(R.prop('value'))
  }
  return [chartMeanData]
}

module.exports.toNumberFormatCode = howManyDecimals => {
  if (!howManyDecimals) {
    return `#,##0`
  }
  const decimalFormat = [...new Array(howManyDecimals)].map(() => '0').join('')
  return `#,##0.${decimalFormat}`
}

module.exports.calcBestSpace = (cols, spaceFactor = 1) =>
  Math.min(Math.round(((1200 - cols * 100) / cols) * spaceFactor), 350)

module.exports.addGrouping = (
  data,
  groupedProducts,
  valuesExtent,
  decimals,
  x,
  y,
  w,
  h
) => {
  const distance = valuesExtent[1] - valuesExtent[0]
  const heightStep = (h - barTopMargin - barBottomMargin) / data.labels.length
  const widthStep = (w - barLabelMargin) / distance
  if (data && data.labels && data.labels.length) {
    return data.labels.map((label, index) => {
      const group = groupedProducts.find(group => group.product === label)
      const value = data.values[index]
      return {
        x: x + barLabelMargin + widthStep * (value - valuesExtent[0] - 0.4),
        y: y + barTopMargin + heightStep * (index + 0.19),
        text: group
          ? `${value.toFixed(decimals)} ${group.group}`
          : value.toFixed(decimals)
      }
    })
  }
  return null
}
module.exports.dynamicFontSizes = chartSettings => {
  const defaultFontSize = 10
  const xAxis = chartSettings.fontSize_labels || 1.8
  const yAxis = chartSettings.fontSize_axis || 1.8
  const valueSize = chartSettings.fontSize_values || 1.3
  const legendFontSize = chartSettings.fontSize_legend || 1.2
  return {
    catAxisLabelFontSize: xAxis * defaultFontSize,
    valAxisLabelFontSize: yAxis * defaultFontSize,
    dataLabelFontSize: valueSize * defaultFontSize,
    legendFontSize: legendFontSize * defaultFontSize
  }
}
