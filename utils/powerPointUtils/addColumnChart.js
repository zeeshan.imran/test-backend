const { applyColors } = require('./chartSettingsUtils')
const {
  toChartData,
  toPptFullwidthLoc,
  calcBestSpace,
  dynamicFontSizes
} = require('./utils')

module.exports = ({ slide, settings }, { results, ...others }, loc) => {
  const { chartSettings } = others
  const fontArray = dynamicFontSizes(chartSettings)

  const chartData = toChartData(results)
  const bestSpace = calcBestSpace(results.length, 1)
  slide.addChart('bar', chartData, {
    showLegend: true,
    showValue: true,
    legendPos: 'b',
    barGapWidthPct: bestSpace,
    ...fontArray,
    chartColors: applyColors(others, settings.chartColors),
    ...toPptFullwidthLoc(loc)
  })
  return slide
}
