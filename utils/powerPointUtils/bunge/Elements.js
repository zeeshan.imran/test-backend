const {
  createLogo,
  createCopyRight,
  createPageNumber
} = require('../themeUtils')
const theme = require('./theme')

module.exports.Logo = createLogo(theme)
module.exports.CopyRight = createCopyRight(theme)
module.exports.PageNumber = createPageNumber(theme)

module.exports.IntroBackground = {
  image: {
    path: './assets/images/background.png',
    w: 13.33,
    h: 7.47
  }
}

module.exports.IntroTitle = {
  placeholder: {
    text: 'Title',
    options: {
      name: '(title)',
      type: 'text',
      x: 1.22,
      y: 4.33,
      w: 5.88,
      h: 1.47,
      fontFace: 'Calibri (Body)',
      color: 'FFFFFF'
    }
  }
}
