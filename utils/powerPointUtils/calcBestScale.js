const R = require('ramda')

module.exports = (chartParts, { settings }) => {
  const {
    chartArea: { width: ow, height: oh },
    spaceBetweenParts
  } = settings

  const height =
    R.pipe(
      R.map(R.prop('height')),
      R.map(h => h + spaceBetweenParts),
      R.sum
    )(chartParts) - spaceBetweenParts

  const width = R.pipe(
    R.map(R.prop('width')),
    R.reduce(R.max, 0)
  )(chartParts)

  return Math.min(oh / height, ow / width)
}
