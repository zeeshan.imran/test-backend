const R = require('ramda')
const PptxGen = require('@flavorwiki/pptxgenjs')

module.exports = ({ template }) => {
  const { ChartMaster, IntroMaster, globalSettings, theme } = require(`./${template}`)

  const pptx = new PptxGen()
  pptx.layout = 'LAYOUT_WIDE'

  pptx.defineSlideMaster(R.clone(ChartMaster))
  pptx.defineSlideMaster(R.clone(IntroMaster))

  return { pptx, theme, settings: globalSettings }
}
