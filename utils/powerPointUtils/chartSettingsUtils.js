module.exports.applyColors = ({ colorsArr }, defaultColors) => {
  if (colorsArr && colorsArr.length > 0) {
    const matches = colorsArr.map(c => /^#([0-9a-f]+)/.exec(c))
    if (matches.every(m => m)) {
      return matches.map(m => m[1])
    }
  }

  return defaultColors
}
