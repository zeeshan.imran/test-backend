const { toPptTagsFullwidthLoc } = require('./utils')
const defaultFontSize = 16

module.exports = ({ slide }, { results }, loc) => {
  results.map((result, index) => {
    const fontSize =
      result.label.length <= 13
        ? defaultFontSize
        : defaultFontSize *
            (defaultFontSize / ((result.label.length - 3) * 1.5)) +
          2
    slide.addText(result.label, {
      fill: { color: 'cccccc' },
      fontSize: fontSize,
      ...toPptTagsFullwidthLoc(loc, index)
    })
  })
  return slide
}
