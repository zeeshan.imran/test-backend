module.exports = {
  setup: require('./setup'),
  createSlide: require('./createSlide'),
  addIntro: require('./addIntro'),
  calcBestScale: require('./calcBestScale'),
  createSlideWithChartParts: require('./createSlideWithChartParts'),
  createSlideMultiLevelChart: require('./createSlideMultiLevelChart'),
  createSlideMultiProductChart: require('./createSlideMultiProductChart'),
  createSlidePCA: require('./createSlidePCA'),
  toBase64: require('./toBase64')
}
