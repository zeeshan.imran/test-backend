const { toPptFullwidthLoc } = require('./utils')

module.exports = ({ slide, settings }, { columns, dataSource }, loc) => {
  const { x, y, w } = toPptFullwidthLoc(loc)
  const rows = [
    columns.map(c =>
      settings.tableHeader ? settings.tableHeader(c.title) : c.title
    ),
    ...dataSource.map(row =>
      columns.map(c =>
        settings.tableRow
          ? settings.tableRow(String(row[c.dataIndex]))
          : String(row[c.dataIndex])
      )
    )
  ]

  slide.addTable(rows, {
    x,
    y,
    w,
    ...settings.table()
  })
}
