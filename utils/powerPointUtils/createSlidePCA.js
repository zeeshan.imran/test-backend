const addPicture = require('./addPicture')
const createSlideWithChartParts = require('./createSlideWithChartParts')

const scaleAndAddPicture = async (slide, chart, { x, y, w, h }) => {
  const scale = Math.min(w / chart.width, h / chart.height)
  addPicture(slide, await chart.getScreenshot(), {
    x: typeof x === 'function' ? x(chart.width * scale) : x,
    y: typeof y === 'function' ? y(chart.height * scale) : y,
    w: chart.width * scale,
    h: chart.height * scale
  })
}

module.exports = async (mainParts, ppt, chart) => {
  const {
    settings: {
      chartArea: { x, y, width, height }
    }
  } = ppt

  for (const mainPart of mainParts) {
    const [chart3d, chart2d1, chart2d2, chart2d3] = mainPart.data.children

    const slide3d = await createSlideWithChartParts(
      ppt,
      chart,
      [],
      'PCA analysis'
    )

    await scaleAndAddPicture(slide3d, chart3d, {
      x: w => x + (width - w) / 2,
      y,
      w: width,
      h: height
    })

    const space = x

    const w = (width - 2 * space) / 3
    const h = height - 2 * space

    const slide2d = await createSlideWithChartParts(
      ppt,
      chart,
      [],
      'PCA analysis'
    )
    await scaleAndAddPicture(slide2d, chart2d1, {
      x: x,
      y: h => y + (height - h) / 2,
      w,
      h
    })
    await scaleAndAddPicture(slide2d, chart2d2, {
      x: x + w + space,
      y: h => y + (height - h) / 2,
      w,
      h
    })
    await scaleAndAddPicture(slide2d, chart2d3, {
      x: x + 2 * (w + space),
      y: h => y + (height - h) / 2,
      w,
      h
    })
  }
}
