const { applyColors } = require('./chartSettingsUtils')
const { toChartData, toPptFullwidthLoc, dynamicFontSizes } = require('./utils')

module.exports = ({ slide, settings }, { results, ...others }, loc) => {
  const chartData = toChartData(results)
  const { chartSettings } = others
  const fontArray = dynamicFontSizes(chartSettings)
  slide.addChart('bar', chartData, {
    barDir: 'bar',
    catAxisOrientation: 'maxMin',
    showLegend: false,
    showValue: true,
    dataLabelPosition: 'outEnd',
    dataLabelFormatCode: '#.0',
    legendPos: 'b',
    ...fontArray,
    chartColors: applyColors(others, settings.chartColors),
    ...toPptFullwidthLoc(loc)
  })
  return slide
}
