const R = require('ramda')

module.exports.createLogo = ({ logoSrc, logoRatio }, overwritten = {}) => {
  const image = R.mergeDeepRight(
    { x: 10.4, y: 6.76, w: 2.2, path: logoSrc },
    overwritten
  )
  image.h = image.w / logoRatio
  return {
    image
  }
}

module.exports.createCopyRight = ({ copyRight }, overwritten = {}) => {
  const options = R.mergeDeepRight(
    {
      fontFace: 'Calibri (Body)',
      fontSize: 7,
      x: 0.72,
      y: 7.16
    },
    overwritten
  )

  return { text: { text: copyRight, options } }
}

module.exports.createPageNumber = (_theme, overwritten = {}) => {
  const pageNumber = R.mergeDeepRight(
    { x: 0.21, y: 7.16, fontFace: 'Calibri (Body)', fontSize: 8 },
    overwritten
  )

  return pageNumber
}

module.exports.createTheme = (theme, overwritten = {}) =>
  R.mergeDeepRight(
    {
      theme,
      ChartMaster: {
        bkgd: theme.background,
        title: 'CHART_MASTER'
      },
      IntroMaster: {
        bkgd: theme.background,
        title: 'INTRO_MASTER'
      },
      globalSettings: {
        chartTitle: ({ title, totalRespondents }) => ({
          texts: [
            title && {
              text: title,
              options: {
                fontSize: 18,
                bold: true
              }
            },
            totalRespondents && {
              text: '\n' + totalRespondents,
              options: { fontSize: 16, bold: false }
            }
          ],
          option: {
            x: 0.21,
            y: 0.28,
            w: 12.84,
            h: 0.8,
            fontFace: 'Calibri (Body)',
            valign: 'top'
          }
        }),
        chartArea: { x: 0.21, y: 0.91, width: 12.84, height: 5.67 },
        spaceBetweenParts: 10,
        tableHeader: text => ({
          text,
          options: {
            h: 1.4,
            bold: true,
            fontSize: 12,
            fill: theme.table.headerBackgroundColor,
            color: theme.table.headerColor
          }
        }),
        tableRow: text => ({
          text,
          options: {
            h: 1.4,
            bold: false,
            fontSize: 14,
            fill: theme.table.bodyBackgroundColor,
            color: theme.table.bodyColor
          }
        }),
        table: () => ({
          autoPage: true,
          autoPageRepeatHeader: true,
          border: [
            theme.table.border,
            theme.table.border,
            theme.table.border,
            theme.table.border
          ]
        }),
        chartColors: theme.chartColors
      }
    },
    overwritten
  )
