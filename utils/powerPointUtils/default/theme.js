module.exports = {
  logoSrc: './assets/images/flavorlogo.png',
  logoRatio: 4000 / 615,
  copyRight: '© 2020 Flavor Wiki. All rights reserved.',
  background: 'f0f0f0',
  table: {
    headerBackgroundColor: '999999',
    headerColor: 'ffffff',
    bodyColor: '333333',
    border: { type: 'solid', pt: 1, color: '999999' }
  },
  chartColors: [
    '3497db',
    'e74c3c',
    '2fcc71',
    'f1c40f',
    '8e44ad',
    '95a5a7',
    'ff7f0e',
    '1abc9c',
    '8c564b',
    'e377c2'
  ]
}
