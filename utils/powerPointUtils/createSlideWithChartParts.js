const calcBestScale = require('./calcBestScale')
const addColumnChart = require('./addColumnChart')
const addTable = require('./addTable')
const addPicture = require('./addPicture')
const createSlide = require('./createSlide')
const getChartTitle = require('../chartUtils/getChartTitle')
const addTitle = require('./addTitle')
const addPieChart = require('./addPieChart')
const addStackedColumnChart = require('./addStackedColumnChart')
const addStackedBarChart = require('./addStackedBarChart')
const addTableChart = require('./addTableChart')
const addColumnChartMean = require('./addColumnChartMean')
const addBarChart = require('./addBarChart')
const addSpiderChart = require('./addSpiderChart')
const addLineChart = require('./addLineChart')
const addPenaltyChart = require('./addPenaltyChart')
const addTagsList = require('./addTagsList')
const { TOTAL_RESPONDENTS } = require('./constants')

const DRAWERS = {
  [TOTAL_RESPONDENTS]: false,
  'column-chart': addColumnChart,
  'stacked-column-chart': addStackedColumnChart,
  'stacked-bar-chart': addStackedBarChart,
  'pie-chart': addPieChart,
  'table-chart': addTableChart,
  table: addTable,
  'column-chart-mean': addColumnChartMean,
  'bar-chart': addBarChart,
  polar: addSpiderChart,
  'line-chart': addLineChart,
  'penalty-chart': addPenaltyChart,
  'tags-list': addTagsList
}

const createSlideWithChartParts = async (
  ppt,
  chart,
  chartParts,
  subTitle,
  statsGrouping
) => {
  const slide = createSlide(ppt)
  let title = await getChartTitle(chart)

  const totalRespondentPart = chartParts.find(
    part => part.type === TOTAL_RESPONDENTS
  )

  const drawableChartParts = chartParts.filter(
    part => DRAWERS[part.type] !== false
  )
  if (subTitle) {
    title = `${title} -- ${subTitle}`
  }

  let totalRespondents = false
  totalRespondents = totalRespondentPart ? totalRespondentPart.data : false

  addTitle(slide, { title, totalRespondents })

  const {
    chartArea: { x: ox, y: oy, width: ow },
    spaceBetweenParts
  } = ppt.settings
  const scale = calcBestScale(drawableChartParts, ppt)

  let top = 0
  for (const part of drawableChartParts) {
    const loc = {
      x: ox + ow / 2 - (part.width * scale) / 2,
      y: oy + top * scale,
      h: part.height * scale,
      w: part.width * scale,
      fullwidth: {
        x: ox,
        w: ow
      }
    }
    top += part.height + spaceBetweenParts

    const drawer = DRAWERS[part.type]
    if (drawer) {
      drawer(slide, part.data, loc, statsGrouping)
    } else {
      addPicture(slide, await part.getScreenshot(), loc)
    }
  }

  return slide
}

module.exports = createSlideWithChartParts
