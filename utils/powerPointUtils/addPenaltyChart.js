const { toPptFullwidthLoc, toPenaltyData } = require('./utils')
const { applyColors } = require('./chartSettingsUtils')

module.exports = (
  { slide, settings, theme },
  { labels, low_plots: lowPlots, high_plots: highPlots, ...others },
  loc
) => {
  const chartData = toPenaltyData({ labels, lowPlots, highPlots })
  const { x, y, w, h } = toPptFullwidthLoc(loc)
  const chartColors = applyColors(others, settings.chartColors)
  slide.addChart('scatter', chartData, {
    showLegend: true,
    lineSize: 0,
    lineDataSymbol: 'circle',
    lineDataSymbols: labels.map((_label, idx) => ({ idx, symbol: 'diamond' })),
    lineDataSymbolSize: 10,
    lineDataSymbolLineSize: 0.5,
    lineDataSymbolLineColor: '000000',
    legendFontSize: 12,
    legendPos: 'b',
    chartColors,
    x,
    y,
    w,
    h
  })

  slide.addText('Too High\nToo Low', {
    x: x + w - 1.6,
    y: y + 0.16,
    w: 1.1,
    h: 0.55,
    fontSize: 10,
    fill: theme.background,
    line: { pt: 0.5, color: 'CCCCCC' },
    bold: true,
    margin: [25, 0, 2.2, 0],
    lineSpacing: 16
  })

  slide.addShape('ellipse', {
    x: x + w - 1.6 + 0.12,
    y: y + 0.16 + 0.125,
    w: 0.11,
    h: 0.11,
    fill: { color: 'DDDDDD' },
    line: { pt: 0.5, color: '999999' }
  })

  slide.addShape('diamond', {
    x: x + w - 1.6 + 0.12,
    y: y + 0.16 + 0.1 + 0.225,
    w: 0.12,
    h: 0.12,
    fill: { color: 'DDDDDD' },
    line: { pt: 0.5, color: '999999' }
  })
  return slide
}
