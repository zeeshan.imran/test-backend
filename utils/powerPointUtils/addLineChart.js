const { toPptFullwidthLoc, toChartData, dynamicFontSizes } = require('./utils')

module.exports = ({ slide, settings }, { results, ...others }, loc) => {
  const {
    chartSettings: { singleColor: colorsArr }
  } = others
  const { chartSettings } = others
  const fontArray = dynamicFontSizes(chartSettings)
  const chartData = toChartData(results)
  slide.addChart('line', chartData, {
    showLegend: false,
    dataBorder: { pt: '1', color: '#333333' },
    dataLabelColor: '696969',
    dataLabelFontFace: 'Arial',
    dataLabelFormatCode: '#.0',
    showValue: true,
    dataLabelPosition: 't',
    lineSmooth: true,
    legendPos: 'r',
    ...fontArray,
    chartColors: colorsArr.length ? colorsArr : settings.chartColors,
    ...toPptFullwidthLoc(loc)
  })
  return slide
}
