const { applyColors } = require('./chartSettingsUtils')
const {
  toPptFullwidthLoc,
  toChartData,
  rotateChartData,
  calcBestSpace,
  toChartMean,
  dynamicFontSizes
} = require('./utils')
const HORIZONTAL_BAR = 'horizontal-bars-mean'

module.exports = async (
  { slide, settings },
  { results, chart_type: chartType, avg, ...others },
  loc
) => {
  const bestSpace = calcBestSpace(results.length)
  const chartColors = applyColors(others, settings.chartColors)
  const { chartSettings } = others
  const fontArray = dynamicFontSizes(chartSettings)

  if (chartType === HORIZONTAL_BAR) {
    slide.addChart('bar', toChartMean(avg), {
      barDir: 'bar',
      showValue: true,
      dataLabelPosition: 'outEnd',
      dataLabelFormatCode: '#.0',
      barGapWidthPct: bestSpace,
      showLegend: true,
      legendPos: 'b',
      chartColors,
      ...fontArray,
      ...toPptFullwidthLoc(loc)
    })
  } else {
    let chartData = toChartData(results)
    chartData = await rotateChartData(chartData)
    slide.addChart('bar', chartData, {
      barDir: 'bar',
      barGrouping: 'percentStacked',
      barGapWidthPct: bestSpace,
      dataLabelPosition: 'ctr',
      showValue: true,
      dataLabelFormatCode: '0%',
      showLegend: true,
      legendPos: 'b',
      chartColors,
      ...fontArray,
      ...toPptFullwidthLoc(loc)
    })
  }

  return slide
}
