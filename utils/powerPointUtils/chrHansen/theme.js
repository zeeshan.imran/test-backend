module.exports = {
  logoSrc: './assets/hansen/logo.png',
  logoRatio: 238 / 86,
  copyRight: '© 2020 Chr. Hansen. All rights reserved.',
  background: 'f0f0f0',
  table: {
    headerBackgroundColor: '003a72',
    headerColor: 'ffffff',
    bodyColor: '003a72',
    border: { type: 'solid', pt: 1, color: '003a72' }
  },
  chartColors: [
    'c3cfe1',
    '9b9b9b',
    '007770',
    '003a72',
    '4c6c9c',
    '4a4a4a',
    '004b88',
    '009864',
    'fcb813',
    'f37321',
    'c30c3e',
    '7a0f2d'
  ]
}
