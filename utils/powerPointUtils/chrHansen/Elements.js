const {
  createLogo,
  createCopyRight,
  createPageNumber
} = require('../themeUtils')
const theme = require('./theme')

module.exports.Logo = createLogo(theme, { x: 11.4, w: 1.65 })
module.exports.CopyRight = createCopyRight(theme, { fontSize: 8 })
module.exports.PageNumber = createPageNumber(theme)

module.exports.IntroBackground = {
  image: {
    path: './assets/hansen/background.png',
    w: 13.33,
    h: 6.47
  }
}

module.exports.IntroTitleBackground = {
  image: {
    path: './assets/hansen/title.png',
    x: 0.73,
    y: 3.68,
    w: 6.73,
    h: 3.07
  }
}

module.exports.IntroTitle = {
  placeholder: {
    text: 'Title',
    options: {
      name: '(title)',
      type: 'text',
      x: 1.22,
      y: 4.33,
      w: 5.88,
      h: 1.47,
      fontFace: 'Calibri (Body)',
      color: 'FFFFFF'
    }
  }
}
