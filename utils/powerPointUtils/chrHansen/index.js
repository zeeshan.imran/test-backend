const {
  Logo,
  CopyRight,
  PageNumber,
  IntroBackground,
  IntroTitleBackground,
  IntroTitle
} = require('./Elements')
const theme = require('./theme')
const { createTheme } = require('../themeUtils')

module.exports = createTheme(theme, {
  ChartMaster: {
    objects: [Logo, CopyRight],
    slideNumber: PageNumber
  },
  IntroMaster: {
    objects: [
      Logo,
      CopyRight,
      IntroBackground,
      IntroTitleBackground,
      IntroTitle
    ]
  }
})
