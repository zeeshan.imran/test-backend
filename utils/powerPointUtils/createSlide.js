module.exports = ({ pptx, settings, theme }, { master = 'CHART_MASTER' } = {}) => {
  let slide = pptx.addSlide(master)

  return { slide, settings, theme }
}
