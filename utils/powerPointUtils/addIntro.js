module.exports = ({ slide }, { title, date }) => {
  slide.addText(
    [
      {
        text: `${title}\n`,
        options: { fontSize: 36, bold: true }
      },
      {
        text: `${date}\n`,
        options: { fontSize: 18 }
      }
    ],
    { placeholder: '(title)' }
  )
}
