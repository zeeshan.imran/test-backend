const addTable = require('./addTable')

module.exports = async (
  { slide, settings },
  { children },
  loc
) => {
  if (children.length > 0) {
    addTable({ slide, settings }, children[0].data, loc)
  }
}
