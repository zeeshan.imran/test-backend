const { toPptLoc } = require('./utils')

module.exports = ({ slide }, data, loc) => {
  const { x, y, w, h } = toPptLoc(loc)

  slide.addImage({
    x,
    y,
    w,
    h,
    data: `image/jpg;base64,${data}`,
    sizing: {
      type: 'contain',
      w,
      h
    }
  })
}
