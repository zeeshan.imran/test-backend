const createSlideWithChartParts = require('./createSlideWithChartParts')

module.exports = async (mainParts, ppt, chart, additionalParts = []) => {
  for (const mainPart of mainParts) {
    const loopChildren = mainPart.data.children
    for (const singleChart of loopChildren) {
      let productName = ''
      if (singleChart.type !== 'table') {
        productName = singleChart.originalProductName
          ? singleChart.originalProductName
          : singleChart.productName
      }
      await createSlideWithChartParts(
        ppt,
        chart,
        [singleChart, ...additionalParts],
        productName
      )
    }
  }
}
