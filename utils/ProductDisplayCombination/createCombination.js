/* globals ProductDisplay, Product */
/**
 * CreateCombination
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const R = require('ramda')
const cloneDeep = require('lodash.clonedeep')
const Combinatorics = require('js-combinatorics')

const reverveProducts = (productIds, productDisplayType, surveyId) => [
  {
    survey: surveyId,
    products: productIds,
    displayType: productDisplayType
  },
  {
    products: cloneDeep(productIds).reverse(),
    displayType: productDisplayType,
    survey: surveyId
  }
]

const permutationProducts = (productIds, productDisplayType, surveyId) => {
  const permutaionList = Combinatorics.permutation(productIds)
  return (
    permutaionList &&
    permutaionList.length &&
    permutaionList.map(productIdsList => ({
      displayType: productDisplayType,
      survey: surveyId,
      products: productIdsList
    }))
  )
}

module.exports = {
  createDisplayCombination: async (
    productIds,
    productDisplayType,
    surveyId
  ) => {
    try {
      const checkProductBrand = await Product.find({
        id: {
          in: productIds
        }
      })
      const getBrands = R.pluck('brand')

      if (R.uniq(getBrands(checkProductBrand)).length > 1) {
        productDisplayType = 'none'
      }
      if (productDisplayType === 'reverse') {
        await ProductDisplay.createEach(
          reverveProducts(productIds, productDisplayType, surveyId)
        )
      } else if (productDisplayType === 'permutation') {
        await ProductDisplay.createEach(
          permutationProducts(productIds, productDisplayType, surveyId)
        )
      } else if (productDisplayType === 'forced') {
        await ProductDisplay.create({
          survey: surveyId,
          products: productIds,
          displayType: productDisplayType
        })
      } else {
        await ProductDisplay.create({
          survey: surveyId,
          products: productIds,
          displayType: productDisplayType
        })
      }
      return true
    } catch (error) {
      return error
    }
  }
}
