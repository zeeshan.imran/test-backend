/* globals sails */
const sharp = require('sharp')
const AWS = require('aws-sdk')
const ApiError = require('./ApiError')

function configureS3 () {
  if (!process.env.BUCKET_NAME) {
    throw new ApiError('BUCKET_NAME is not configured')
  }

  if (!process.env.AWS_ACCESS_KEY_ID) {
    throw new ApiError('AWS_ACCESS_KEY_ID is not configured')
  }

  if (!process.env.AWS_SECRET_ACCESS_KEY) {
    throw new ApiError('AWS_SECRET_ACCESS_KEY is not configured')
  }

  AWS.config = new AWS.Config({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: 'eu-west-1',
    signatureVersion: 'v4'
  })
}

function getObjectKey (url) {
  const match = new RegExp(`${process.env.BUCKET_NAME}/([^$]+)`).exec(url)
  if (!match) {
    return false
  }

  return match[1]
}

async function deleteFile (key) {
  try {
    configureS3()

    const s3 = new AWS.S3()

    await s3
      .deleteObject({
        Bucket: process.env.BUCKET_NAME,
        Key: key
      })
      .promise()
  } catch (ex) {
    throw ex
  }
}

async function uploadFile (key, fileBody, opts = {}) {
  try {
    configureS3()

    const s3 = new AWS.S3()

    await s3
      .upload({
        Bucket: process.env.BUCKET_NAME,
        Key: key,
        CacheControl: new Date().toString(),
        Body: fileBody,
        ...opts
      })
      .promise()

    return `${process.env.BUCKET_URL}/${key}`
  } catch (ex) {
    throw ex
  }
}

async function copyFile (key, newKey) {
  try {
    configureS3()

    const s3 = new AWS.S3()

    const photoObject = await s3
      .getObject({ Bucket: process.env.BUCKET_NAME, Key: key })
      .promise()

    await s3
      .upload({
        Bucket: process.env.BUCKET_NAME,
        Key: newKey,
        CacheControl: new Date().toString(),
        Body: photoObject.Body
      })
      .promise()

    return `${process.env.BUCKET_URL}/${newKey}`
  } catch (ex) {
    throw ex
  }
}

async function resizePhoto (key) {
  try {
    configureS3()

    const s3 = new AWS.S3()

    const photoObject = await s3
      .getObject({ Bucket: process.env.BUCKET_NAME, Key: key })
      .promise()

    sails.log.warn('resizing image')

    const resizedStream = sharp(photoObject.Body).resize(400, 400, {
      fit: 'inside'
    })

    await s3
      .upload({
        Bucket: process.env.BUCKET_NAME,
        Key: key,
        CacheControl: new Date().toString(),
        Body: resizedStream
      })
      .promise()
  } catch (ex) {
    throw ex
  }
}

module.exports = {
  resizePhoto,
  uploadFile,
  deleteFile,
  copyFile,
  getObjectKey
}
