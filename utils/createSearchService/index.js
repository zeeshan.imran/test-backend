/* globals _ */
const createSearchService = (Model, settings = {}) => {
  const db = Model.getDatastore().manager
  const MongoModel = db.collection(Model.tableName)

  let parseOutput = settings && settings.parseOutput
  if (!parseOutput) {
    parseOutput = raw => raw
  }

  async function search ({ query, orderBy, skip, limit }) {
    const cursor = await MongoModel.find(query)
      .skip(skip)
      .limit(limit)
      .sort(orderBy)

    return _.map(await cursor.toArray(), raw => ({
      id: raw._id,
      ...parseOutput(raw)
    }))
  }

  async function count ({ query }) {
    return MongoModel.count(query)
  }

  return { search, count }
}

module.exports = createSearchService
