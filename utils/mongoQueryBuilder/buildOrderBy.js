const _ = require('@sailshq/lodash')

const SORT_DIRECTIONS = {
  ASC: 1,
  DESC: -1
}
const buildOrderBy = input => {
  if (!_.isArray(input)) {
    return buildOrderBy([input])
  }

  const parsedOrderBy = _.reduce(
    input,
    (sort, { orderBy, orderDirection }) => {
      if (orderBy && orderDirection) {
        return {
          ...sort,
          [orderBy]: SORT_DIRECTIONS[orderDirection]
        }
      }

      return sort
    },
    {}
  )

  return _.isEmpty(parsedOrderBy) ? false : parsedOrderBy
}

module.exports = buildOrderBy
