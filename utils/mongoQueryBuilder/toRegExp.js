const _ = require('@sailshq/lodash')

const toRegExp = (value, isEmail = false) => {
  let regex = _.escapeRegExp(value)
    .replace(/^%/, '.*')
    .replace(/([^\\])%/g, '$1.*')
    .replace(/\\%/g, '%')

  if (isEmail) {
    regex = regex.replace(/\s/, '\\+')
  }

  return new RegExp(`^${regex}$`, 'gi')
}

module.exports = toRegExp
