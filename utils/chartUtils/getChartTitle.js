module.exports = async chart => {
  const title = await chart.$(':scope .ant-card-head .ant-card-head-title')
  return title.evaluate(el => el.innerText)
}
