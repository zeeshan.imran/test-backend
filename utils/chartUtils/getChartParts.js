module.exports = async chart => {
  const domParts = await chart.$$(':scope .ant-card-body > *')
  const cache = new Map()

  async function hidePart (domPart) {
    return domPart.evaluate(el => {
      el.style.display = 'none'
    })
  }

  async function showPart (domPart) {
    return domPart.evaluate(
      (el, display) => {
        el.style.display = display
      },
      [cache[domPart].display]
    )
  }

  for (const domPart of domParts) {
    const display = await domPart.evaluate(el => {
      var style = window.getComputedStyle
        ? window.getComputedStyle(el, null)
        : el.currentStyle

      return style.display
    })

    cache[domPart] = {
      display
    }

    await hidePart(domPart)
  }

  const panelBody = await chart.$(':scope .ant-card-body')
  const parts = []
  for (const domPart of domParts) {
    showPart(domPart)
    const part = {
      hide: async () => hidePart(domPart),
      show: async () => showPart(domPart),
      ...(await domPart.evaluate(el => ({
        type: el.getAttribute('data-ppt-type'),
        data: JSON.parse(el.getAttribute('data-ppt-raw')),
        height: el.scrollHeight,
        width: el.scrollWidth
      }))),
      ...(await panelBody.evaluate(el => ({
        height: el.scrollHeight,
        width: el.scrollWidth
      }))),
      getScreenshot: async () => {
        await showPart(domPart)
        const data = await panelBody.screenshot({
          encoding: 'base64',
          type: 'jpeg',
          quality: 100
        })
        await hidePart(domPart)
        return data
      }
    }

    if (
      part.type === 'table-chart' ||
      part.type === 'multi-chart' ||
      part.type === 'fisher-chart' ||
      part.type === 'tukey-chart' ||
      part.type === 'anova-chart' ||
      part.type === 'pca'
    ) {
      const childrenDom = await domPart.$$('[data-ppt-type]')

      const productName = await domPart.evaluate(el => {
        const elProd = el.querySelector('.product-name')
        return elProd && elProd.innerText
      })
      if (part.type === 'pca' || part.type === 'table-chart') {
        part.data.children = await Promise.all(
          childrenDom.map(async childDom => ({
            ...(await childDom.evaluate(
              (el, productName) => ({
                type: el.getAttribute('data-ppt-type'),
                data: JSON.parse(el.getAttribute('data-ppt-raw')),
                height: el.scrollHeight,
                width: el.scrollWidth,
                originalProductName: null,
                productName
              }),
              [productName]
            )),
            getScreenshot: async () => {
              await showPart(domPart)
              const data = await childDom.screenshot({
                encoding: 'base64',
                type: 'jpeg',
                quality: 100
              })
              await hidePart(domPart)
              return data
            }
          }))
        )
      } else {
        part.data.children = await Promise.all(
          childrenDom.map(async childDom => ({
            ...(await childDom.evaluate(
              (el, productName) => ({
                type: el.getAttribute('data-ppt-type'),
                data: JSON.parse(el.getAttribute('data-ppt-raw')),
                height: el.scrollHeight,
                width: el.scrollWidth,
                originalProductName: el.previousSibling.innerText,
                productName
              }),
              [productName]
            )),
            getScreenshot: async () => {
              await showPart(domPart)
              const data = await childDom.screenshot({
                encoding: 'base64',
                type: 'jpeg',
                quality: 100
              })
              await hidePart(domPart)
              return data
            }
          }))
        )
      }
    }

    parts.push(part)
    hidePart(domPart)
  }

  return parts
}
