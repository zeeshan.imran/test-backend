/**
 * @typedef {import('puppeteer').Page} Page
 */

/**
 * @param {Page} page
 */
module.exports = async page => {
  return page.$$(`[id^="chart-view"] .ant-card`)
}
