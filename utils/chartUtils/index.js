module.exports = {
  getCharts: require('./getCharts'),
  getChartTitle: require('./getChartTitle'),
  getChartParts: require('./getChartParts')
}
