/* globals sails */
const rp = require('request-promise')

module.exports = {
  addCreateSurveyInMySql: async survey => {
    if (!survey) {
      return
    }

    const {
      id,
      name,
      title,
      tastingNotes: {
        tastingId,
        tastingLeader,
        customer,
        country,
        dateOfTasting,
        otherInfo
      }
    } = survey

    rp({
      method: 'POST',
      uri: `${sails.config.custom.statsApi}/survey/${id}`,
      body: {
        id,
        name,
        title,
        tastingId,
        tastingLeader,
        customer,
        country,
        dateOfTasting,
        otherInfo
      },
      json: true
    })
  }
}
