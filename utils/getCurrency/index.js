const { COUNTRY_LIST, currencyPrefixes, currencySuffixes } = require('../../utils/constants')
const defaultCurrencies = require('../../utils/defaultCurrencies')

const getCurrency = (availableIn = '') => {
  const availableCountry =
      COUNTRY_LIST.find(country => {
        return country.code === availableIn
      }) || {}
  const prefix = currencyPrefixes[availableCountry.currency] || ''
  const suffix = currencySuffixes[availableCountry.currency] || ''
  return `${prefix}${suffix}`
}

const getCurrencyAlpha = (availableIn = 'US') => {
  return defaultCurrencies[availableIn]
}

module.exports = {
  getCurrency,
  getCurrencyAlpha
}
