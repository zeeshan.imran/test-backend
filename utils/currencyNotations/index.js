const currencyNotations = {
  USD: '$',
  GBP: '£',
  EUR: '€'
}

module.exports = currencyNotations
