/* globals InvalidEmail */
const rp = require('request-promise')

module.exports = async (type, request) => {
  const email = request.body.email || request.body.emailAddress
  const ipAddress =
    request.headers['x-forwarded-for'] || request.connection.remoteAddress

  const data = await rp({
    method: 'GET',
    uri: `https://api.emailverifyapi.com/v3/lookups/json?email=${email}&key=${process.env.EMAIL_CHECKER_KEY}`,
    json: true
  })

  if (
    data.address &&
    (!data.validFormat || !data.deliverable || !data.hostExists)
  ) {
    await InvalidEmail.create({
      type,
      email,
      ipAddress,
      emailCheckerData: data
    })

    return false
  }

  return true
}
