const fs = require('fs')
const util = require('util')
const access = util.promisify(fs.access)
const mkdir = util.promisify(fs.mkdir)

const exists = async path => {
  try {
    await access(path)
    return true
  } catch (ex) {
    return false
  }
}

const createPdfFolder = async () => {
  const assets = './assets'
  if (!(await exists(assets))) {
    await mkdir(assets)
  }
  const pdfFolder = './assets/pdf'
  if (!(await exists(pdfFolder))) {
    await mkdir(pdfFolder)
  }
}

const pdfCreation = async surveyId => {
  await createPdfFolder()
  return `assets/pdf/${surveyId}.pdf`
}

const createPptFolder = async () => {
  const assets = './assets'
  if (!(await exists(assets))) {
    await mkdir(assets)
  }
  const pptFolder = './assets/ppt'
  if (!(await exists(pptFolder))) {
    await mkdir(pptFolder)
  }
}
const pptCreation = async surveyId => {
  await createPptFolder()
  return `assets/ppt/${surveyId}.pptx`
}

module.exports = { pdfCreation, pptCreation }
