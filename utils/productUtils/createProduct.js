/* globals Product, Brand */
const s3Utils = require('../../utils/s3Utils')

module.exports = async product => {
  const {
    name,
    photo,
    brand: brandName,
    reward,
    isAvailable,
    isSurveyCover,
    sortingOrderId,
    rejectedReward,
    delayToNextProduct,
    extraDelayToNextProduct,
    amountInDollar,
    internalName: intName
  } = product

  const internalName = intName || ''

  const brand = await Brand.findOrCreate(
    { name: brandName },
    { name: brandName }
  )

  // NOTE: STAGFW-455 tested
  const createdProduct = await Product.create({
    name,
    photo,
    brand: brand.id,
    reward,
    rejectedReward,
    delayToNextProduct,
    extraDelayToNextProduct,
    isAvailable,
    isSurveyCover: Boolean(isSurveyCover),
    sortingOrderId,
    amountInDollar,
    internalName
  }).fetch()

  const s3Key = s3Utils.getObjectKey(photo)
  if (s3Key) {
    await s3Utils.resizePhoto(s3Key)
  }

  return createdProduct
}
