/* globals Product, Brand */
const s3Utils = require('../../utils/s3Utils')

const brandFindOrCreate = async (name) => {
  const brands = await Brand.find({ where: { name } })

  if (brands.length > 0) {
    return brands[0]
  }

  const brand = await Brand.create({ name }).fetch()
  return brand
}
module.exports = async (id, productInput) => {
  const {
    name,
    photo,
    brand: brandName,
    reward,
    rejectedReward,
    isAvailable,
    isSurveyCover,
    sortingOrderId,
    amountInDollar,
    internalName: intName,
    delayToNextProduct, 
    extraDelayToNextProduct
  } = productInput

  const internalName = intName || ''
  const brand = await brandFindOrCreate(brandName)

  // TODO: STAGFW-455 tested
  const product = await Product.findOne({ id })
  const shouldResizePhoto = product.photo !== photo

  if (shouldResizePhoto) {
    const s3Key = s3Utils.getObjectKey(photo)
    if (s3Key) {
      await s3Utils.resizePhoto(s3Key)
    }
  }

  const updatedProduct = await Product.updateOne({
    id
  }).set({
    name,
    photo,
    brand: brand.id,
    reward,
    rejectedReward,
    isAvailable,
    isSurveyCover: Boolean(isSurveyCover),
    sortingOrderId,
    amountInDollar,
    internalName,
    delayToNextProduct, 
    extraDelayToNextProduct
  })

  return updatedProduct
}
