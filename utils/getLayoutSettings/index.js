/**
 * This function is used to get clone of settings of survey while
 * filtering out the linked survey layout settings and return array.
 *
 * @private
 * @param {Function} getNewId The `function` that checks in the current set of questions.
 * @param {Array} pages The provided page of settings.
 * @returns {Array} Returns the new array of composed arguments.
 */
const TYPE_BANNER = 'banner'
const getLayoutSettings = async (getNewId, pages) => {
  let updatedPages = []
  await pages.map(async (item, index) => {
    // outer loop
    updatedPages[index] = []
    await item.map((element, innerIndex) => {
      // inner loop of pages
      const newValue = element['i'].split('-')
      const clonedQuestionId = getNewId(newValue[0])
      if (newValue.length === 1 && clonedQuestionId) {
        updatedPages[index][innerIndex] = {
          ...element,
          ...{ i: clonedQuestionId }
        }
        return element
      } else if (newValue.length === 2 && clonedQuestionId) {
        updatedPages[index][innerIndex] = {
          ...element,
          ...{ i: `${clonedQuestionId}-${newValue[1]}` }
        }
        return element
      } else if (newValue[0] === TYPE_BANNER) {
        updatedPages[index][innerIndex] = {
          ...element,
          ...{ i: newValue[0] }
        }
        return element
      }
      return element
    })

    return item
  })

  const filterPageArray = updatedPages.filter(e => e.length)
  return filterPageArray
}

module.exports = getLayoutSettings
