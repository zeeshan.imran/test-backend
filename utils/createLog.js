/* globals Logs */
const ignoreQuestions = ['profile']

const ignoreFields = [
  '_id',
  'id',
  'createdAt',
  'updatedAt',
  'editMode',
  'optionId',
  '__typename',
  'relatedQuestions',
  'nextQuestion',
  'nextQuestionPrompt'
]

const pureCompareFields = [
  'linkedSurveys',
  'numericOptions',
  'pairsOptions',
  'skipFlow',
  'sliderOptions',
  'matrixOptions'
]

const isExist = (data) => {
  return data !== undefined && data !== null
}

const isExistField = (data, field) => {
  return isExist(data) && isExist(data[field])
}

const isObject = (data) => {
  return typeof data === 'object'
}

const differenceBetween = (oldData, newData) => {
  const differences = []

  const compare = async (field, oldValue, newValue) => {
    if (oldValue !== newValue) {
      differences.push({
        field: field,
        oldValue: oldValue,
        newValue: newValue
      })
    }
  }

  const examine = {
    array: (oldValue, newValue, fieldPath) => {
      const length =
        oldValue.length > newValue.length ? oldValue.length : newValue.length

      for (let i = 0; i < length; i++) {
        if (isObject(oldValue[i]) || isObject(newValue[i])) {
          examine.object(oldValue[i], newValue[i], fieldPath)
        } else {
          compare(fieldPath, oldValue[i], newValue[i])
        }
      }
    },
    object: (oldValue, newValue, fieldPath) => {
      if (
        (oldValue && oldValue.value === 'other') ||
        (newValue && newValue.value === 'other')
      ) {
        const oldOpenAnswer =
          oldValue && oldValue.isOpenAnswer ? 'true' : 'false'
        const newOpenAnswer =
          newValue && newValue.isOpenAnswer ? 'true' : 'false'
        compare('isOpenAnswer', oldOpenAnswer, newOpenAnswer)
        return
      }

      const keys = []
      if (oldValue) {
        keys.push(...Object.keys(oldValue))
      }
      if (newValue) {
        keys.push(...Object.keys(newValue))
      }
      const uniqueKeys = Array.from(new Set(keys))

      for (let i = 0; i < uniqueKeys.length; i++) {
        const key = uniqueKeys[i]
        examine.property(key, oldValue, newValue, fieldPath)
      }
    },
    property: (field, oldValue, newValue, fieldPath) => {
      const nextFieldPath = fieldPath ? `${fieldPath}.${field}` : field

      if (ignoreFields.includes(field)) {
        return
      }

      if (pureCompareFields.includes(field)) {
        const strOldValue = JSON.stringify((oldValue && oldValue[field]) || {})
        const strNewValue = JSON.stringify((newValue && newValue[field]) || {})

        if (strOldValue !== strNewValue) {
          differences.push({
            field: nextFieldPath,
            oldValue: '',
            newValue: ''
          })
        }
        return
      }

      if (!isExistField(oldValue, field) && isExistField(newValue, field)) {
        if (Array.isArray(newValue[field])) {
          examine.array([], newValue[field], nextFieldPath)
          return
        }

        if (isObject(newValue[field])) {
          examine.object(null, newValue[field], nextFieldPath)
          return
        }

        differences.push({
          field: nextFieldPath,
          oldValue:
            oldValue && typeof oldValue[field] === 'boolean'
              ? oldValue[field].toString()
              : null,
          newValue: newValue[field]
        })
        return
      }

      if (isExistField(oldValue, field) && !isExistField(newValue, field)) {
        if (Array.isArray(oldValue[field])) {
          examine.array(oldValue[field], [], nextFieldPath)
          return
        }

        if (isObject(oldValue[field])) {
          examine.object(oldValue[field], null, nextFieldPath)
          return
        }

        differences.push({
          field: nextFieldPath,
          oldValue: oldValue[field],
          newValue:
            newValue && typeof newValue[field] === 'boolean'
              ? newValue[field].toString()
              : null
        })
        return
      }

      if (isExistField(oldValue, field) && isExistField(newValue, field)) {
        if (Array.isArray(oldValue[field]) || Array.isArray(newValue[field])) {
          examine.array(oldValue[field], newValue[field], nextFieldPath)
          return
        }

        if (isObject(oldValue[field]) || isObject(newValue[field])) {
          examine.object(oldValue[field], newValue[field], nextFieldPath)
          return
        }

        compare(nextFieldPath, oldValue[field], newValue[field])
      }
    }
  }

  examine.object(oldData, newData, '')

  const orderDifference = differences.find(difference => {
    return difference.field === 'order'
  })

  if (orderDifference) {
    return differences.filter(difference => {
      return difference.field !== 'skipFlow'
    })
  }

  return differences
}

module.exports = {
  survey: async (userId, surveyId, oldData, newData) => {
    if (!oldData && newData) {
      await Logs.create({
        by: userId,
        action: 'create',
        type: 'create-survey',
        relatedSurvey: surveyId,
        collections: ['survey'],
        values: newData
      })
    }

    if (oldData && !newData) {
      await Logs.create({
        by: userId,
        action: 'delete',
        type: 'delete-survey',
        relatedSurvey: surveyId,
        collections: ['survey'],
        values: oldData
      })
    }

    if (oldData && newData) {
      const differences = differenceBetween(oldData, newData)

      if (differences.length > 0) {
        await Logs.create({
          by: userId,
          action: 'update',
          type: 'update-survey',
          relatedSurvey: surveyId,
          collections: ['survey'],
          values: differences
        })
      }
    }
  },
  product: async (userId, surveyId, oldData, newData) => {
    if (!oldData && newData) {
      await Logs.create({
        by: userId,
        action: 'create',
        type: `create-product`,
        relatedSurvey: surveyId,
        relatedProduct: newData.id,
        collections: ['survey', 'product'],
        values: newData
      })
    }

    if (oldData && !newData) {
      await Logs.create({
        by: userId,
        action: 'delete',
        type: `delete-product`,
        relatedSurvey: surveyId,
        relatedProduct: oldData.id,
        collections: ['survey', 'product'],
        values: oldData
      })
    }

    if (oldData && newData) {
      const differences = differenceBetween(oldData, newData)

      if (differences.length > 0) {
        await Logs.create({
          by: userId,
          action: 'update',
          type: `update-product`,
          relatedSurvey: surveyId,
          relatedProduct: newData.id,
          collections: ['survey', 'product'],
          values: differences
        })
      }
    }
  },
  question: async (userId, surveyId, oldData, newData) => {
    if (
      (oldData && ignoreQuestions.includes(oldData.typeOfQuestion)) ||
      (newData && ignoreQuestions.includes(newData.typeOfQuestion))
    ) {
      return
    }

    if (oldData && oldData.nextQuestion) {
      oldData.nextQuestionPrompt = oldData.nextQuestion.prompt
    }

    if (newData && newData.nextQuestion) {
      newData.nextQuestionPrompt = newData.nextQuestion.prompt
    }

    if (!oldData && newData) {
      await Logs.create({
        by: userId,
        action: 'create',
        type: `create-question`,
        relatedSurvey: surveyId,
        relatedQuestion: newData.id,
        collections: ['survey', 'question'],
        values: newData
      })
    }

    if (oldData && !newData) {
      await Logs.create({
        by: userId,
        action: 'delete',
        type: `delete-question`,
        relatedSurvey: surveyId,
        relatedQuestion: oldData.id,
        collections: ['survey', 'question'],
        values: oldData
      })
    }

    if (oldData && newData) {
      const differences = differenceBetween(oldData, newData)

      if (differences.length > 0) {
        await Logs.create({
          by: userId,
          action: 'update',
          type: `update-question`,
          relatedSurvey: surveyId,
          relatedQuestion: newData.id,
          collections: ['survey', 'question'],
          values: differences
        })
      }
    }
  }
}
