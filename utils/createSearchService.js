/* globals _, sails */
const R = require('ramda')

const removeFalsyFromArray = R.filter(R.identity)

/**
 * PLEASE DON'T PUT BUSINESS LOGIC HERE
 * createSearchService is a factory function to create searchService.
 * This function returns two functions:
 *   - count: take responsibility to count documents that matched the query
 *   - search: take responsibility to search documents that matched the query
 *
 * @param {*} Model
 * @param {*} settings
 */
const createSearchService = (Model, settings = {}) => {
  const db = Model.getDatastore().manager
  const Collection = db.collection(Model.tableName)

  let parseOutput = settings && settings.parseOutput
  if (!parseOutput) {
    parseOutput = () => raw => raw
  }

  async function search (pipeline, searchInputParams = {}) {
    try {
      let { orderBy, skip, limit } = searchInputParams

      let $limit = 10
      if (limit === 0) {
        $limit = Number.MAX_SAFE_INTEGER
      } else if (limit > 0) {
        $limit = limit
      }

      const cursor = await Collection.aggregate(
        removeFalsyFromArray([
          ...pipeline,
          orderBy && {
            $sort: orderBy
          },
          { $skip: skip || 0 },
          { $limit }
        ])
      )

      const parseRawOutput = parseOutput(searchInputParams)

      const docs = await cursor.toArray()

      return Promise.all(
        _.map(docs, async raw => ({
          id: raw._id,
          ...(await parseRawOutput(raw))
        }))
      )
    } catch (ex) {
      sails.log.error(ex.toString())
      throw ex
    }
  }

  async function count (pipeline) {
    try {
      const cursor = await Collection.aggregate([
        ...pipeline,
        {
          $count: 'aggregate_count'
        }
      ])

      const docs = await cursor.toArray()

      return docs.length ? docs[0]['aggregate_count'] : 0
    } catch (ex) {
      sails.log.error(ex.toString())
      throw ex
    }
  }
  /*
  TODO: please remove if this code is not used, otherwise move it to the other file
  async function singleSurvey (surveyId) {
    const aggregateArray = [{ $match: { _id: ObjectId(surveyId) } }]
    aggregateArray.push({
      $lookup: {
        from: 'question',
        localField: '_id',
        foreignField: 'survey',
        as: 'questions'
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'product_surveys__survey_products',
        localField: '_id',
        foreignField: 'survey_products',
        as: 'products'
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'product',
        localField: 'products.product_surveys',
        foreignField: '_id',
        as: 'products'
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'survey_exclusiveTasters__user_exclusiveSurveys',
        localField: '_id',
        foreignField: 'survey_exclusiveTasters',
        as: 'exclusiveTasters'
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'user',
        localField: 'exclusiveTasters.user_exclusiveSurveys',
        foreignField: '_id',
        as: 'exclusiveTasters'
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'organization',
        localField: 'owner',
        foreignField: '_id',
        as: 'owner'
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'user',
        localField: 'creator',
        foreignField: '_id',
        as: 'creator'
      }
    })
    aggregateArray.push({
      $lookup: {
        from: 'user',
        localField: 'lastModifier',
        foreignField: '_id',
        as: 'lastModifier'
      }
    })
    aggregateArray.push({
      $unwind: { path: '$owner', preserveNullAndEmptyArrays: true }
    })
    aggregateArray.push({
      $unwind: { path: '$creator', preserveNullAndEmptyArrays: true }
    })
    aggregateArray.push({
      $unwind: { path: '$lastModifier', preserveNullAndEmptyArrays: true }
    })
    const cursor = await Collection.aggregate(aggregateArray)

    let docs = await cursor.toArray()
    if (docs) {
      docs = JSON.parse(JSON.stringify(docs[0]).replace(/_id/g, 'id'))
    }

    return docs
  }
  */

  return { search, count }
}

module.exports = createSearchService
