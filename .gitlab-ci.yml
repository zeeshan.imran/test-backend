stages:
  - install_deps
  - test
  - deploy
  - scale

image: node:lts

.job_base:
  artifacts:
    expire_in: 2 hrs
    paths:
      - node_modules/

install_deps:
  artifacts:
    expire_in: 2 hrs
    paths:
      - node_modules/
  script:
    - npm install
    - git diff # show diffs to make sure package-lock.json is not changed
  stage: install_deps
  only:
    - merge_requests
    - master
    - blc-master
    - hansen-master
    - develop
    - staging
    - staging1
    - staging2
    - staging3
    - staging4
    - staging5
    - staging6
    - staging7
    - staging8
    - staging9
    - staging10

test:
  extends: .job_base
  variables:
    BUCKET_NAME: flavorwiki-storage
    BUCKET_URL: https://s3-eu-west-1.amazonaws.com/flavorwiki-storage
    AWS_ACCESS_KEY_ID: test-id
    AWS_SECRET_ACCESS_KEY: test-key
    REACT_APP_THEME: default
  script:
    - npm run test -- --coverage
  coverage: /All files\s*\|\s*([\d\.]+)/
  stage: test
  only:
    - merge_requests
    - master
    - blc-master
    - hansen-master
    - develop
    - staging
    - staging1
    - staging2
    - staging3
    - staging4
    - staging5
    - staging6
    - staging7
    - staging8
    - staging9
    - staging10

deploy:staging:
  extends: .job_base
  variables:
    SSH_PRIVATE_KEY: ${STAGING_PRIVATE_KEY}
    SSH_KNOWN_HOSTS: ${KNOWN_HOSTS}
    SSH_USER: ${STAGING_USER}
    SSH_HOST: ${STAGING_HOST}
    DEPLOY_PATH: /home/deploy/react_app/$CI_COMMIT_REF_NAME/backend
    REACT_APP_THEME: default
  script:
    - echo "deploy on ${SSH_USER}@${SSH_HOST}..."
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - npm install -g pm2
    - pm2 deploy staging.config.js $CI_COMMIT_REF_NAME setup 2>&1 || true
    - pm2 deploy staging.config.js $CI_COMMIT_REF_NAME --force
  stage: deploy
  #when: manual
  only:
    - staging
    - staging1
    - staging2
    - staging3
    - staging4
    - staging6
    - staging8
    - staging9
    - staging10

deploy:stagingHansen:
  extends: .job_base
  variables:
    SSH_PRIVATE_KEY: ${STAGING_PRIVATE_KEY}
    SSH_KNOWN_HOSTS: ${KNOWN_HOSTS}
    SSH_USER: ${STAGING_USER}
    SSH_HOST: ${STAGING_HOST}
    DEPLOY_PATH: /home/deploy/react_app/$CI_COMMIT_REF_NAME/backend
    REACT_APP_THEME: chrHansen
  script:
    - echo "deploy on ${SSH_USER}@${SSH_HOST}..."
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - npm install -g pm2
    - pm2 deploy staging.config.js $CI_COMMIT_REF_NAME setup 2>&1 || true
    - pm2 deploy staging.config.js $CI_COMMIT_REF_NAME --force
  stage: deploy
  #when: manual
  only:
    - staging5

deploy:stagingBungee:
  extends: .job_base
  variables:
    SSH_PRIVATE_KEY: ${STAGING_PRIVATE_KEY}
    SSH_KNOWN_HOSTS: ${KNOWN_HOSTS}
    SSH_USER: ${STAGING_USER}
    SSH_HOST: ${STAGING_HOST}
    DEPLOY_PATH: /home/deploy/react_app/$CI_COMMIT_REF_NAME/backend
    REACT_APP_THEME: bunge
  script:
    - echo "deploy on ${SSH_USER}@${SSH_HOST}..."
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - npm install -g pm2
    - pm2 deploy staging.config.js $CI_COMMIT_REF_NAME setup 2>&1 || true
    - pm2 deploy staging.config.js $CI_COMMIT_REF_NAME --force
  stage: deploy
  #when: manual
  only:
    - staging7

scale:staging:
  variables:
    SSH_PRIVATE_KEY: ${STAGING_PRIVATE_KEY}
    SSH_KNOWN_HOSTS: ${KNOWN_HOSTS}
    SSH_USER: ${STAGING_USER}
    SSH_HOST: ${STAGING_HOST}
  script:
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - ssh -tt $SSH_USER@$SSH_HOST 'pm2 scale fw-backend-staging 3' || true
  stage: scale
  only:
    - staging

deploy:production:
  extends: .job_base
  variables:
    SSH_PRIVATE_KEY: ${PROD_PRIVATE_KEY}
    SSH_KNOWN_HOSTS: ${KNOWN_HOSTS}
    SSH_USER: ${PROD_USER}
    SSH_HOST: ${PROD_HOST}
    ENV_PATH: /home/deploy/react_app
    DEPLOY_PATH: /home/deploy/react_app/$CI_COMMIT_REF_NAME/backend
    REACT_APP_THEME: default
  script:
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - npm install -g pm2
    - pm2 deploy production.config.js $CI_COMMIT_REF_NAME setup 2>&1 || true
    - pm2 deploy production.config.js $CI_COMMIT_REF_NAME --force
  stage: deploy
  #when: manual
  only:
    - master

deploy:hansen:
  extends: .job_base
  variables:
    SSH_PRIVATE_KEY: ${HANSEN_PRIVATE_KEY}
    SSH_KNOWN_HOSTS: ${KNOWN_HOSTS}
    SSH_USER: ${HANSEN_USER}
    SSH_HOST: ${HANSEN_HOST}
    ENV_PATH: /home/deploy/react_app
    DEPLOY_PATH: /home/deploy/react_app/$CI_COMMIT_REF_NAME/backend
    REACT_APP_THEME: chrHansen
  script:
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - npm install -g pm2
    - pm2 deploy production.config.js $CI_COMMIT_REF_NAME setup 2>&1 || true
    - pm2 deploy production.config.js $CI_COMMIT_REF_NAME --force
  stage: deploy
  only:
    - hansen-master

deploy:bunge:
  extends: .job_base
  variables:
    SSH_PRIVATE_KEY: ${BUNGE_PRIVATE_KEY}
    SSH_KNOWN_HOSTS: ${KNOWN_HOSTS}
    SSH_USER: ${BUNGE_USER}
    SSH_HOST: ${BUNGE_HOST}
    ENV_PATH: /home/deploy/react_app
    DEPLOY_PATH: /home/deploy/react_app/$CI_COMMIT_REF_NAME/backend
    REACT_APP_THEME: bunge
  script:
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | ssh-add -
    - mkdir -p ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    - npm install -g pm2
    - pm2 deploy production.config.js $CI_COMMIT_REF_NAME setup 2>&1 || true
    - pm2 deploy production.config.js $CI_COMMIT_REF_NAME --force
  stage: deploy
  only:
    - blc-master
