/**
 * isSuperAdminOrPowerUser
 *
 * A simple policy that blocks requests from non-super-admins.
 *
 * For more about how to use policies, see:
 *   https://sailsjs.com/config/policies
 *   https://sailsjs.com/docs/concepts/policies
 *   https://sailsjs.com/docs/concepts/policies/access-control-and-permissions
 */
module.exports = function (req, res, next) {
  const { user, superUser } = req.token
  const allowTypes = ['operator', 'power-user']

  if (superUser && (superUser.isSuperAdmin || allowTypes.includes(superUser.type))) {
    return next()
  }

  if (user.isSuperAdmin || allowTypes.includes(user.type)) {
    return next()
  }

  return res.forbidden()
}
