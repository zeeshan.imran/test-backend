/* globals jwToken, User, process */

module.exports = function (req, res, next) {
  let token
  if (req.headers && req.headers.authorization) {
    let parts = req.headers.authorization.split(' ')
    if (parts.length === 2) {
      let scheme = parts[0]
      let credentials = parts[1]
      if (/^Bearer$/i.test(scheme)) {
        token = credentials
      }
    } else {
      return res.badRequest('Wrong Auth Format')
    }
  } else if (req.param('token')) {
    token = req.param('token')
    // We delete the token from param to not mess with blueprints
    delete req.query.token
  } else {
    return res.badRequest('No Authorization Header Found')
  }

  jwToken.verify(token, (err, user) => {
    if (err) return res.badRequest('Invalid token')
    /* istanbul ignore next */
    if (!user) return res.badRequest('User not found')
    if (user.apiVersion !== process.env.USER_API_VERSION) {
      return res.badRequest('User API version outdated')
    }

    if (user.isSuperAdmin) {
      if (req.headers.impersonate) {
        User.findOne({
          id: req.headers.impersonate
        }).exec((err, impersonateUser) => {
          if (err) return res.forbidden('Impersonate user not found')
          if (!impersonateUser)
            return res.forbidden('Impersonate user not found')
          req.mainToken = token
          req.token = { superUser: user, user: impersonateUser }
          next()
        })
        return
      }
    }
    req.mainToken = token
    req.token = { user }
    User.findOne({ id: user.id }).exec((err, result) => {
      if (err) return res.badRequest('Invalid token')
      if (!result) return res.badRequest('Invalid token')

      next()
    })
  })
}
