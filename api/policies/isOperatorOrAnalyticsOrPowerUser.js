module.exports = function (req, res, next) {
  const { user } = req.token
  const allowTypes = ['operator', 'analytics', 'power-user']

  if (allowTypes.includes(user.type)) {
    return next()
  }

  res.forbidden('Not authorized')
}
