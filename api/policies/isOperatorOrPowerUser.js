module.exports = function (req, res, next) {
  const { user } = req.token
  const allowTypes = ['operator', 'power-user']

  if (allowTypes.includes(user.type)) {
    return next()
  }

  res.forbidden('Not authorized')
}
