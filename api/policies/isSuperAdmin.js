/**
 * is-super-admin
 *
 * A simple policy that blocks requests from non-super-admins.
 *
 * For more about how to use policies, see:
 *   https://sailsjs.com/config/policies
 *   https://sailsjs.com/docs/concepts/policies
 *   https://sailsjs.com/docs/concepts/policies/access-control-and-permissions
 */
module.exports = function (req, res, proceed) {
  if (!req.token) {
    return res.unauthorized('Not authorized')
  }

  const { user } = req.token

  if (!user.isSuperAdmin) {
    return res.forbidden('Not authorized')
  }
  
  return proceed()
  
}
