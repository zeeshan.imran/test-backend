module.exports = function (req, res, next) {
  const { user } = req.token

  if (user.organization) {
    req.token.organization = user.organization
    return next()
  }

  res.forbidden('Not authorized')
}
