/* globals Organization */
const R = require('ramda')
const createSearchService = require('../../../utils/createSearchService')
const queryBuilder = require('../../../utils/mongoQueryBuilder')

const removeFalsyFromArray = R.filter(R.identity)
const createOrganizationSearchPipeline = ({ type, keyword, inactive = false }) => {
  let $match = {
    inactive: inactive
  }

  if (keyword) {
    const keywordAsInt = parseInt(keyword, 10)
    $match = {
      ...$match,
      $or: removeFalsyFromArray([
        { name: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } },
        {
          'modifiedBy.emailAddress': {
            $regex: queryBuilder.toRegExp(`%${keyword}%`, true)
          }
        },
        {
          'modifiedBy.fullName': {
            $regex: queryBuilder.toRegExp(`%${keyword}%`)
          }
        },
        {
          'createdBy.emailAddress': {
            $regex: queryBuilder.toRegExp(`%${keyword}%`, true)
          }
        },
        {
          'createdBy.fullName': {
            $regex: queryBuilder.toRegExp(`%${keyword}%`)
          }
        },
        !isNaN(keywordAsInt) && { usersNumber: keywordAsInt }
      ])
    }
  }

  let matchValidUser = {
    inactive: false,
    $expr: {
      $eq: ['$organization', '$$orgId']
    }
  }
  if (type && type.length) {
    matchValidUser = {
      type: { $in: type },
      ...matchValidUser
    }
  }

  const lookupUsersNumber = [
    {
      $lookup: {
        from: 'user',
        let: { orgId: '$_id' },
        pipeline: [
          {
            $match: matchValidUser
          },
          { $project: { id: '$_id' } }
        ],
        as: 'members'
      }
    },
    { $addFields: { usersNumber: { $size: ['$members'] } } },
    { $unset: 'members' }
  ]

  const lookupCreatedBy = [
    {
      $lookup: {
        from: 'user',
        let: { userId: '$createdBy' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$userId']
              }
            }
          },
          {
            $project: {
              emailAddress: '$emailAddress',
              fullName: '$fullName',
              id: '$_id'
            }
          }
        ],
        as: 'createdBy'
      }
    },
    {
      $unwind: {
        path: '$createdBy',
        preserveNullAndEmptyArrays: true
      }
    }
  ]

  const lookupModifiedBy = [
    {
      $lookup: {
        from: 'user',
        let: { userId: '$modifiedBy' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$userId']
              }
            }
          },
          {
            $project: {
              emailAddress: '$emailAddress',
              fullName: '$fullName',
              id: '$_id'
            }
          }
        ],
        as: 'modifiedBy'
      }
    },
    {
      $unwind: {
        path: '$modifiedBy',
        preserveNullAndEmptyArrays: true
      }
    }
  ]

  return [
    ...lookupUsersNumber,
    ...lookupCreatedBy,
    ...lookupModifiedBy,
    { $match }
  ]
}

const organizationSearch = createSearchService(Organization)

module.exports = {
  organizationSearch,
  createOrganizationSearchPipeline,
  createOrganizationSearchOrderBy: queryBuilder.buildOrderBy
}
