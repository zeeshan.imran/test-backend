/* globals Logs */
const ObjectId = require('mongodb').ObjectID
const R = require('ramda')
const createSearchService = require('../../../utils/createSearchService')
const queryBuilder = require('../../../utils/mongoQueryBuilder')

const removeFalsyFromArray = R.filter(R.identity)
const createLogsSearchPipeline = ({ surveyId, keyword }) => {
  let $match = {}

  if (surveyId) {
    $match = {
      type: { $ne: 'photo-validation' },
      relatedSurvey: ObjectId(surveyId)
    }
  }

  if (keyword) {
    $match = {
      ...$match,
      $or: removeFalsyFromArray([
        {
          type: {
            $regex: queryBuilder.toRegExp(`%${keyword}%`)
          }
        },
        {
          'by.emailAddress': {
            $regex: queryBuilder.toRegExp(`%${keyword}%`, true)
          }
        },
        {
          'by.fullName': {
            $regex: queryBuilder.toRegExp(`%${keyword}%`)
          }
        }
      ])
    }
  }

  const lookup = [
    {
      $lookup: {
        from: 'user',
        let: { userId: '$by' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$userId']
              }
            }
          },
          {
            $project: {
              id: '$_id',
              fullName: '$fullName',
              emailAddress: '$emailAddress'
            }
          }
        ],
        as: 'by'
      }
    },
    {
      $unwind: {
        path: '$by',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'product',
        let: { productId: '$relatedProduct' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$productId']
              }
            }
          },
          {
            $project: {
              id: '$_id',
              name: '$name',
              photo: '$photo'
            }
          }
        ],
        as: 'relatedProduct'
      }
    },
    {
      $unwind: {
        path: '$relatedProduct',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'surveyenrollment',
        let: { enrollmentId: '$relatedEnrollment' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$enrollmentId']
              }
            }
          },
          {
            $project: {
              id: '$_id',
              state: '$state'
            }
          }
        ],
        as: 'relatedEnrollment'
      }
    },
    {
      $unwind: {
        path: '$relatedEnrollment',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'question',
        let: { questionId: '$relatedQuestion' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$questionId']
              }
            }
          },
          {
            $project: {
              id: '$_id',
              prompt: '$prompt',
              typeOfQuestion: '$typeOfQuestion'
            }
          }
        ],
        as: 'relatedQuestion'
      }
    },
    {
      $unwind: {
        path: '$relatedQuestion',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: 'answer',
        let: { answerId: '$relatedAnswer' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$answerId']
              }
            }
          },
          {
            $project: {
              id: '$_id',
              value: '$value',
              timeToAnswer: '$timeToAnswer'
            }
          }
        ],
        as: 'relatedAnswer'
      }
    },
    {
      $unwind: {
        path: '$relatedAnswer',
        preserveNullAndEmptyArrays: true
      }
    }
  ]

  return [...lookup, { $match }]
}

const logsSearch = createSearchService(Logs)

module.exports = {
  logsSearch,
  createLogsSearchPipeline,
  createLogsSearchOrderBy: queryBuilder.buildOrderBy
}
