/* eslint-disable node/no-extraneous-require */
/* globals User */
const R = require('ramda')
const createSearchService = require('../../../utils/createSearchService')
const queryBuilder = require('../../../utils/mongoQueryBuilder')
const ObjectId = require('mongodb').ObjectID

const removeFalsyFromArray = R.filter(R.identity)
const userSearch = createSearchService(User)

const createUserSearchPipeline = ({ organization, type, keyword, inactive = false }) => {
  let $addFields = {
    types: {
      $cond: {
        if: { $eq: ['$isSuperAdmin', true] },
        then: 'super-admin',
        else: '$type'
      }
    }
  }

  let $match = {
    inactive: inactive
  }

  if (type) {
    $match = {
      ...$match,
      type: { $in: type }
    }
  }

  if (organization) {
    $match = {
      ...$match,
      'organization.id': ObjectId(organization)
    }
  }

  if (keyword) {
    $match = {
      ...$match,
      $or: removeFalsyFromArray([
        { fullName: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } },
        {
          emailAddress: { $regex: queryBuilder.toRegExp(`%${keyword}%`, true) }
        },
        {
          types: { $regex: queryBuilder.toRegExp(`%${keyword}%`) }
        },
        !organization && {
          'organization.name': { $regex: queryBuilder.toRegExp(`%${keyword}%`) }
        }
      ])
    }
  }

  const lookupOrganization = [
    {
      $lookup: {
        from: 'organization',
        let: { organizationId: '$organization' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$_id', '$$organizationId']
              }
            }
          },
          {
            $project: {
              id: '$_id',
              name: '$name'
            }
          }
        ],
        as: 'organization'
      }
    },
    {
      $unwind: {
        path: '$organization',
        preserveNullAndEmptyArrays: true
      }
    }
  ]

  return [...lookupOrganization, { $addFields }, { $match }]
}

module.exports = {
  userSearch,
  createUserSearchPipeline,
  createUserSearchOrderBy: queryBuilder.buildOrderBy
}
