/* globals Survey */
const R = require('ramda')
// eslint-disable-next-line node/no-extraneous-require
const ObjectId = require('mongodb').ObjectID
const createSearchService = require('../../../utils/createSearchService')
const queryBuilder = require('../../../utils/mongoQueryBuilder')

const removeFalsyFromArray = R.filter(R.identity)
const surveySearch = createSearchService(Survey)

const createSurveySearchPipeline = (
  {
    state,
    owner,
    userId,
    country,
    keyword,
    omitScreeners = false,
    omitUsedTastings = '',
    userType,
    authorizationType = 'selected',
    showAllSurveys = true,
    parentId = false,
    onlyShared = false
  },
  { populateProducts = true } = {}
) => {
  const $matchKeyword = keyword && {
    $or: [
      { name: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } },
      { uniqueName: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } }
    ]
  }

  const $matchOwner = {
    $or: removeFalsyFromArray([
      !!owner && userType !== 'analytics' && !onlyShared && { owner: ObjectId(owner) },
      {
        sharedStatsUsers: ObjectId(userId)
      }
    ])
  }

  const lookupProducts = [
    {
      $lookup: {
        from: 'product_surveys__survey_products',
        localField: '_id',
        foreignField: 'survey_products',
        as: 'products'
      }
    },
    {
      $addFields: {
        products: {
          $map: {
            input: '$products',
            in: '$$this.product_surveys'
          }
        }
      }
    }
  ]

  const lookupSharedStatsUsers = {
    beforeMatch: [
      {
        $lookup: {
          from: 'survey_sharedStatsUsers__user_sharedStatsSurveys',
          localField: '_id',
          foreignField: 'survey_sharedStatsUsers',
          as: 'sharedStatsUsers'
        }
      },
      {
        $addFields: {
          sharedStatsUsers: {
            $map: {
              input: '$sharedStatsUsers',
              in: '$$this.user_sharedStatsSurveys'
            }
          }
        }
      }
    ]
  }

  let $matchBy = {
    state: state || { $ne: 'deprecated' }
  }

  if (parentId) { $matchBy = { folder: parentId } }

  if (omitScreeners) {
    $matchBy = {
      state: state || { $ne: 'deprecated' },
      isScreenerOnly: false,
      authorizationType: authorizationType
    }
  }

  if (country) {
    $matchBy = {
      ...$matchBy,
      country: country
    }
  }

  const returnElement = [
    { $match: $matchBy },
    ...lookupSharedStatsUsers.beforeMatch,
    { $match: { $and: removeFalsyFromArray([$matchKeyword, $matchOwner]) } },
    ...(populateProducts ? lookupProducts : [])
  ]

  if (!showAllSurveys) {
    returnElement.push({
      $lookup: {
        from: 'survey_linkedSurveys__survey_linkedSurveys_survey',
        localField: '_id',
        foreignField: 'survey_linkedSurveys_survey',
        as: 'surveyLinked'
      }
    })
    returnElement.push({
      $unwind: { path: '$surveyLinked', preserveNullAndEmptyArrays: true }
    })
    if (omitUsedTastings) {
      returnElement.push({
        $match: {
          $or: [
            { surveyLinked: { $exists: false } },
            {
              'surveyLinked.survey_linkedSurveys': ObjectId(omitUsedTastings)
            }
          ]
        }
      })
    } else {
      returnElement.push({
        $match: { surveyLinked: { $exists: false } }
      })
    }
  }

  return returnElement
}

module.exports = {
  surveySearch,
  createSurveySearchPipeline
}
