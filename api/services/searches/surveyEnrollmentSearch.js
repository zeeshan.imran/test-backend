/* globals SurveyEnrollment */
const ObjectId = require('mongodb').ObjectID
const R = require('ramda')
const createSearchService = require('../../../utils/createSearchService')
const queryBuilder = require('../../../utils/mongoQueryBuilder')

const removeFalsyFromArray = R.filter(R.identity)
const createSurveyEnrollmentSearchPipeline = ({
  surveyId,
  status,
  productId,
  userId,
  keyword
}) => {
  const lookup = []
  let $match = {
    state: 'finished',
    answers: { $ne: [] }
  }

  if (surveyId) {
    $match = {
      ...$match,
      survey: ObjectId(surveyId)
    }
  }

  if (status) {
    if (status === 'valid' || status === 'invalid') {
      $match = {
        ...$match,
        validation: status
      }
    }

    if (status === 'not_processed') {
      $match = {
        ...$match,
        processed: false
      }
    }

    if (status === 'escalation') {
      $match = {
        ...$match,
        escalation: true
      }
    }

    if (status === 'hidden') {
      $match = {
        ...$match,
        hiddenFromCharts: true
      }
    }
  }

  if (productId) {
    lookup.push({
      $addFields: {
        filterSavedRewards: '$savedRewards'
      }
    })

    lookup.push({
      $unwind: {
        path: '$filterSavedRewards',
        preserveNullAndEmptyArrays: true
      }
    })

    $match = {
      ...$match,
      'filterSavedRewards.id': productId,
      'filterSavedRewards.answered': true
    }
  }

  if (userId) {
    lookup.push({
      $lookup: {
        from: 'logs',
        let: { surveyEnrollmentId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$relatedEnrollment', '$$surveyEnrollmentId']
              },
              by: ObjectId(userId)
            }
          }
        ],
        as: 'logs'
      }
    })

    $match = {
      ...$match,
      logs: { $ne: [] }
    }
  }

  if (keyword) {
    const orConditions = []

    if (ObjectId.isValid(keyword)) {
      orConditions.push({
        _id: ObjectId(keyword)
      })

      orConditions.push({
        'user.id': ObjectId(keyword)
      })
    } else {
      orConditions.push({
        paypalEmail: {
          $regex: queryBuilder.toRegExp(`%${keyword}%`, true)
        }
      })

      orConditions.push({
        'user.emailAddress': {
          $regex: queryBuilder.toRegExp(`%${keyword}%`, true)
        }
      })

      orConditions.push({
        'user.paypalEmailAddress': {
          $regex: queryBuilder.toRegExp(`%${keyword}%`, true)
        }
      })
    }

    $match = {
      ...$match,
      $or: removeFalsyFromArray(orConditions)
    }
  }

  lookup.push({
    $lookup: {
      from: 'question',
      let: { surveyId: '$survey' },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ['$survey', '$$surveyId']
            },
            typeOfQuestion: 'upload-picture'
          }
        }
      ],
      as: 'questions'
    }
  })

  lookup.push({
    $lookup: {
      from: 'answer',
      let: { surveyEnrollmentId: '$_id' },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ['$enrollment', '$$surveyEnrollmentId']
            },
            'value.0': { $regex: 'https://s3.*' }
          }
        }
      ],
      as: 'answers'
    }
  })

  lookup.push({
    $lookup: {
      from: 'user',
      let: { userId: '$user' },
      pipeline: [
        {
          $match: {
            $expr: {
              $eq: ['$_id', '$$userId']
            }
          }
        },
        {
          $project: {
            id: '$_id',
            fullName: '$fullName',
            emailAddress: '$emailAddress',
            paypalEmailAddress: '$paypalEmailAddress',
            blackListed: '$blackListed'
          }
        }
      ],
      as: 'user'
    }
  })

  lookup.push({
    $unwind: {
      path: '$user',
      preserveNullAndEmptyArrays: true
    }
  })

  return [...lookup, { $match }]
}

const surveyEnrollmentSearch = createSearchService(SurveyEnrollment)

module.exports = {
  surveyEnrollmentSearch,
  createSurveyEnrollmentSearchPipeline,
  createSurveyEnrollmentSearchOrderBy: queryBuilder.buildOrderBy
}
