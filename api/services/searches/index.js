module.exports = {
  ...require('./surveySearch'),
  ...require('./logsSearch'),
  ...require('./organizationSearch'),
  ...require('./userSearch'),
  ...require('./qrCodeSearch'),
  ...require('./surveyEnrollmentSearch'),
  ...require('./answerSearch'),
  ...require('./folderSurveySearch')
}
