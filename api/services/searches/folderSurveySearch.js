const queryBuilder = require('../../../utils/mongoQueryBuilder')

const folderSurveySearchPipeline = (state, keyword) => {
  let surveyFilter = {
    state: {
      $in: ['draft', 'active', 'suspended']
    }
  }
  if (state && state !== 'all') {
    surveyFilter = {
      state: state === 'published' ? 'active' : state
    }
  }
  let folderfilter = {}

  if (keyword) {
    Object.assign(surveyFilter, {
      $or: [
        { title: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } },
        { name: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } },
        { uniqueName: { $regex: queryBuilder.toRegExp(`%${keyword}%`) } }
      ]
    })
    folderfilter = {
      name: { $regex: queryBuilder.toRegExp(`%${keyword}%`) }
    }
  }
  const surveyPipeline = [
    {
      $sort: { openedAt: -1 }
    },
    {
      $lookup: {
        from: 'survey_linkedSurveys__survey_linkedSurveys_survey',
        localField: '_id',
        foreignField: 'survey_linkedSurveys_survey',
        as: 'screeners'
      }
    },
    {
      $addFields: {
        screeners: {
          $map: {
            input: '$screeners',
            in: '$$this.survey_linkedSurveys'
          }
        }
      }
    },
    {
      $lookup: {
        from: 'survey',
        localField: 'screeners',
        foreignField: '_id',
        as: 'screeners'
      }
    },
    {
      $lookup: {
        from: 'product_surveys__survey_products',
        localField: '_id',
        foreignField: 'survey_products',
        as: 'products'
      }
    },
    {
      $addFields: {
        products: {
          $map: {
            input: '$products',
            in: '$$this.product_surveys'
          }
        }
      }
    },
    {
      $lookup: {
        from: 'survey_sharedStatsUsers__user_sharedStatsSurveys',
        let: { surveyId: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: {
                $eq: ['$survey_sharedStatsUsers', '$$surveyId']
              }
            }
          },
          {
            $project: {
              user_sharedStatsSurveys: '$user_sharedStatsSurveys'
            }
          },
          {
            $lookup: {
              from: 'user',
              let: { userId: '$user_sharedStatsSurveys' },
              pipeline: [
                {
                  $match: {
                    $expr: {
                      $eq: ['$_id', '$$userId']
                    }
                  }
                },
                {
                  $project: {
                    id: '$_id',
                    organization: '$organization'
                  }
                },
                {
                  $lookup: {
                    from: 'organization',
                    let: { organizationId: '$organization' },
                    pipeline: [
                      {
                        $match: {
                          $expr: {
                            $eq: ['$_id', '$$organizationId']
                          }
                        }
                      },
                      {
                        $project: {
                          id: '$_id'
                        }
                      }
                    ],
                    as: 'organization'
                  }
                },
                { $unwind: '$organization' }
              ],
              as: 'user'
            }
          },
          { $unwind: '$user' }
        ],
        as: 'sharedStatsUsers'
      }
    }
  ]
  return {
    surveyPipeline,
    folderfilter,
    surveyFilter
  }
}

module.exports = {
  folderSurveySearchPipeline
}
