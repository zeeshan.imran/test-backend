/* globals Survey SurveyEnrollment */
/**
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const ObjectId = require('mongodb').ObjectID

const { getProductDisplay } = require('../../utils/ProductDisplayCombination')

const getCompulsorySurveys = async (country, surveyLanguage) => {
  if (!country) return ''
  
  const srcSurvey = await Survey.findOne({
    country: country,
    compulsorySurvey: true,
    state: { nin: ['deprecated', 'draft', 'suspended'] },
    surveyLanguage: surveyLanguage
  }).populate('questions')
  
  return srcSurvey || ''
}
const getScreenerSurveys = async surveyId => {
  const db = Survey.getDatastore().manager
  const cursor = await db
    .collection('survey_linkedSurveys__survey_linkedSurveys_survey')
    .aggregate([
      {
        $match: {
          survey_linkedSurveys_survey: ObjectId(surveyId)
        }
      },
      {
        $lookup: {
          from: 'survey',
          localField: 'survey_linkedSurveys',
          foreignField: '_id',
          as: 'screenerSurveys'
        }
      },
      { $unwind: '$screenerSurveys' },
      { $replaceRoot: { newRoot: '$screenerSurveys' } }
    ])

  return cursor.toArray()
}

const createTasterEnrolments = async (
  screenerSurvey,
  linkSurveyIds,
  screenerEnrolment,
  user
) => {
  try {
    const { rejectedEnrollment } = screenerEnrolment || {}
    linkSurveyIds.forEach(async linkSurveyId => {
      const {
        productDisplay,
        productDisplayOrder,
        productDisplayType
      } = await getProductDisplay(linkSurveyId)
      const linkedSurvey = await Survey.findOne({ id: linkSurveyId }).populate(
        'products',
        {
          select: [
            'id',
            'reward',
            'isAvailable',
            'amountInDollar',
            'rejectedReward',
            'delayToNextProduct',
            'extraDelayToNextProduct'
          ]
        }
      )
      let rewards =
        linkedSurvey.products && linkedSurvey.products.length
          ? linkedSurvey.products
              .filter(product => product.isAvailable)
              .map(product => ({
                id: product.id,
                payable: true,
                reward:
                  linkedSurvey.productRewardsRule &&
                  linkedSurvey.productRewardsRule.active
                    ? linkedSurvey.productRewardsRule.max
                    : rejectedEnrollment
                    ? product.rejectedReward
                    : product.reward,
                answered: false,
                amountInDollar: product.amountInDollar,
                delayToNextProduct: product.delayToNextProduct,
                extraDelayToNextProduct: product.extraDelayToNextProduct
              }))
          : []
      await SurveyEnrollment.create({
        productRewardsRule: linkedSurvey.productRewardsRule,
        user: user,
        survey: linkSurveyId,
        lastAnsweredQuestion: null,
        savedRewards: rewards,
        browserInfo: screenerEnrolment.browserInfo || [],
        productDisplay: productDisplay,
        productDisplayOrder: productDisplayOrder,
        productDisplayType: productDisplayType,
        enrollmentReferralAmount: screenerSurvey.referralAmount,
        state: 'waiting-for-product',
        allowedDaysToFillTheTasting: screenerSurvey.allowedDaysToFillTheTasting,
        enrollmentReferralAmountInDollar:
          screenerSurvey.referralAmountInDollar || 0,
        rejectedEnrollment
      })
    })
  } catch (error) {
    console.error(error)
  }
}
const getAnswerValidation = async (questionId, productId) => {
  const db = Survey.getDatastore().manager
  const cursor = await db.collection('answer').aggregate([
    {
      $match: {
        question: ObjectId(questionId),
        product: ObjectId(productId)
      }
    },
    {
      $lookup: {
        from: 'surveyenrollment',
        localField: 'enrollment',
        foreignField: '_id',
        as: 'enrollmentInfo'
      }
    },
    { $unwind: '$enrollmentInfo' },
    {
      $lookup: {
        from: 'user',
        localField: 'enrollmentInfo.user',
        foreignField: '_id',
        as: 'userInfo'
      }
    }
  ])

  return cursor.toArray()
}
module.exports = {
  getCompulsorySurveys: getCompulsorySurveys,
  getScreenerSurveys: getScreenerSurveys,
  createTasterEnrolments: createTasterEnrolments,
  getAnswerValidation: getAnswerValidation
}
