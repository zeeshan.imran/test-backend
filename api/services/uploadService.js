const AWS = require('aws-sdk')
const faker = require('faker')
const mime = require('mime-types')

module.exports = {
  getSignedUrl: async (req, res) => {
    try {
      AWS.config = new AWS.Config({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        region: process.env.AWS_REGION || 'eu-west-1',
        signatureVersion: 'v4'
      })

      const { type: mimeType, surveyId = '' } = req.body
      const uuid = (surveyId ? surveyId + '/' : '') + faker.random.uuid()
      const extension = mime.extension(mimeType)
      const s3 = new AWS.S3({
        params: {
          Bucket: `${process.env.BUCKET_NAME}`,
          Key: `${uuid}.${extension}`,
          ContentType: mimeType
        }
      })

      const url = await s3.getSignedUrl('putObject', {
        Expires: 60
      })
      return res.ok({
        signedUrl: url,
        fileLink: `${process.env.BUCKET_URL}/${uuid}.${extension}`
      })
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
