// eslint-disable-next-line node/no-extraneous-require
const Promise = require('bluebird')
const path = require('path')
const readFile = Promise.promisify(require('fs').readFile)

const getHtmlTemplateContent = async templateFileName => {
  const filePath = path.join(__dirname, './html-templates', templateFileName)
  const htmlContent = await readFile(filePath, 'utf-8')
  return htmlContent
}

/**
 * We can create any share link by this tool
 * - the share link will be /share/{{uniqueName}}
 * - when user share this link, the title, description will be used to generate the social's widget (FB, Twitter, LinkedIn)
 * - when the user click on the share link, it will redirect the user to the targetLink
 * @param {Object} data - Share information
 * @param {string} data.uniqueName - Id of link to share.
 * @param {string} data.title - Title of share content
 * @param {string} data.description - Describe about the share content
 * @param {string} data.targetLink - The link of content (a survey link...)
 */

const createShareContent = async data => {
  let template = await getHtmlTemplateContent('share-survey.html')

  for (let prop of [
    'uniqueName',
    'title',
    'description',
    'targetLink',
    'baseUrl'
  ]) {
    template = template.replace(RegExp(`{{${prop}}}`, 'g'), data[prop])
  }

  return template
}

module.exports = { createShareContent }
