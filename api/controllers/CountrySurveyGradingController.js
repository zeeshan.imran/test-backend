/* globals CountrySurveyGrading, _  */
/**
 * CountrySurveyGradingController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const userRoleCheck = type =>
  _.includes(['operator', 'power-user', 'super-admin'], type)

const gradingOverlapingCheck = grades =>
  grades &&
  grades.length &&
  grades.map((g1, indexG1) =>
    grades.map((g2, indexG2) => {
      if (indexG1 !== indexG2) {
        return (
          (Number(g2.scoreRange.start) >= Number(g1.scoreRange.start) &&
            Number(g2.scoreRange.start) <= Number(g1.scoreRange.end)) ||
          (Number(g2.scoreRange.end) >= Number(g1.scoreRange.start) &&
            Number(g2.scoreRange.end) <= Number(g1.scoreRange.end)) ||
          (Number(g1.scoreRange.start) >= Number(g2.scoreRange.start) &&
            Number(g1.scoreRange.start) <= Number(g2.scoreRange.end)) ||
          (Number(g1.scoreRange.end) >= Number(g2.scoreRange.start) &&
            Number(g1.scoreRange.end) <= Number(g2.scoreRange.end))
        )
      }
    })
  )

module.exports = {
  addCountrySurveyGrading: async (req, res) => {
    try {
      if (!req.token) {
        res.unauthorized()
      }

      const {
        user: { id: userId, organization, type }
      } = req.token

      if (!userRoleCheck(type)) {
        res.badRequest('NOT_ALLOWED')
      }

      if (req && req.body && !req.body.country) {
        res.badRequest('COUNTRY_IS_MANDATORY')
      }

      const overlap = _.compact(
        _.spread(_.union)(gradingOverlapingCheck(req.body.grades))
      )

      if (overlap && overlap.length && overlap[0]) {
        res.badRequest('SCORES_OVERLAP')
      }

      const countryCheck = await CountrySurveyGrading.findOne({
        where: { country: req.body.country }
      })

      if (!countryCheck) {
        const addGrades = await CountrySurveyGrading.create(
          Object.assign({}, req.body, {
            createdBy: userId,
            owner: organization
          })
        ).fetch()
        res.send(addGrades)
      } else {
        res.badRequest('GRADING_EXISTS')
      }
    } catch (error) {
      res.badRequest(error)
    }
  },
  updateCountrySurveyGrading: async (req, res) => {
    try {
      const { id } = req.params
      if (!req.token) {
        res.unauthorized()
      }

      const {
        user: { id: userId, organization, type }
      } = req.token

      if (!userRoleCheck(type)) {
        res.badRequest('NOT_ALLOWED')
      }

      if (req && req.body && !req.body.country) {
        res.badRequest('COUNTRY_IS_MANDATORY')
      }

      const overlap = _.compact(
        _.spread(_.union)(gradingOverlapingCheck(req.body.grades))
      )
      if (overlap && overlap.length && overlap[0]) {
        res.badRequest('SCORES_OVERLAP')
      }

      const updateGrades = await CountrySurveyGrading.updateOne({ id }).set(
        Object.assign({}, req.body, {
          updatedBy: userId,
          owner: organization
        })
      )

      res.send(updateGrades)
    } catch (error) {
      res.badRequest(error)
    }
  },
  getCountrySurveyGrading: async (req, res) => {
    try {
      if (!req.token) {
        res.unauthorized()
      }

      const {
        user: { organization }
      } = req.token

      const whereClause = {
        owner: organization
      }

      const grades = await CountrySurveyGrading.find({
        where: whereClause
      })
        .sort('updatedAt DESC')
        .populate('owner')
        .populate('createdBy')
        .populate('updatedBy')

      res.send(grades)
    } catch (error) {
      res.badRequest(error)
    }
  }
}
