/* globals Survey, Settings, SurveyEnrollment, Answer, Product, User, PairQuestion, Question, emailService, ProductDisplay, sails */
/* globals localStorage */ // for puppeteer
/**
 * SurveyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
// eslint-disable-next-line node/no-extraneous-require
const ObjectId = require('mongodb').ObjectID
const s3Utils = require('../../utils/s3Utils')
const R = require('ramda')
const { QUESTION_TYPE } = require('../../utils/constants')
const requestUtils = require('../../utils/requestUtils')
const createLog = require('../../utils/createLog')
const {
  getScreenerSurveys,
  getCompulsorySurveys
} = require('../services/surveyService')
const {
  surveySearch,
  createSurveySearchPipeline
} = require('../services/searches')
// const currencyNotations = require('../../utils/currencyNotations')
const { createShareContent } = require('../services/shareService')
const escapeHtml = require('../../utils/escapeHtml')
const getLayoutSettings = require('../../utils/getLayoutSettings')
const puppeteer = require('puppeteer')
const { pdfCreation, pptCreation } = require('../../utils/pdfCreation')
const { addCreateSurveyInMySql } = require('../../utils/mysqlData')
const {
  createDisplayCombination,
  updateDisplayCombination
} = require('../../utils/ProductDisplayCombination')
const fs = require('fs')
const shortid = require('shortid')
const { getCurrencyAlpha } = require('../../utils/getCurrency')
const chartUtils = require('../../utils/chartUtils')
const powerPointUtils = require('../../utils/powerPointUtils')
const moment = require('moment')
const { TOTAL_RESPONDENTS } = require('../../utils/powerPointUtils/constants')

const _ = require('@sailshq/lodash')
const db = Survey.getDatastore().manager

const sortByOrder = (a, b) =>
  typeof a.order === 'number' && typeof b.order === 'number'
    ? a.order - b.order
    : 0

const printResultSet = async (
  surveyId,
  jobGroupId,
  layout,
  templateName,
  totalRespondents
) => {
  const browser = await puppeteer.launch({
    headless: true,
    ignoreHTTPSErrors: true,
    args: [
      '--no-sandbox',
      '--proxy-server=https=xxx:xxx',
      '--disable-dev-shm-usage'
    ]
  })
  const page = await browser.newPage()
  await page.setRequestInterception(true)
  page.on('request', req => {
    const url = req.url()
    // optimize: block unnecessary
    const BLOCK_URLS = [
      '://cdn.slaask.com',
      '://slaask.com',
      '://uploads.slaask.com',
      '://ws.pusherapp.com',
      '://stats.pusher.com'
    ]
    if (BLOCK_URLS.some(BLOCK_URL => url.includes(BLOCK_URL))) {
      req.abort()
    } else {
      req.continue()
    }
  })
  await page.setViewport({
    width: 1920,
    height: 900
  })

  await page.goto(process.env.APP_URL, { waitUntil: 'domcontentloaded' })

  await page.evaluate(
    (layout, totalRespondents) => {
      /* eslint-disable */
      if (layout) {
        localStorage.setItem('pdf-layout', JSON.stringify(layout))
      }
      localStorage.setItem('total-respondents', totalRespondents)
      /* eslint-enable */
    },
    layout,
    totalRespondents
  )

  await page.goto(
    `${process.env.APP_URL}/preview-${templateName}-pdf/${surveyId}/${jobGroupId}`,
    {
      waitUntil: 'networkidle0'
    }
  )
  await page.waitForSelector('#loading', { hidden: true })

  const downloadFile = `${surveyId}-${shortid.generate()}`
  const pathForPdf = await pdfCreation(downloadFile)
  const { docHeight, pageHeight } = await page.evaluate(() => ({
    docHeight: document.documentElement.scrollHeight,
    pageHeight: window.__PAGE_HEIGHT
  }))
  const POSIBLE_EMPTY_SPACES = 0.75
  await page.pdf({
    path: pathForPdf,
    format: (layout && layout.paper) || 'A4',
    printBackground: true,
    displayHeaderFooter: false,
    pageRanges: `1-${Math.ceil(docHeight / pageHeight - POSIBLE_EMPTY_SPACES)}`,
    scale: 0.5,
    margin: {
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    }
  })
  await browser.close()

  return downloadFile
}

const repairEmptyRewards = async ({ survey }) => {
  try {
    const emptySurveyEnrollments = await SurveyEnrollment.find({
      survey: survey.id,
      paypalEmail: { '!=': [''] },
      state: 'finished',
      productIncentiveExported: false,
      validation: 'valid',
      savedRewards: ['', []]
    }).populate('selectedProducts')

    if (!emptySurveyEnrollments.length) {
      return { hasError: false }
    }
    for (let count = 0; count < emptySurveyEnrollments.length; count++) {
      const currentSurveyEnrollment = emptySurveyEnrollments[count]
      const { selectedProducts } = currentSurveyEnrollment

      if (selectedProducts && selectedProducts.length) {
        const selectedIds = selectedProducts.map(product => product.id)
        const newSavedRewards = survey.products.map(product => {
          return {
            id: product.id,
            reward: product.reward,
            answered: selectedIds.indexOf(product.id) >= 0,
            test: 123
          }
        })
        await SurveyEnrollment.updateOne({
          id: currentSurveyEnrollment.id
        }).set({
          savedRewards: newSavedRewards
        })
      }
    }
    return { hasError: false }
  } catch (error) {
    return { hasError: true, errorDetail: error }
  }
}

const getSurveyPdfLayout = async (surveyId, prop) => {
  const surveySetting = await Settings.findOne({
    where: { surveyId }
  })
  if (surveySetting) {
    return surveySetting[prop]
  }
  return null
}

const getAllSurveyEnrollments = async (survey, id, paypalEmail) => {
  let allSurveyEnrollments = []
  if (survey.isScreenerOnly) {
    const linkedSurveyIds = R.pluck('id', survey.linkedSurveys)
    const screenerSurveyEnrollments = await SurveyEnrollment.find({
      where: {
        survey: id,
        referral: { '!=': [''] },
        state: 'finished',
        shareExported: false,
        paypalEmail: paypalEmail
      },
      select: ['id', 'user', 'referral']
    }).populate('user')
    const userIds = []
    screenerSurveyEnrollments.forEach(screenerEnrollment => {
      if (
        screenerEnrollment.user &&
        !screenerEnrollment.user.completedReferredSurvey
      ) {
        userIds.push(screenerEnrollment.user.id)
      }
    })
    if (userIds.length) {
      const linkedSurveyEnrollments = await SurveyEnrollment.find({
        where: {
          survey: { in: linkedSurveyIds },
          user: { in: userIds },
          state: 'finished',
          validation: 'valid',
          shareExported: false
        },
        select: ['id', 'user']
      })

      linkedSurveyEnrollments.forEach(linkedEnrollment => {
        screenerSurveyEnrollments.forEach(screenerEnrollment => {
          if (
            screenerEnrollment.user &&
            screenerEnrollment.user.id === linkedEnrollment.user
          ) {
            allSurveyEnrollments.push(screenerEnrollment)
          }
        })
      })
    }
    return allSurveyEnrollments
  } else {
    allSurveyEnrollments = await SurveyEnrollment.find({
      where: {
        survey: id,
        referral: { '!=': [''] },
        state: 'finished',
        paypalEmail: paypalEmail,
        validation: 'valid',
        shareExported: false
      },
      select: ['id', 'referral']
    })
    return allSurveyEnrollments
  }
}

module.exports = {
  isUniqueNameDuplicated: async (req, res) => {
    try {
      const { uniqueName, surveyId } = req.body

      const surveys = await Survey.find({ uniqueName: uniqueName })
      if (!surveys.length) {
        return res.ok(false)
      } else if (
        surveys.length === 1 &&
        surveyId &&
        surveys[0]['id'] === surveyId
      ) {
        return res.ok(false)
      }
      return res.ok(true)
    } catch (error) {
      res.badRequest(error)
    }
  },
  createSurvey: async (req, res) => {
    try {
      const {
        predecessorSurvey,
        exclusiveTasters,
        products,
        productDisplayType
      } = req.body
      const surveyToCreate = R.omit(['exclusiveTaster'], req.body)
      const { user } = req.token

      let creator = user.id
      if (predecessorSurvey) {
        const survey = await Survey.findOne({
          id: predecessorSurvey
        })
        creator = survey.creator
        surveyToCreate['emails'] = survey.emails
        surveyToCreate['folder'] = survey.folder
      }

      let tasterIds
      if (exclusiveTasters) {
        const populatedTasters = await User.find({
          where: { emailAddress: exclusiveTasters }
        })
        tasterIds = populatedTasters.map(taster => taster.id)
      }

      const createdSurvey = await Survey.create(
        Object.assign({}, surveyToCreate, {
          exclusiveTasters: tasterIds,
          creator: creator,
          lastModifier: user.id,
          openedAt: Date.now(),
          publishedAt: Date.now()
        })
      ).fetch()
      createLog.survey(user.id, createdSurvey.id, null, createdSurvey)

      const { id } = createdSurvey
      await createDisplayCombination(products, productDisplayType, id)

      await addCreateSurveyInMySql(createdSurvey)

      return res.ok(createdSurvey)
    } catch (error) {
      res.badRequest(error)
    }
  },
  getSurveyShareContent: async (req, res) => {
    const { uniqueName } = req.params
    const survey = await Survey.findOne({
      uniqueName
    })
    res.send(
      await createShareContent({
        uniqueName: `${survey.uniqueName}`,
        baseUrl: `${process.env.APP_URL}`,
        targetLink: `${process.env.APP_URL}/survey/${survey.id}`,
        title: `${survey.name} - FlavorWiki`,
        description: `Thanks for your interest in taking ${escapeHtml(
          survey.name
        )}`
      })
    )
  },
  updateSurvey: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { id } = req.params
      const { exclusiveTasters, products } = req.body
      const fieldsToUpdate = R.omit(['exclusiveTaster'], req.body)
      const { user } = req.token
      const survey = await Survey.findOne({ id }).populate('linkedSurveys')
      const { state: surveyState } = survey
      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      let tasterIds
      if (exclusiveTasters && exclusiveTasters.length) {
        const populatedTasters = await User.find({
          where: { emailAddress: exclusiveTasters }
        })
        tasterIds = populatedTasters.map(taster => taster.id)
      }

      let questions = await Question.find({
        survey: id
      })

      const questionIds = []
      const paypalEmailQuestionIds = []

      questions.forEach(question => {
        if (question.typeOfQuestion === QUESTION_TYPE.PAYPAL_EMAIL) {
          paypalEmailQuestionIds.push(question.id)
        }

        questionIds.push(question.id)
      })

      if (fieldsToUpdate.state === 'active') {
        if (survey.state === 'draft') {
          fieldsToUpdate.publishedAt = Date.now()

          await Answer.destroy({
            question: { in: questionIds }
          })
        }
      }

      if (
        paypalEmailQuestionIds.length > 0 &&
        'referralAmount' in fieldsToUpdate &&
        !fieldsToUpdate.referralAmount
      ) {
        await Question.destroy({
          id: { in: paypalEmailQuestionIds }
        })
      }

      const updatedSurvey = await Survey.updateOne({
        id
      }).set(
        Object.assign({}, fieldsToUpdate, {
          exclusiveTasters: tasterIds,
          lastModifier: user.id
        })
      )

      const newSurvey = await Survey.findOne({ id }).populate('linkedSurveys')
      createLog.survey(user.id, survey.id, survey, newSurvey)

      const { productDisplayType } = updatedSurvey
      if (productDisplayType === 'none' || surveyState === 'draft') {
        await updateDisplayCombination(products, productDisplayType, id)
      }

      if (surveyState === 'draft' && products && products.length) {
        await createDisplayCombination(products, productDisplayType, id)
      }
      await addCreateSurveyInMySql(updatedSurvey)
      return res.ok(updatedSurvey)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getCompulsorySurveys: async (req, res) => {
    const { surveyId, isShared } = req.params
    const currentSurvey = await Survey.findOne({
      id: surveyId,
      compulsorySurvey: false
    })
    if (currentSurvey) {
      const compulsorySurvey = await getCompulsorySurveys(
        currentSurvey.country,
        currentSurvey.surveyLanguage
      )
      if (compulsorySurvey) {
        const screeningQuestions = compulsorySurvey.questions
          .filter(q => {
            if (q.displayOn === 'screening') {
              if (isShared === 'true') {
                if (q.showOnShared) {
                  return true
                }
              } else {
                return true
              }
            }
            return false
          })
          .sort(sortByOrder)
        return res.ok([
          {
            screeningQuestions,
            ...compulsorySurvey
          }
        ])
      }
    }

    return res.ok([])
  },
  getScreenerSurveys: async (req, res) => {
    const { surveyId, isShared } = req.params
    const screenerSurveys = await getScreenerSurveys(surveyId)
    let finalScreeners = []
    if (screenerSurveys && screenerSurveys.length) {
      const allScreenerIds = R.pluck('_id', screenerSurveys).map(e =>
        e.toString()
      )
      finalScreeners = await Survey.find({
        id: { in: allScreenerIds },
        state: {
          nin: ['deprecated']
        }
      }).populate('questions')

      finalScreeners = finalScreeners.map(screener => {
        const screeningQuestions = screener.questions
          .filter(q => {
            if (q.displayOn === 'screening') {
              if (isShared === 'true') {
                if (q.showOnShared) {
                  return true
                }
              } else {
                return true
              }
            }
            return false
          })
          .sort(sortByOrder)
        return {
          screeningQuestions,
          ...screener
        }
      })
    }
    res.ok(finalScreeners)
  },
  getSurveysByOwner: async (req, res) => {
    try {
      const {
        token: {
          user: { organization: owner, type, id }
        }
      } = req

      const searchQuery = {
        ...requestUtils.parseSearchQuery(req),
        userType: type,
        owner,
        userId: id,
        parentId: false
      }
      const pipeline = createSurveySearchPipeline(searchQuery)
      const { skip, limit } = searchQuery

      const surveys = await surveySearch.search(pipeline, {
        skip,
        limit,
        orderBy: { openedAt: -1 }
      })

      return res.ok(surveys)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  countSurveys: async (req, res) => {
    try {
      const {
        token: {
          user: { organization: owner, type, id }
        }
      } = req

      const searchQuery = {
        ...requestUtils.parseSearchQuery(req),
        userType: type,
        owner,
        userId: id
      }
      const pipeline = createSurveySearchPipeline(searchQuery, {
        populateProducts: false
      })

      const count = await surveySearch.count(pipeline)

      return res.ok(count)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getDemoSurvey: async (req, res) => {
    try {
      const { demoName } = req.params
      const survey = await Survey.findOne({ uniqueName: demoName })
      if (!survey) {
        return res.badRequest(`Could not find survey with the name ${demoName}`)
      }
      return res.ok(survey)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  openedSurvey: async (req, res) => {
    try {
      const { identifier } = req.params
      const survey = await Survey.findOne({ id: identifier })
      if (survey) {
        await Survey.update({
          id: survey.id
        })
          .set({
            openedAt: Date.now()
          })
          .meta({ fetch: false })
      }
      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurvey: async (req, res) => {
    try {
      const { identifier } = req.params
      let survey = await Survey.findOne({ uniqueName: identifier })
      if (survey) {
        return res.redirect('/survey/' + survey.id)
      }
      survey = await Survey.findOne({ id: identifier })
        .populate('creator')
        // .populate('lastModifier')
        .populate('questions')
        .populate('products', {
          sort: 'createdAt ASC'
        })
        // .populate('exclusiveTasters')
        // .populate('sharedStatsUsers')
        .populate('linkedSurveys')

      if (survey) {
        return res.ok(survey)
      }
      return res.badRequest(
        `Could not find survey with the name or id ${identifier}`
      )
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyByOrganization: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { identifier } = req.params
      const { user } = req.token

      let survey = await Survey.findOne({ uniqueName: identifier })
      if (survey) {
        return res.redirect('/survey/' + survey.id)
      }
      survey = await Survey.findOne({ id: identifier })
        .populate('creator')
        .populate('lastModifier')
        .populate('questions')
        // .populate('exclusiveTasters')
        .populate('sharedStatsUsers')
        .populate('linkedSurveys')
        .populate('products', {
          sort: 'createdAt ASC'
        })

      if (survey && survey.owner === user.organization) {
        return res.ok(survey)
      }
      if (survey) {
        const { sharedStatsUsers } = survey
        const sharedUsers = sharedStatsUsers.length
          ? R.pluck('id', sharedStatsUsers)
          : []
        if (sharedUsers.indexOf(user.id) >= 0) {
          return res.ok(survey)
        }
      }
      return res.badRequest(
        `Could not find survey with the name or id ${identifier}`
      )
    } catch (error) {
      return res.badRequest(error)
    }
  },
  cloneSurvey: async (req, res) => {
    try {
      const { user } = req.token
      // Get survey id to be cloned
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      // Fetch survey to clone
      const survey = await Survey.findOne({ id })
        .populate('products')
        .populate('questions')

      // Product display grab
      let productDisplayFromDb = await ProductDisplay.find({
        survey: id
      })
      // Need to fix this  and discuss with Hammad
      if (productDisplayFromDb.length) {
        productDisplayFromDb = productDisplayFromDb[0]
      }

      const productDisplay = productDisplayFromDb
        ? R.omit(['id', 'survey'], productDisplayFromDb)
        : []

      const { products = [], questions = [], uniqueName } = survey

      // Remove old data which are not needed to clone
      const clonedSurveyData = R.omit(
        ['products', 'questions', 'id', 'createdAt', 'updatedAt', 'openedAt'],
        survey
      )

      clonedSurveyData.creator = user.id

      const clonedProducts = await Promise.all(
        products.map(async product => {
          let { photo } = product
          let photoKey = s3Utils.getObjectKey(photo)
          const mainProductId = product && product.id
          const clonedProduct = R.omit(['id', 'photo'], product)

          if (photoKey) {
            let timestamp = new Date().valueOf()
            let newPhotoKey = `copy-at-${timestamp}-of-${photoKey}`
            photo = await s3Utils.copyFile(photoKey, newPhotoKey)
          }

          const clonedProductDoc = await Product.create({
            ...clonedProduct,
            photo
          }).fetch()
          const clonedProductId = clonedProductDoc && clonedProductDoc.id
          if (productDisplay && productDisplay.products) {
            const indexOfMainProductId = productDisplay.products.indexOf(
              mainProductId
            )
            if (indexOfMainProductId > -1) {
              productDisplay.products[indexOfMainProductId] = clonedProductId
            }
          }
          return clonedProductDoc
        })
      )

      // Create list of product ids to associate to new survey
      const mapId = R.map(R.prop('id'))

      const clonedSurvey = await Survey.create(
        Object.assign({}, clonedSurveyData, {
          creator: user.id,
          products: mapId(clonedProducts),
          name: `copy of ${clonedSurveyData.name}`,
          state: 'draft',
          uniqueName: `copy-of-${uniqueName}-${new Date().valueOf()}`
        })
      ).fetch()
      createLog.survey(user.id, clonedSurvey.id, null, clonedSurvey)

      // Add new entry in product Display
      if (productDisplay && productDisplay.products && clonedSurvey) {
        productDisplay['survey'] = clonedSurvey.id
        await ProductDisplay.create(productDisplay)
      }

      // Filter and get ids of all questions which are type of 'paired-questions'
      const pairedQuestionsId = questions
        .filter(question => question.typeOfQuestion === 'paired-questions')
        .map(question => question.id)

      // Find Pairs by questions id
      const pairedQuestions = await PairQuestion.find({
        question: pairedQuestionsId
      })

      // Clone questions
      // is store newQuestionId by questionId
      const newIds = {}
      const settingsNewIds = {}
      const getNewId = id => newIds[id]
      const getNewSettingId = id => settingsNewIds[id]
      for (const question of questions) {
        // Remove old attributes
        const clonedQuestionData = R.omit(
          ['id', 'createdAt', 'updatedAt', 'survey', 'skipFlow', 'openedAt'],
          question
        )

        // Create questions with new survey associated
        const clonedQuestion = await Question.create(
          Object.assign({}, clonedQuestionData, { survey: clonedSurvey.id })
        ).fetch()

        newIds[question.id] = clonedQuestion.id
        // settingsID
        settingsNewIds[
          `${question.id}-${question.chartType}`
        ] = `${clonedQuestion.id}-${question.chartType}`
        // If type === 'paired-questions' duplicate pairs
        if (clonedQuestion.typeOfQuestion === 'paired-questions') {
          const pairs =
            pairedQuestions.filter(pair => pair.question === question.id) || []

          // Create new list of pairs without old attributes and new question id
          const clonedPairsData = pairs.map(pair => {
            const clonedPair = R.omit(
              ['id', 'createdAt', 'updatedAt', 'question', 'openedAt'],
              pair
            )
            return Object.assign({}, clonedPair, {
              question: clonedQuestion.id
            })
          })

          // Create new pairs
          await PairQuestion.createEach(clonedPairsData)
        }
      }

      // reindex nextQuestion
      for (let question of questions) {
        const { id, nextQuestion, likingQuestion, relatedQuestions } = question
        await Question.updateOne({ id: getNewId(id) }).set({
          nextQuestion: getNewId(nextQuestion),
          likingQuestion: getNewId(likingQuestion) || null,
          relatedQuestions: relatedQuestions
            ? R.map(getNewId)(relatedQuestions)
            : null
        })
      }
      // Settings clone code
      const surveySetting = await Settings.findOne({
        where: { surveyId: id }
      })
      if (surveySetting) {
        let { chartsSettings, operatorPdfLayout, tasterPdfLayout } =
          surveySetting || {}
        const updatedChartSettings = {} // variable for ChartSettings
        if (chartsSettings) {
          for (const [key, value] of Object.entries(chartsSettings)) {
            const updateSettingKey = getNewSettingId(key)
            if (key === 'global') {
              updatedChartSettings['global'] = value
            } else if (updateSettingKey !== undefined) {
              updatedChartSettings[updateSettingKey] = value
            }
          }
        }

        if (operatorPdfLayout) {
          const { pages: operatorPages } = operatorPdfLayout
          const filterOperatorPageElements = await getLayoutSettings(
            getNewId,
            operatorPages
          )

          operatorPdfLayout = {
            ...operatorPdfLayout,
            ...{ pages: filterOperatorPageElements }
          }
        }
        if (tasterPdfLayout) {
          const { pages: tasterPages } = tasterPdfLayout
          const filteredTasterElements = await getLayoutSettings(
            getNewId,
            tasterPages
          )

          tasterPdfLayout = {
            ...operatorPdfLayout,
            ...{ pages: filteredTasterElements }
          }
        }
        await Settings.create({
          chartsSettings: updatedChartSettings,
          operatorPdfLayout,
          tasterPdfLayout,
          surveyId: clonedSurvey.id
        })
      }
      return res.ok({ survey: clonedSurvey.id })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  deprecateSurvey: async (req, res) => {
    try {
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id })

      if (survey && survey.owner !== user.organization) {
        return res.ok(survey)
      }

      const { uniqueName } = survey

      await Survey.updateOne({ id }).set({
        state: 'deprecated',
        uniqueName: `${uniqueName}-deprecated-at-${new Date().toISOString()}`,
        linkedSurveys: []
      })

      return res.ok(id)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyShareStats: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id }).populate('linkedSurveys')
      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }
      const { dualCurrency = false } = survey
      const paypalEmail = { '!=': [''] }

      const allSurveyEnrollments = await getAllSurveyEnrollments(
        survey,
        id,
        paypalEmail
      )

      if (allSurveyEnrollments.length) {
        const allReferrals = R.pluck('referral', allSurveyEnrollments)
        const frequencyCount = R.countBy(R.identity, allReferrals)
        const allRewardEnrollements = await SurveyEnrollment.find({
          where: {
            id: { in: R.uniq(allReferrals) },
            paypalEmail: paypalEmail
          },
          select: [
            'paypalEmail',
            'enrollmentReferralAmount',
            'user',
            'enrollmentReferralAmountInDollar'
          ]
        }).populate('user')

        if (allRewardEnrollements.length) {
          const resultant = []
          for (let count = 0; count < allRewardEnrollements.length; count++) {
            let refferedIds = []
            const referralCount =
              frequencyCount[allRewardEnrollements[count].id]
            const email = allRewardEnrollements[count].paypalEmail
            const fullName = allRewardEnrollements[count].user
              ? allRewardEnrollements[count].user.fullName
              : ''
            const enrollmentAmount = allRewardEnrollements[count]
              .enrollmentReferralAmount
              ? allRewardEnrollements[count].enrollmentReferralAmount
              : survey.referralAmount
            const amount = referralCount
              ? referralCount * enrollmentAmount
              : enrollmentAmount
            const currency = getCurrencyAlpha(survey.country)

            const enrollmentAmountInDollar = allRewardEnrollements[count]
              .enrollmentReferralAmountInDollar
              ? allRewardEnrollements[count].enrollmentReferralAmountInDollar
              : survey.referralAmountInDollar || 0
            const amountInDollar = referralCount
              ? referralCount * enrollmentAmountInDollar
              : enrollmentAmountInDollar

            allSurveyEnrollments.forEach(surveyEnrollment => {
              if (
                surveyEnrollment.referral === allRewardEnrollements[count].id
              ) {
                refferedIds.push(surveyEnrollment.id)
              }
            })
            resultant.push({
              fullName,
              email,
              amount,
              currency,
              refferedIds,
              amountInDollar: dualCurrency ? amountInDollar : 0
            })
          }
          return res.ok(resultant)
        }
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getGiftCardShares: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id }).populate('linkedSurveys')

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }
      const { dualCurrency = false } = survey

      let allSurveyEnrollments = await getAllSurveyEnrollments(survey, id, '')

      if (allSurveyEnrollments.length) {
        const allReferrals = R.pluck('referral', allSurveyEnrollments)
        const frequencyCount = R.countBy(R.identity, allReferrals)
        const allRewardEnrollements = await SurveyEnrollment.find({
          where: {
            id: { in: R.uniq(allReferrals) },
            user: { '!=': [''] },
            paypalEmail: ''
          },
          select: [
            'id',
            'user',
            'enrollmentReferralAmount',
            'enrollmentReferralAmountInDollar'
          ]
        }).populate('user')

        if (allRewardEnrollements.length) {
          const resultant = []
          for (let count = 0; count < allRewardEnrollements.length; count++) {
            let refferedIds = []
            const referralCount =
              frequencyCount[allRewardEnrollements[count].id]
            const email = allRewardEnrollements[count].user.emailAddress
            const fullName = allRewardEnrollements[count].user.fullName
            const enrollmentAmount = allRewardEnrollements[count]
              .enrollmentReferralAmount
              ? allRewardEnrollements[count].enrollmentReferralAmount
              : survey.referralAmount
            const amount = referralCount
              ? referralCount * enrollmentAmount
              : enrollmentAmount

            const enrollmentAmountInDollar = allRewardEnrollements[count]
              .enrollmentReferralAmountInDollar
              ? allRewardEnrollements[count].enrollmentReferralAmountInDollar
              : survey.referralAmountInDollar || 0
            const amountInDollar = referralCount
              ? referralCount * enrollmentAmountInDollar
              : enrollmentAmount

            const currency = getCurrencyAlpha(survey.country)

            allSurveyEnrollments.forEach(surveyEnrollment => {
              if (
                surveyEnrollment.referral === allRewardEnrollements[count].id
              ) {
                refferedIds.push(surveyEnrollment.id)
              }
            })
            resultant.push({
              fullName,
              email,
              amount,
              currency,
              refferedIds,
              amountInDollar: dualCurrency ? amountInDollar : 0
            })
          }
          return res.ok(resultant)
        }
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getProductIncentiveStats: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id })
        .populate('products')
        .populate('linkedSurveys')

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      if (survey && !survey.isPaypalSelected) {
        return res.badRequest('Paypal is not available for this survey')
      }

      const { hasError, errorDetail } = await repairEmptyRewards({ survey })
      if (hasError) {
        return res.badRequest(errorDetail)
      }
      const { dualCurrency = false } = survey

      const linkedSurveyIds = R.pluck('id', survey.linkedSurveys)

      let allSurveyEnrollments = []
      if (survey.isScreenerOnly) {
        const screenerSurveyEnrollments = await SurveyEnrollment.find({
          where: {
            survey: id,
            paypalEmail: { '!=': [''] },
            state: 'finished'
            // productIncentiveExported: false
          },
          select: ['id', 'paypalEmail', 'savedRewards', 'referral', 'user']
        }).populate('user')
        const userIds = screenerSurveyEnrollments.map(screenerEnrollment => {
          if (screenerEnrollment.user && screenerEnrollment.user.id) {
            return screenerEnrollment.user.id
          }
        })
        if (userIds.length) {
          const linkedSurveyEnrollments = await SurveyEnrollment.find({
            where: {
              survey: { in: linkedSurveyIds },
              user: { in: userIds },
              savedRewards: { '!=': ['', []] },
              state: 'finished',
              validation: 'valid',
              productIncentiveExported: false
            },
            select: ['id', 'paypalEmail', 'savedRewards', 'referral', 'user']
          }).populate('user')
          const addedEnrollments = []
          linkedSurveyEnrollments.forEach(linkedEnrollment => {
            screenerSurveyEnrollments.forEach(screenerEnrollment => {
              if (
                screenerEnrollment.user &&
                screenerEnrollment.user.id === linkedEnrollment.user.id
              ) {
                linkedEnrollment['paypalEmail'] =
                  screenerEnrollment['paypalEmail']
                if (addedEnrollments.indexOf(linkedEnrollment.id) < 0) {
                  allSurveyEnrollments.push(linkedEnrollment)
                  addedEnrollments.push(linkedEnrollment.id)
                }
              }
            })
          })
        }
      } else {
        allSurveyEnrollments = await SurveyEnrollment.find({
          where: {
            survey: id,
            paypalEmail: { '!=': [''] },
            savedRewards: { '!=': ['', []] },
            state: 'finished',
            validation: 'valid',
            productIncentiveExported: false
          },
          select: ['id', 'paypalEmail', 'savedRewards', 'referral', 'user']
        }).populate('user')
      }

      if (allSurveyEnrollments.length) {
        const resultant = []
        for (
          let counter = 0;
          counter < allSurveyEnrollments.length;
          counter++
        ) {
          const currentEnrollement = allSurveyEnrollments[counter]
          const totalReward = R.sum(
            R.pluck(
              'reward',
              R.filter(
                reward => reward.answered && reward.payable,
                currentEnrollement.savedRewards
              )
            )
          )

          const amountInDollar =
            R.sum(
              R.pluck(
                'amountInDollar',
                R.filter(
                  reward => reward.answered && reward.payable,
                  currentEnrollement.savedRewards
                )
              )
            ) || 0

          resultant.push({
            id: currentEnrollement['id'],
            email: currentEnrollement['paypalEmail'],
            fullName: currentEnrollement['user']
              ? currentEnrollement['user'].fullName
              : '',
            amount: totalReward,
            referral: currentEnrollement['referral'],
            currency: getCurrencyAlpha(survey.country),
            amountInDollar: dualCurrency ? amountInDollar : 0
          })
        }
        return res.ok(resultant)
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getGiftCardIncentives: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const { user } = req.token
      const id = req.param('id')

      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id })
        .populate('products')
        .populate('linkedSurveys')

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }

      if (
        survey &&
        (!survey.isGiftCardSelected ||
          survey.authorizationType !== 'enrollment')
      ) {
        return res.badRequest('Gift Card is not available for this survey')
      }

      const { hasError, errorDetail } = await repairEmptyRewards({ survey })
      if (hasError) {
        return res.badRequest(errorDetail)
      }
      const { dualCurrency = false } = survey

      const linkedSurveyIds = R.pluck('id', survey.linkedSurveys)

      let allSurveyEnrollments = []
      if (survey.isScreenerOnly) {
        const screenerSurveyEnrollments = await SurveyEnrollment.find({
          where: {
            survey: id,
            paypalEmail: '',
            state: 'finished'
            // giftCardIncentiveExported: false
          },
          select: ['id', 'paypalEmail', 'savedRewards', 'referral', 'user']
        }).populate('user')
        const userIds = screenerSurveyEnrollments.map(screenerEnrollment => {
          if (screenerEnrollment.user && screenerEnrollment.user.id) {
            return screenerEnrollment.user.id
          }
        })
        if (userIds.length) {
          const linkedSurveyEnrollments = await SurveyEnrollment.find({
            where: {
              survey: { in: linkedSurveyIds },
              user: { in: userIds },
              savedRewards: { '!=': ['', []] },
              paypalEmail: '',
              state: 'finished',
              validation: 'valid',
              giftCardIncentiveExported: false
            },
            select: ['id', 'paypalEmail', 'savedRewards', 'referral', 'user']
          }).populate('user')
          const addedEnrollments = []
          linkedSurveyEnrollments.forEach(linkedEnrollment => {
            screenerSurveyEnrollments.forEach(screenerEnrollment => {
              if (
                screenerEnrollment.user &&
                screenerEnrollment.user.id === linkedEnrollment.user.id
              ) {
                if (addedEnrollments.indexOf(linkedEnrollment.id) < 0) {
                  allSurveyEnrollments.push(linkedEnrollment)
                  addedEnrollments.push(linkedEnrollment.id)
                }
              }
            })
          })
        }
      } else {
        allSurveyEnrollments = await SurveyEnrollment.find({
          where: {
            survey: id,
            paypalEmail: '',
            state: 'finished',
            validation: 'valid',
            giftCardIncentiveExported: false
          },
          select: ['id', 'paypalEmail', 'savedRewards', 'referral', 'user']
        }).populate('user')
      }
      if (allSurveyEnrollments.length) {
        const resultant = []
        for (
          let counter = 0;
          counter < allSurveyEnrollments.length;
          counter++
        ) {
          const currentEnrollement = allSurveyEnrollments[counter]
          const totalReward = R.sum(
            R.pluck(
              'reward',
              R.filter(
                reward => reward.answered && reward.payable,
                currentEnrollement.savedRewards
              )
            )
          )
          const amountInDollar =
            R.sum(
              R.pluck(
                'amountInDollar',
                R.filter(
                  reward => reward.answered && reward.payable,
                  currentEnrollement.savedRewards
                )
              )
            ) || 0

          resultant.push({
            id: currentEnrollement['id'],
            email: currentEnrollement['user'].emailAddress,
            fullName: currentEnrollement['user'].fullName,
            referral: currentEnrollement['referral'],
            amount: totalReward,
            amountInDollar: dualCurrency ? amountInDollar : 0,
            currency: getCurrencyAlpha(survey.country)
          })
        }
        return res.ok(resultant)
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  dispatchEmail: async (req, res) => {
    try {
      const { id, email, type, surveyEnrollment } = req.body
      if (!id) {
        throw new Error('Survey Id is required for the Dispatcher')
      }
      const survey = await Survey.findOne({ id }).populate('products')
      if (!email) {
        throw new Error(`E-mail is required for the Dispatcher`)
      }
      if (!survey) {
        throw new Error(`Couldn't find the survey with the following id: ${id}`)
      }
      const {
        promotionSettings: { active, showAfterTaster, code },
        promotionOptions: { promoProducts }
      } = survey
      let shareSurey = survey
      let screenerSurveys = await getScreenerSurveys(survey.id)
      screenerSurveys = screenerSurveys.filter(screenerSurvey => {
        return screenerSurvey.state === 'active'
      })
      if (screenerSurveys.length > 0) {
        shareSurey = screenerSurveys[0]
      }

      const hostname = process.env.APP_URL

      const shareLink = `${hostname}/share/${shareSurey.uniqueName}${
        surveyEnrollment ? `?referral=${surveyEnrollment}` : ''
      }`

      switch (type) {
        case 'survey-completed': {
          const { products } = survey
          const productsNames = products.map(product => product.name)
          const productsImages = products.map(product => product.photo)
          let productNameString = ''
          let productList = {}
          if (promoProducts.length > 0) {
            promoProducts.forEach((item, index) => {
              productNameString += '' + item.label + ''
              productList[`promo_product_${index + 1}`] = item.label
              productList[`promo_product_${index + 1}_link`] = item.url
            })
          }

          const url = `${hostname}/survey/${id}`
          if (
            survey.compulsorySurvey ||
            survey.authorizationType === 'public'
          ) {
            break
          }
          if (survey.isScreenerOnly) {
            await emailService.sendSurveyWaitingMail({
              email,
              productsNames,
              productsImages,
              url,
              language: survey.surveyLanguage,
              shareLink,
              amount: survey.referralAmount,
              allowedDaysToFillTheTasting:
                survey.allowedDaysToFillTheTasting || 5,
              surveyEmail: R.path(['emails', 'surveyWaiting'])(survey),
              country: survey.country,
              active,
              code,
              productList,
              productNameString,
              showAfterTaster
            })
            break
          } else {
            const amount =
              screenerSurveys && screenerSurveys.length
                ? screenerSurveys[0].referralAmount
                : survey.referralAmount
            await emailService.sendSurveyCompletionMail({
              email,
              shareLink,
              language: survey.surveyLanguage,
              amount: amount,
              allowedDaysToFillTheTasting:
                survey.allowedDaysToFillTheTasting || 5,
              surveyEmail: survey.emails['surveyCompleted'],
              country: survey.country,
              active,
              code,
              showAfterTaster
            })
          }

          break
        }
        // case 'survey-waiting': {
        //   const { products } = survey
        //   const productsNames = products.map(product => product.name)
        //   const productsImages = products.map(product => product.photo)
        //   const url = `${hostname}/survey/${id}`
        //   if(survey.compulsorySurvey){
        //     break
        //   }
        //   await emailService.sendSurveyWaitingMail({
        //     email,
        //     productsNames,
        //     productsImages,
        //     url,
        //     language: survey.surveyLanguage,
        //     shareLink,
        //     amount: survey.referralAmount,
        //     allowedDaysToFillTheTasting:
        //       survey.allowedDaysToFillTheTasting || 5,
        //     surveyEmail: R.path(['emails', 'surveyWaiting'])(survey),
        //     country: survey.country
        //   })
        //   break
        // }
        default:
          throw new Error('Unknown type of e-mail service')
      }
      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  shareSurveyStats: async (req, res) => {
    try {
      const { user } = req.token
      const {
        users,
        surveyId,
        isScreenerShownInShare,
        isCompulsorySurveyShownInShare
      } = req.body

      if (!surveyId) {
        throw new Error('Survey Id is required for the Dispatcher')
      }

      const survey = await Survey.findOne({ id: surveyId })

      if (survey && survey.owner !== user.organization) {
        return res.unauthorized()
      }
      await Survey.update({ id: surveyId }).set({
        sharedStatsUsers: users,
        isScreenerShownInShare,
        isCompulsorySurveyShownInShare
      })

      const updatedSurvey = await Survey.findOne({ id: surveyId }).populate(
        'sharedStatsUsers'
      )
      return res.ok(updatedSurvey)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getCoversOfSurveys: async (req, res) => {
    const { surveys } = req.body

    const db = Survey.getDatastore().manager
    const cursor = await db
      .collection('product_surveys__survey_products')
      .aggregate([
        { $match: { survey_products: { $in: surveys.map(ObjectId) } } },
        {
          $lookup: {
            from: 'product',
            let: { surveys: '$product_surveys' },
            pipeline: [
              {
                $match: {
                  $expr: { $eq: ['$_id', '$$surveys'] },
                  isSurveyCover: true
                }
              }
            ],
            as: 'products'
          }
        },
        {
          $project: {
            _id: false,
            survey: '$survey_products',
            coverPhoto: { $arrayElemAt: ['$products.photo', 0] }
          }
        },
        {
          $unwind: '$coverPhoto'
        }
      ])

    res.send(await cursor.toArray())
  },
  downloadPdf: async (req, res) => {
    const file = req.params.id
    res.set('Content-Disposition', `attachment; filename=${file}.pdf`)
    fs.createReadStream(`./assets/pdf/${file}.pdf`).pipe(res)
  },
  downloadPpt: async (req, res) => {
    const file = req.params.id
    res.set('Content-Disposition', `attachment; filename=${file}.pptx`)
    fs.createReadStream(`./assets/ppt/${file}.pptx`).pipe(res)
  },
  getSurveyPdf: async (req, res) => {
    try {
      const { surveyId, jobGroupId } = req.params
      const { layout: requestLayout } = req.body

      const layout =
        requestLayout ||
        (await getSurveyPdfLayout(surveyId, 'operatorPdfLayout'))

      const survey = await Survey.findOne({
        where: { id: surveyId }
      })

      let totalRespondents = null

      if (
        survey &&
        survey.pdfFooterSettings &&
        survey.pdfFooterSettings.active
      ) {
        totalRespondents = await SurveyEnrollment.count({
          survey: surveyId,
          state: 'finished'
        })
      }

      const downloadFile = await printResultSet(
        surveyId,
        jobGroupId,
        layout,
        'operator',
        totalRespondents
      )

      return res.ok({ downloadFile })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getTasterPdf: async (req, res) => {
    try {
      const { surveyId, jobGroupId } = req.params
      const { layout: requestLayout } = req.body

      const layout =
        requestLayout || (await getSurveyPdfLayout(surveyId, 'tasterPdfLayout'))

      const downloadFile = await printResultSet(
        surveyId,
        jobGroupId,
        layout,
        'taster'
      )

      return res.ok({ downloadFile })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getQuickAccessSurveysByOwner: async (req, res) => {
    try {
      const {
        token: {
          user: { organization: owner, type, id }
        }
      } = req

      const surveys = await db.collection('survey').aggregate([
        {
          $match: {
            owner: ObjectId(owner),
            $or: [{ state: 'active' }, { state: 'draft' }]
          }
        },
        { $sort: { openedAt: -1 } },
        {
          $lookup: {
            from: 'product_surveys__survey_products',
            localField: '_id',
            foreignField: 'survey_products',
            as: 'products'
          }
        },
        {
          $addFields: {
            products: {
              $map: {
                input: '$products',
                in: '$$this.product_surveys'
              }
            }
          }
        },
        { $limit: 3 }
      ])
      const surveyList = await surveys.toArray()
      const newSurveyList =
        surveyList &&
        surveyList.length &&
        surveyList.map(survey => ({
          id: survey._id,
          ...survey
        }))
      return res.ok(newSurveyList)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyPpt: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const newToken = req.mainToken
      const { surveyId } = req.params
      const { jobGroupId, currentEnv } = req.body
      const surveyData = await Survey.findOne({
        where: {
          id: surveyId
        },
        select: ['name', 'createdAt']
      })
      const { createdAt, name } = surveyData
      const createdTime = moment(Number(createdAt)).format('MMMM DD, YYYY')
      const browser = await puppeteer.launch({
        headless: true,
        ignoreHTTPSErrors: true,
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
          '--disable-dev-shm-usage'
        ]
      })
      const page = await browser.newPage()
      await page.setRequestInterception(true)
      page.on('request', req => {
        const url = req.url()
        // optimize: block unnecessary
        const BLOCK_URLS = [
          '://cdn.slaask.com',
          '://slaask.com',
          '://uploads.slaask.com',
          '://ws.pusherapp.com',
          '://stats.pusher.com'
        ]
        if (BLOCK_URLS.some(BLOCK_URL => url.includes(BLOCK_URL))) {
          req.abort()
        } else {
          req.continue()
        }
      })
      await page.setViewport({
        width: 1920,
        height: 900
      })
      page.setExtraHTTPHeaders({
        Authorization: `Bearer ${newToken}`
      })
      await page.setDefaultNavigationTimeout(0)
      await page.goto(process.env.APP_URL, { waitUntil: 'domcontentloaded' })

      await page.goto(
        `${process.env.APP_URL}/preview-operator-ppt/${surveyId}/${jobGroupId}`,
        {
          waitUntil: 'networkidle0'
        }
      )

      const pathForPpt = await pptCreation(surveyId)
      // eslint-disable-next-line node/no-deprecated-api
      fs.exists(pathForPpt, exists => {
        if (exists) {
          fs.unlinkSync(pathForPpt)
        }
      })

      const ppt = powerPointUtils.setup({ template: currentEnv })
      // the omitBackground is not working with puppeter in current version
      // we inject the background as same as the slide background
      // to make it looks like transparent
      await page.evaluate(
        background => {
          const simulateBackgroundElements = document.querySelectorAll(
            '.ant-card [data-ppt-type="map"]'
          )
          simulateBackgroundElements.forEach(
            q => (q.closest('.ant-card').style.background = background)
          )
        },
        [`#${ppt.theme.background}`]
      )
      // add intro
      const slide = powerPointUtils.createSlide(ppt, {
        master: 'INTRO_MASTER'
      })
      powerPointUtils.addIntro(slide, { title: name, date: createdTime })

      const charts = await chartUtils.getCharts(page)

      for (let chart of charts) {
        try {
          const chartParts = await chartUtils.getChartParts(chart)
          const totalRespondentParts = chartParts.filter(
            part => part.type === TOTAL_RESPONDENTS
          )
          const anovaParts = chartParts.filter(
            part => part.type === 'anova-chart'
          )
          const fisherParts = chartParts.filter(
            part => part.type === 'fisher-chart'
          )
          const tukeyParts = chartParts.filter(
            part => part.type === 'tukey-chart'
          )
          const multiParts = chartParts.filter(
            part => part.type === 'multi-chart'
          )
          const pcaParts = chartParts.filter(part => part.type === 'pca')
          const biggerParts = chartParts.filter(
            part =>
              (part.type === 'table' && part.data.dataSource.length > 4) ||
              part.type === 'penalty-chart'
          )
          const smallParts = R.without(
            [
              ...totalRespondentParts,
              ...multiParts,
              ...biggerParts,
              ...fisherParts,
              ...tukeyParts,
              ...anovaParts,
              ...pcaParts
            ],
            chartParts
          )
          const statsGrouping = fisherParts.length
            ? fisherParts
            : tukeyParts.length
            ? tukeyParts
            : false
          if (smallParts.length > 0) {
            await powerPointUtils.createSlideWithChartParts(
              ppt,
              chart,
              [...totalRespondentParts, ...smallParts],
              '',
              statsGrouping
            )
          }

          if (multiParts.length > 0) {
            await powerPointUtils.createSlideMultiProductChart(
              multiParts,
              ppt,
              chart,
              totalRespondentParts
            )
          }

          for (const biggerPart of biggerParts) {
            await powerPointUtils.createSlideWithChartParts(
              ppt,
              chart,
              [biggerPart],
              '',
              statsGrouping
            )
          }

          if (anovaParts.length > 0) {
            await powerPointUtils.createSlideMultiLevelChart(
              anovaParts,
              ppt,
              chart,
              totalRespondentParts
            )
          }
          if (fisherParts.length > 0) {
            await powerPointUtils.createSlideMultiLevelChart(
              fisherParts,
              ppt,
              chart,
              totalRespondentParts
            )
          }

          if (tukeyParts.length > 0) {
            await powerPointUtils.createSlideMultiLevelChart(
              tukeyParts,
              ppt,
              chart,
              totalRespondentParts
            )
          }

          if (pcaParts.length > 0) {
            await powerPointUtils.createSlidePCA(pcaParts, ppt, chart)
          }
        } catch (error) {
          // error
          sails.log.error('Error while creating a new slide', error)
        }
      }
      await browser.close()
      const data = await powerPointUtils.toBase64(ppt)
      fs.writeFile(pathForPpt, data, { encoding: 'base64' }, function (err) {
        if (err) throw err
        else {
          sails.log('success')
          return res.ok()
        }
      })
    } catch (error) {
      sails.log.error(error)
      return res.badRequest(error)
    }
  },
  getCompulsorySurveyByCountry: async (req, res) => {
    try {
      const { user } = req.token
      if (user && user.country) {
        const compulsorySurvey = await Survey.find({
          compulsorySurvey: true,
          country: user.country
        })
        return res.ok(compulsorySurvey ? compulsorySurvey[0] : [])
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getQuestionWithOrder: async (req, res) => {
    try {
      const { surveyId } = req.params
      const sortOrder = ['screening', 'end', 'begin', 'middle', 'payments']
      if (surveyId) {
        let questionsId = await Question.find({
          where: { survey: surveyId },
          select: ['id', 'displayOn', 'typeOfQuestion']
        })
        let results = []
        _.forEach(sortOrder, av => {
          const object = _.filter(questionsId, { displayOn: av })
          results = [...results, ...object]
        })

        return res.ok(results)
      }
      return res.ok([])
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
