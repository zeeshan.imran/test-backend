/* global Settings */
/**
 * SettingsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  getChartsSettings: async (req, res) => {
    try {
      const { surveyId } = req.params
      const surveySetting = await Settings.findOne({ surveyId: surveyId })
      res.ok(surveySetting || {})
    } catch (error) {
      return res.badRequest(error)
    }
  },
  saveChartsSettings: async (req, res) => {
    try {
      const {
        surveyId,
        chartsSettings,
        operatorPdfLayout,
        tasterPdfLayout
      } = req.body

      const updateSet = {}

      if (chartsSettings) {
        updateSet.chartsSettings = chartsSettings
      }

      if (operatorPdfLayout) {
        updateSet.operatorPdfLayout = operatorPdfLayout
      }

      if (tasterPdfLayout) {
        updateSet.tasterPdfLayout = tasterPdfLayout
      }

      await Settings.findOrCreate(
        { surveyId: surveyId },
        {
          surveyId: surveyId,
          ...updateSet
        }
      ).exec(async (err, setting, wasCreated) => {
        if (err) {
          return res.serverError(err)
        }

        if (!wasCreated) {
          await Settings.updateOne({ id: setting.id }).set({
            ...updateSet
          })
        }

        const savedChartsSettings = await Settings.findOne({
          surveyId: surveyId
        })
        res.ok(savedChartsSettings || [])
      })
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
