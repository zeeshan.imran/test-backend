/* globals Survey */
const { createSurveyShareLink } = require('../services/shareService')

/*
curl -X POST \
  http://localhost:1337/system/resetShareLinks \
  -H 'Authorization: Bearer ADMIN_TOKEN' \
  -H 'Host: localhost:1337' \
  -H 'User-Agent: PostmanRuntime/7.20.1' \
  -H 'cache-control: no-cache'
  */
module.exports = {
  resetShareLinks: async (req, res) => {
    const surveys = await Survey.find({ state: { '!=': 'deprecated' } })
    for (const survey of surveys) {
      createSurveyShareLink(survey)
    }
    res.send('All share links is reset')
  }
}
