/* globals Folder, Survey */

/**
 * FoldersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
// eslint-disable-next-line node/no-extraneous-require
const ObjectId = require('mongodb').ObjectID
const { folderSurveySearchPipeline } = require('../services/searches')
module.exports = {
  getFolderPath: async (req, res) => {
    try {
      const { id } = req.params
      let route = { pathName: [], pathId: [] }

      if (!req.token) {
        res.unauthorized()
        return
      }

      if (!id) {
        res.send(route)
        return
      }

      const checkFolder = async folderId => {
        const folder = await Folder.findOne({
          id: folderId,
          owner: req.token.user.organization,
          status: 'active'
        })

        if (folder) {
          route.pathName.unshift(folder.name)
          route.pathId.unshift(folder.id)

          if (folder.parent) {
            await checkFolder(folder.parent)
          }
        }
      }

      await checkFolder(id)
      res.send(route)
    } catch (error) {
      res.badRequest(error)
    }
  },

  listFolders: async (req, res) => {
    try {
      if (!req.token) {
        res.unauthorized()
      } else {
        const { user } = req.token
        const { parentId } = req.query
        let whereClause = {
          owner: user.organization,
          status: 'active'
        }
        Object.assign(whereClause, { parent: parentId ? parentId : null })
        const folders = await Folder.find({
          where: whereClause
        })
          .populate('children', { status: 'active' })
          .sort('createdAt desc')
        res.send(folders)
      }
    } catch (error) {
      res.badRequest(error)
    }
  },

  createFolder: async (req, res) => {
    try {
      if (!req.token) {
        res.unauthorized()
      } else if (!req.body.name) {
        res.badRequest({ error: 'folder name is mandatory' })
      } else {
        const { user } = req.token
        const folder = await Folder.create(
          Object.assign({}, req.body, {
            owner: user.organization
          })
        ).fetch()
        res.send(folder)
      }
    } catch (error) {
      res.badRequest(error)
    }
  },

  showFolder: async (req, res) => {
    try {
      if (!req.token) {
        res.unauthorized()
      } else {
        const { parentId, state, keyword } = req.query
        const { user } = req.token
        const db = Folder.getDatastore().manager
        let result = []
        const {
          surveyPipeline,
          folderfilter,
          surveyFilter
        } = folderSurveySearchPipeline(state, keyword)

        if (!parentId) {
          result = await db.collection('folder').aggregate([
            { $limit: 1 },
            { $project: { _id: 1 } },
            { $project: { _id: 0 } },
            {
              $lookup: {
                from: 'folder',
                pipeline: [
                  {
                    $match: {
                      parent: null,
                      status: 'active',
                      owner: ObjectId(user.organization),
                      ...folderfilter
                    }
                  },
                  {
                    $sort: { createdAt: -1 }
                  }
                ],
                as: 'childFolders'
              }
            },
            {
              $lookup: {
                from: 'survey',
                pipeline: [
                  {
                    $match: {
                      $and: [
                        {
                          $or: [
                            {
                              folder: {
                                $exists: false
                              }
                            },
                            {
                              folder: null
                            }
                          ],
                          owner: ObjectId(user.organization),
                          ...surveyFilter
                        }
                      ]
                    }
                  },
                  ...surveyPipeline
                ],
                as: 'surveys'
              }
            },
            {
              $project: {
                Union: { $concatArrays: ['$childFolders', '$surveys'] }
              }
            },
            { $unwind: '$Union' },
            { $replaceRoot: { newRoot: '$Union' } }
          ])
        } else {
          result = await db.collection('folder').aggregate([
            { $limit: 1 },
            { $project: { _id: 1 } },
            { $project: { _id: 0 } },
            {
              $lookup: {
                from: 'folder',
                pipeline: [
                  {
                    $match: {
                      parent: ObjectId(parentId),
                      status: 'active',
                      owner: ObjectId(user.organization),
                      ...folderfilter
                    }
                  },
                  {
                    $sort: { createdAt: -1 }
                  }
                ],
                as: 'childFolders'
              }
            },
            {
              $lookup: {
                from: 'survey',
                pipeline: [
                  {
                    $match: {
                      folder: ObjectId(parentId),
                      owner: ObjectId(user.organization),
                      ...surveyFilter
                    }
                  },
                  ...surveyPipeline
                ],
                as: 'surveys'
              }
            },
            {
              $project: {
                Union: { $concatArrays: ['$childFolders', '$surveys'] }
              }
            },
            { $unwind: '$Union' },
            { $replaceRoot: { newRoot: '$Union' } }
          ])
        }
        const resultValues = await result.toArray()
        
        res.send(resultValues ? resultValues.map(r => {
          if (r.screeners && r.screeners.length) {
            r.screeners = r.screeners.map(rs => {
              rs.id = rs._id
              return rs
            })
          }
          return r
        }) : resultValues)
      }
    } catch (error) {
      res.badRequest(error)
    }
  },

  updateFolder: async (req, res) => {
    try {
      const { id } = req.params
      if (!req.token) {
        res.unauthorized()
      } else if (!req.body.name) {
        res.badRequest({ error: 'folder name is mandatory' })
      } else {
        const updatedFolder = await Folder.updateOne({ id }).set({
          name: req.body.name
        })
        res.send(updatedFolder)
      }
    } catch (error) {
      res.badRequest(error)
    }
  },

  destroyFolder: async (req, res) => {
    try {
      const { id } = req.params
      if (!req.token) {
        res.unauthorized()
      } else {
        const deletedFolder = await Folder.updateOne({ id }).set({
          status: 'deleted'
        })
        res.send(deletedFolder)
      }
    } catch (error) {
      res.badRequest(error)
    }
  },

  moveSurveyToFolder: async (req, res) => {
    try {
      const { id } = req.params
      const { surveyId, folderId } = req.body
      if (!req.token) {
        res.unauthorized()
      } else if (!surveyId && !folderId) {
        res.badRequest({ error: 'Survey Id or Parent Folder Id is mandatory.' })
      } else {
        if (surveyId) {
          const addSurveyToFolder = await Survey.updateOne({
            id: surveyId
          }).set({
            folder: id === 'null' ? null : id
          })
          res.send(addSurveyToFolder)
        } else {
          const addFolderToFolder = await Folder.updateOne({
            id: folderId
          }).set({
            parent: id === 'null' ? null : id
          })
          res.send(addFolderToFolder)
        }
      }
    } catch (error) {
      res.badRequest(error)
    }
  }
}
