/* globals sails, Survey, SurveyEnrollment, Question, Answer, User, emailService, Logs, _, CountrySurveyGrading */
/**
 * SurveyEnrollmentController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const ObjectId = require('mongodb').ObjectID
const rp = require('request-promise')
const R = require('ramda')
const moment = require('moment')
const ApiError = require('../../utils/ApiError')
const requestUtils = require('../../utils/requestUtils')
const {
  answerSearch,
  surveyEnrollmentSearch,
  createSurveyEnrollmentSearchPipeline
} = require('../services/searches')
const {
  getScreenerSurveys,
  createTasterEnrolments,
  getAnswerValidation
} = require('../services/surveyService')

const canChangeSurveyState = ({ from, to }) => {
  const wantsToRollbackFinished = from === 'finished' && to !== 'paid'
  const inTerminalState = from === 'paid' || from === 'rejected'
  return !wantsToRollbackFinished && !inTerminalState
}

const updateLinkedSurveys = async surveyEnrollment => {
  const { survey, user } = surveyEnrollment
  const userId = user && user.id ? user.id : user
  const mainSurvey = await Survey.findOne({ id: survey }).populate(
    'linkedSurveys',
    { state: 'active' }
  )
  if (mainSurvey) {
    const { linkedSurveys, isScreenerOnly } = mainSurvey
    if (isScreenerOnly && linkedSurveys.length) {
      const linkSurveyIds = R.pluck('id', linkedSurveys)
      await Survey.addToCollection(
        R.uniq(linkSurveyIds),
        'exclusiveTasters'
      ).members(userId)
      await createTasterEnrolments(
        mainSurvey,
        linkSurveyIds,
        surveyEnrollment,
        userId
      )
    }
  }
}

const sendMatrixStats = async (surveyId, respondentId, matrixAnswers) => {
  const matrixAnswersByQuestion = R.groupBy(R.prop('question'), matrixAnswers)
  for (let questionId in matrixAnswersByQuestion) {
    const matrixAnswersByProductQuestion = R.groupBy(
      R.prop('product'),
      matrixAnswersByQuestion[questionId]
    )
    for (let productId in matrixAnswersByProductQuestion) {
      const answers = R.flatten(
        matrixAnswersByProductQuestion[productId].map(answer => {
          const matrixValues = answer.value[0]
          return Object.keys(matrixValues).map(row => ({
            row,
            rowValues: Array.isArray(matrixValues[row])
              ? matrixValues[row] // send multiple values
              : [matrixValues[row]] // send single value as array with single item
          }))
        })
      )
      try {
        await rp({
          method: 'POST',
          uri: `${sails.config.custom.statsApi}/survey/${surveyId}/respondent/${respondentId}/matrix/${questionId}/${productId}`,

          body: answers,
          json: true
        })
      } catch (error) {
        // eslint-disable-next-line
        throw new Error(error)
      }
    }
  }
}

const sendSurveyStats = async surveyEnrollment => {
  const questionsAnsweredIds = surveyEnrollment.answers.map(a => a.question)
  const questionsAnswered = await Question.find({
    id: questionsAnsweredIds
  })
  const questionsById = questionsAnswered.reduce(
    (acc, q) => Object.assign({}, acc, { [q.id]: q }),
    {}
  )

  const filteredAnswers = surveyEnrollment.answers.filter(
    answer =>
      questionsById[answer.question] &&
      questionsById[answer.question].typeOfQuestion !== 'paired-questions' &&
      questionsById[answer.question].typeOfQuestion !== 'slider' &&
      questionsById[answer.question].typeOfQuestion !== 'matrix'
  )

  let flatAnswers = filteredAnswers.reduce((flat, answer) => {
    const flatAnswer = answer.value.map(value =>
      // Answer can be a plain string or object
      Object.assign(
        {},
        answer,
        typeof value === 'object' ? value : { value: value }
      )
    )
    return [...flat, ...flatAnswer]
  }, [])
  let flatItems = []
  flatAnswers.map(item => {
    if (item.value === 'other' && item.desc) {
      let copyItem = Object.assign({}, item)
      copyItem.value = item.desc
      flatItems.push(copyItem)
    }
  })
  flatAnswers = [...flatItems, ...flatAnswers]
  const answersByProduct = flatAnswers.reduce((acc, answer) => {
    const targetProduct = answer.product || 'no-product'

    if (
      answer.value !== '' &&
      answer.value !== null &&
      answer.value !== undefined
    ) {
      acc[targetProduct] = [
        ...(acc[targetProduct] || []),
        {
          answer_id: answer.id,
          question_id: answer.question,
          answer_value: answer.value,
          answer_desc: answer.desc
        }
      ]
    }

    return acc
  }, {})

  const productsWithAnswers = Object.keys(answersByProduct).map(productId => {
    let formattedProductId = productId === 'no-product' ? null : productId
    return {
      product_id: formattedProductId,
      data: answersByProduct[productId]
    }
  })

  const surveyId = surveyEnrollment.survey
  const respondentId = surveyEnrollment.id
  const userRepondent =
    surveyEnrollment && surveyEnrollment.user && surveyEnrollment.user.id
  const validation =
    surveyEnrollment.validation && surveyEnrollment.validation === 'valid'
      ? 1
      : 0

  const startedAt = new Date(surveyEnrollment.createdAt).getTime()
  const finishedAt = new Date(surveyEnrollment.finishedAt).getTime()

  const socioEconomicRating = surveyEnrollment.socioEconomicRating
  rp({
    method: 'POST',
    uri: `${sails.config.custom.statsApi}/survey/${surveyId}/respondent/${respondentId}/validation`,
    body: {
      user_id: userRepondent,
      validation,
      sec_name:
        socioEconomicRating && socioEconomicRating.grade
          ? socioEconomicRating.grade
          : null,
      sec_score:
        socioEconomicRating && socioEconomicRating.grade
          ? socioEconomicRating.score
          : null,
      started_at: startedAt,
      finished_at: finishedAt,
      escalation: surveyEnrollment.escalation,
      processed: surveyEnrollment.processed,
      product_display_id: surveyEnrollment.productDisplay
    },
    json: true
  })

  rp({
    method: 'POST',
    uri: `${sails.config.custom.statsApi}/survey/${surveyId}/respondent/${respondentId}`,
    body: productsWithAnswers,
    json: true
  })

  sendMatrixStats(
    surveyId,
    respondentId,
    surveyEnrollment.answers.filter(
      answer => questionsById[answer.question].typeOfQuestion === 'matrix'
    )
  )
}
const updateUserForCompulsorySurvey = async userId => {
  await User.updateOne({
    id: userId
  }).set({
    isCompulsorySurveyTaken: true
  })
}

const compulsorySurveyScoreCalculate = async (answers, country) => {
  const questionIds = answers.map(ans => ans.question)

  const questoinTypes = [
    'choose-one',
    'choose-multiple',
    'matrix',
    'dropdown',
    'vertical-rating'
  ]

  const countryScores = await CountrySurveyGrading.findOne({
    country: country
  })

  const getQuestions = await Question.find({
    where: {
      id: { in: questionIds },
      considerValueForScore: true,
      typeOfQuestion: { in: questoinTypes }
    }
  })

  const surveyAnswers = getQuestions.map(question =>
    answers.map(ans => {
      if (ans.question === question.id) {
        if (
          question.typeOfQuestion === 'choose-one' ||
          question.typeOfQuestion === 'dropdown'
        ) {
          return (
            question &&
            question.options &&
            question.options.length &&
            question.options.map(option => {
              if (ans.value[0] === option.value) {
                return option.scoreValue
              }
            })
          )
        } else if (question.typeOfQuestion === 'choose-multiple') {
          const chooseMultipleAnswerValues =
            question &&
            question.options &&
            question.options.length &&
            question.options.map(option => {
              return ans.value.map(value => {
                if (value === option.value) {
                  return option.scoreValue
                }
              })
            })
          return (
            (chooseMultipleAnswerValues &&
              chooseMultipleAnswerValues.length &&
              _.flatten(chooseMultipleAnswerValues)) ||
            []
          )
        } else if (question.typeOfQuestion === 'matrix') {
          let ansValues
          if (
            question &&
            question.settings &&
            question.settings.chooseMultiple
          ) {
            ansValues = _.flatten(Object.values(ans.value[0]))
          } else {
            ansValues = Object.values(ans.value[0])
          }
          const matrixAnswerValues =
            question &&
            question.options &&
            question.options.length &&
            question.options.map(option => {
              return ansValues.map(value => {
                if (value === option.value) {
                  return option.scoreValue
                }
              })
            })
          return (
            (matrixAnswerValues &&
              matrixAnswerValues.length &&
              _.flatten(matrixAnswerValues)) ||
            []
          )
        } else {
          return ans.value
        }
      }
    })
  )

  const calculateValues = _.flatten(_.compact(_.flatten(surveyAnswers))).map(
    val => {
      if (typeof val === 'number' || typeof val === 'string') {
        return Number(val)
      } else if (typeof val === 'object') {
        return _.flatten(Object.values(val)).map(v => Number(v))
      }
    }
  )

  const userSurveyScore = _.sum(_.compact(_.flatten(calculateValues)))
  let userScoreAndGrade = {}
  countryScores &&
    countryScores.grades &&
    countryScores.grades.length &&
    countryScores.grades.map(g => {
      if (
        g &&
        g.scoreRange &&
        g.scoreRange.start &&
        g.scoreRange.end &&
        Number(userSurveyScore) >= Number(g.scoreRange.start) &&
        Number(userSurveyScore) <= Number(g.scoreRange.end)
      ) {
        userScoreAndGrade = {
          grade: g.grade,
          score: userSurveyScore
        }
      }
    })
  return userScoreAndGrade
}

const updateUserSocioEconomicRating = async (userId, enrolmentId, rating) => {
  await User.updateOne({
    id: userId
  }).set({
    socioEconomicRating: rating
  })
  await SurveyEnrollment.updateOne({
    id: enrolmentId
  }).set({
    socioEconomicRating: rating
  })
}

const userRoleCheck = type =>
  _.includes(['operator', 'power-user', 'super-admin'], type)

// Reject function
const updateRejectedEnrollment = async (
  enrollmentId,
  rejectedEnrollment,
  question
) => {
  const { rejectedQuestion } = await SurveyEnrollment.findOne({
    where: { id: enrollmentId },
    select: ['rejectedQuestion']
  })
  await SurveyEnrollment.update({
    id: enrollmentId
  }).set({
    rejectedQuestion: [...rejectedQuestion, question],
    rejectedEnrollment
  })
  return true
}
module.exports = {
  saveQuestions: async (req, res) => {
    try {
      let {
        surveyEnrollment: surveyEnrollmentId,
        questions: newQuestions
      } = req.body

      await SurveyEnrollment.update({
        id: surveyEnrollmentId
      }).set({
        savedQuestions: newQuestions
      })
      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  saveRewards: async (req, res) => {
    try {
      let {
        surveyEnrollment: surveyEnrollmentId,
        rewards: newRewards
      } = req.body
      if (newRewards && newRewards.length) {
        newRewards = newRewards.map(newReward => {
          return {
            ...newReward,
            reward: Number(newReward.reward),
            showOnCharts: true,
            payable: true
          }
        })
      }
      await SurveyEnrollment.update({
        id: surveyEnrollmentId
      }).set({
        savedRewards: newRewards
      })
      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  submitForcedSignUp: async (req, res) => {
    try {
      const { surveyEnrollment, email, password, country } = req.body

      if (!surveyEnrollment || !email) {
        return res.badRequest('Something went wrong')
      }

      let user = await User.findOne({ emailAddress: email.toLowerCase() })

      if (!user) {
        user = await User.create({
          emailAddress: email,
          password: password,
          country: country,
          type: 'taster',
          isTaster: true
        }).fetch()
        await emailService.sendWelcomeEmail({
          email,
          language: user.language
        })
      } else {
        if (password && country) {
          const updatedUser = await User.updateOne({ id: user.id }).set({
            password,
            country,
            type: user.type === 'respondent' ? `taster` : user.type
          })
          if (
            updatedUser &&
            updatedUser.password &&
            updatedUser.password !== ''
          ) {
            await emailService.sendWelcomeEmail({
              email,
              language: user.language
            })
          }
        }
      }

      await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        user: user.id
      })

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  submitAnswer: async (req, res) => {
    try {
      const {
        question,
        surveyEnrollment,
        value,
        startedAt,
        selectedProduct: product,
        rejectedEnrollment
      } = req.body
      // await Answer.destroy({ id: question, enrollment: surveyEnrollment })
      const targetQuestion = await Question.findOne({ id: question }).populate(
        'survey'
      )

      let hasScreener = false
      const survey = targetQuestion.survey
      if (survey.isScreenerOnly & rejectedEnrollment && survey.screenOut) {
        await updateRejectedEnrollment(
          surveyEnrollment,
          rejectedEnrollment,
          question
        )
      } else {
        const checkScreener = await getScreenerSurveys(survey.id)
        hasScreener = checkScreener.length > 0

        if (hasScreener) {
          const { user } = await SurveyEnrollment.findOne({
            where: { id: surveyEnrollment },
            select: ['user']
          })
          const surveyId = _.last(checkScreener)._id.toString()

          const rejSurvey = await SurveyEnrollment.find({
            where: { survey: surveyId, user },
            select: ['rejectedEnrollment']
          })

          const screenRejectedEnrollment =
            rejSurvey &&
            rejSurvey.length &&
            _.last(rejSurvey).rejectedEnrollment

          if (rejectedEnrollment) {
            await updateRejectedEnrollment(
              surveyEnrollment,
              rejectedEnrollment,
              question
            )
          } else if (screenRejectedEnrollment) {
            await SurveyEnrollment.update({
              id: surveyEnrollment
            }).set({
              rejectedEnrollment: true
            })
          }
        } else {
          if (rejectedEnrollment && survey.screenOut) {
            await updateRejectedEnrollment(
              surveyEnrollment,
              rejectedEnrollment,
              question
            )
          }
        }
      }

      if (!['active', 'draft'].includes(survey.state)) {
        throw new ApiError('E_SURVEY_IS_NOT_ACTIVE', 'The survey is not active')
      }

      const { typeOfQuestion } = targetQuestion
      let answersToCreate = []

      if (typeOfQuestion === 'paypal-email' && value[0]) {
        const updatedSurveyEnrollment = await SurveyEnrollment.update({
          id: surveyEnrollment
        })
          .set({
            paypalEmail: value[0]
          })
          .fetch()
        if (survey.isScreenerOnly && updatedSurveyEnrollment.length) {
          const user = await User.update({
            id: updatedSurveyEnrollment[0].user
          })
            .set({
              paypalEmailAddress: value[0]
            })
            .fetch()
        }
      }

      if (typeOfQuestion === 'email' && value && value[0]) {
        if (survey.authorizationType === 'public') {
          let user = await User.findOne({ emailAddress: value[0] })
          if (!user) {
            user = await User.create({ emailAddress: value[0] }).fetch()
          }
          await SurveyEnrollment.update({
            id: surveyEnrollment
          }).set({
            user: user.id
          })
        }
      }

      let accTimeToAnswer = []
      switch (typeOfQuestion) {
        case 'choose-product': {
          await SurveyEnrollment.addToCollection(
            surveyEnrollment,
            'selectedProducts',
            value
          )
          const answerValue = value && value.length ? value[0] : ''

          if (answerValue) {
            const currentEnrollment = await SurveyEnrollment.findOne({
              id: surveyEnrollment
            })
            const { savedRewards } = currentEnrollment
            const editedSavedRewards = savedRewards.map(sr => {
              if (sr.id === answerValue) {
                sr.startedAt = new Date().getTime()
              }
              return sr
            })

            await SurveyEnrollment.updateOne({ id: surveyEnrollment }).set({
              lastSelectedProduct: answerValue,
              savedRewards: editedSavedRewards
            })
          }
          answersToCreate = []
          break
        }
        case 'info':
        case 'profile':
          answersToCreate = []
          break
        case 'paypal-email':
          answersToCreate[0] = {
            question,
            startedAt,
            timeToAnswer: Date.now() - new Date(startedAt).getTime(),
            enrollment: surveyEnrollment,
            value
          }
          break
        case 'paired-questions':
          if (value) {
            answersToCreate = value.map((pair, index) => {
              const { pairQuestion, value: pairValue, timeToAnswer } = pair
              accTimeToAnswer[index] =
                timeToAnswer + (accTimeToAnswer[index - 1] || 0)
              const startedPairAt = startedAt + accTimeToAnswer[index]

              return {
                question,
                startedAt: startedPairAt,
                timeToAnswer,
                enrollment: surveyEnrollment,
                pairQuestion,
                product,
                value: [pairValue]
              }
            })
          }

          break
        case 'choose-one':
          const currentQuestion = await Question.findOne({ id: question })
          const currentEnrollment = await SurveyEnrollment.findOne({
            id: surveyEnrollment
          })
          const checkReducedReward =
            currentQuestion &&
            currentQuestion.options &&
            currentQuestion.options.length &&
            currentQuestion.options.find(
              option =>
                option.hasReduceReward && R.includes(option.value, value)
            )
          if (checkReducedReward) {
            const newRewards = currentEnrollment.savedRewards.map(
              savedReward => {
                if (savedReward.id === product) {
                  return {
                    ...savedReward,
                    reward: Number(checkReducedReward.reduceRewardValue)
                  }
                } else {
                  return savedReward
                }
              }
            )
            await SurveyEnrollment.update({
              id: surveyEnrollment
            }).set({
              savedRewards: newRewards
            })
          }
          answersToCreate[0] = {
            question,
            startedAt,
            timeToAnswer: Date.now() - new Date(startedAt).getTime(),
            enrollment: surveyEnrollment,
            value,
            product
          }
          break

        default:
          answersToCreate[0] = {
            question,
            startedAt,
            timeToAnswer: Date.now() - new Date(startedAt).getTime(),
            enrollment: surveyEnrollment,
            value,
            product
          }
          break
      }

      if (
        !question.required &&
        answersToCreate.length &&
        answersToCreate[0].value === undefined
      ) {
        await SurveyEnrollment.updateOne({ id: surveyEnrollment }).set({
          lastAnsweredQuestion: question
        })
        return res.ok([])
      }

      const answers = await Answer.createEach(answersToCreate).fetch()
      await SurveyEnrollment.updateOne({ id: surveyEnrollment }).set({
        lastAnsweredQuestion: question
      })
      return res.ok(answers)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  setSurveySelectedProducts: async (req, res) => {
    try {
      const { surveyEnrollment, selectedProducts } = req.body
      await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        selectedProducts
      })
      const updatedSurveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollment
      }).populate('selectedProducts')

      return res.ok(updatedSurveyEnrollment)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  setPaypalEmail: async (req, res) => {
    try {
      const { surveyEnrollment, paypalEmail } = req.body
      const updatedSurveyEnrollment = await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        paypalEmail
      })
      return res.ok(updatedSurveyEnrollment)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  setState: async (req, res) => {
    try {
      const { surveyEnrollment, state } = req.body
      const { state: currentState } = await SurveyEnrollment.findOne({
        id: surveyEnrollment
      })
      if (!canChangeSurveyState({ from: currentState, to: state })) {
        return res.forbidden()
      }
      const updatedSurveyEnrollment = await SurveyEnrollment.updateOne({
        id: surveyEnrollment
      }).set({
        state,
        finishedAt: state === 'finished' ? new Date() : ''
      })
      return res.ok(updatedSurveyEnrollment)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  sendSurveyStats: async (req, res) => {
    const { surveyEnrollment: surveyEnrollmentId } = req.body

    const surveyEnrollment = await SurveyEnrollment.findOne({
      id: surveyEnrollmentId
    }).populate('answers')
    await sendSurveyStats(surveyEnrollment)

    return res.ok()
  },
  finishSurvey: async (req, res) => {
    try {
      const { surveyEnrollment: surveyEnrollmentId } = req.body
      let surveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollmentId
      })
        .populate('answers')
        .populate('user')

      const { user } = surveyEnrollment
      if (user && user.blackListed) {
        const updatedSurveyEnrollment = await SurveyEnrollment.updateOne({
          id: surveyEnrollmentId
        }).set({
          state: 'finished',
          validation: 'invalid',
          processed: true,
          finishedAt: new Date()
        })

        surveyEnrollment = {
          ...surveyEnrollment,
          state: updatedSurveyEnrollment.state,
          finishedAt: updatedSurveyEnrollment.finishedAt,
          validation: updatedSurveyEnrollment.validation,
          processed: updatedSurveyEnrollment.proceed
        }
      } else {
        const updatedSurveyEnrollment = await SurveyEnrollment.updateOne({
          id: surveyEnrollmentId
        }).set({
          state: 'finished',
          finishedAt: new Date()
        })

        surveyEnrollment = {
          ...surveyEnrollment,
          state: updatedSurveyEnrollment.state,
          finishedAt: updatedSurveyEnrollment.finishedAt
        }
      }

      const survey = await Survey.findOne({ id: surveyEnrollment.survey })

      if (survey && survey.isScreenerOnly && user) {
        await updateLinkedSurveys(surveyEnrollment)
      }

      if (survey && survey.compulsorySurvey) {
        if (surveyEnrollment && user && user.id) {
          await updateUserForCompulsorySurvey(user.id)
        }
        if (
          surveyEnrollment &&
          surveyEnrollment.answers &&
          surveyEnrollment.answers.length &&
          survey.country &&
          user &&
          user.id
        ) {
          const userSocioEcnomicRating = await compulsorySurveyScoreCalculate(
            surveyEnrollment.answers,
            survey.country
          )
          if (userSocioEcnomicRating.grade && userSocioEcnomicRating.score) {
            await updateUserSocioEconomicRating(
              user.id,
              surveyEnrollment.id,
              userSocioEcnomicRating
            )

            surveyEnrollment = {
              ...surveyEnrollment,
              socioEconomicRating: userSocioEcnomicRating
            }
          }
        }
      }
      const updatedSurveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollmentId
      })

      await sendSurveyStats(surveyEnrollment)

      return res.ok(updatedSurveyEnrollment)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  rejectSurvey: async (req, res) => {
    try {
      const { surveyEnrollment: surveyEnrollmentId } = req.body
      const surveyEnrollment = await SurveyEnrollment.findOne({
        id: surveyEnrollmentId
      }).populate('answers')

      if (
        !canChangeSurveyState({ from: surveyEnrollment.state, to: 'rejected' })
      ) {
        return res.forbidden()
      }
      await SurveyEnrollment.updateOne({
        id: surveyEnrollmentId
      }).set({
        state: 'rejected',
        finishedAt: new Date()
      })

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  finishSurveyScreening: async (req, res) => {
    try {
      const { surveyEnrollment: surveyEnrollmentId } = req.body
      await SurveyEnrollment.updateOne({
        id: surveyEnrollmentId
      }).set({
        state: 'waiting-for-product'
      })

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyEnrollmentValidations: async (req, res) => {
    try {
      const { surveyId } = req.params
      const searchQuery = { ...requestUtils.parseSearchQuery(req), surveyId }
      const pipeline = createSurveyEnrollmentSearchPipeline(searchQuery)
      const { skip, limit } = searchQuery

      const surveyEnrollments = await surveyEnrollmentSearch.search(pipeline, {
        skip,
        limit,
        orderBy: { createdAt: -1 }
      })

      const questions = await Question.find({
        survey: surveyId,
        typeOfQuestion: 'upload-picture'
      })

      const questionIds = questions.map(question => {
        return question.id.toString()
      })
      const surveyEnrollementIds = surveyEnrollments.map(surveyEnrollment => {
        return surveyEnrollment.id.toString()
      })

      const answers = await Answer.find({
        where: {
          question: { in: questionIds },
          enrollment: { in: surveyEnrollementIds }
        }
      }).populate('question')
        .populate('product')
        .sort('createdAt ASC')
      
      surveyEnrollments.forEach(surveyEnrollment => {
        const surveyEnrollmentAnswers = answers.filter(answer => {
          return answer.enrollment.toString() === surveyEnrollment.id.toString()
        })

        surveyEnrollment.count = {
          id: surveyEnrollment.id,
          InvalidCount: 0,
          validCount: 0,
          payPalCount: 0
        }
        surveyEnrollment.answers = surveyEnrollmentAnswers
      })

      return res.ok(surveyEnrollments)
    } catch (error) {
      res.badRequest(error)
    }
  },
  countSurveyEnrollmentValidations: async (req, res) => {
    try {
      const { surveyId } = req.params
      const searchQuery = { ...requestUtils.parseSearchQuery(req), surveyId }
      const pipeline = createSurveyEnrollmentSearchPipeline(searchQuery)
      const count = await surveyEnrollmentSearch.count(pipeline)

      return res.ok(count)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getSurveyFunnel: async (req, res) => {
    try {
      const { query } = req.query
      const params = JSON.parse(query)
      const { surveyId, currentPage, perPage } = params
      if (!surveyId) return res.badRequest('Not Survey Id Provided')

      const survey = await Survey.findOne({ id: surveyId })

      if (!survey) return res.badRequest('Not Survey Found')

      const skipValue = (currentPage - 1) * perPage

      const total = await Question.count({
        survey: surveyId
      })

      const funnel = []

      const questions = await Question.find({
        where: {
          survey: surveyId
        },
        select: ['id', 'prompt', 'typeOfQuestion']
      })
        .sort('order ASC')
        .skip(skipValue)
        .limit(perPage)

      if (questions.length) {
        const answers = await answerSearch.funnel(questions)
        const distinctUser = await answerSearch.distincter(questions)
        if (answers) {
          const groupByQuestions = R.groupBy(answer => {
            return answer.question
          })
          const groupUserByQuestions = R.groupBy(distinctUsers => {
            return distinctUsers.question
          })
          const userByQuestions = groupUserByQuestions(distinctUser)
          const answersByQuestions = groupByQuestions(answers)
          questions.map(value => {
            funnel.push({
              question: value,
              product:
                answersByQuestions[value.id] &&
                answersByQuestions[value.id][0].product[0].id
                  ? answersByQuestions[value.id][0].product
                  : null,
              responses: answersByQuestions[value.id]
                ? answersByQuestions[value.id][0].responses
                : 0,
              userAnswer: userByQuestions[value.id]
                ? userByQuestions[value.id][0].countEnrollment
                : 0
            })
          })
        }
        return res.ok({
          total,
          funnel
        })
      }

      return res.ok({
        total: 0,
        funnel: []
      })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateSurveyEnrollementValidation: async (req, res) => {
    try {
      const { superUser, user: currentUserToken } = req.token

      const params = req.body
      const beforeUpdate = await SurveyEnrollment.findOne({ id: params.id })

      if (!beforeUpdate) {
        throw new Error('Not found')
      }

      let updatedSaveRewards = {}
      if (params && params.savedRewards && params.savedRewards.length) {
        updatedSaveRewards = {
          savedRewards: params.savedRewards
        }
      }

      let updated = await SurveyEnrollment.updateOne({ id: params.id }).set({
        comment: params.comment ? params.comment : '',
        validation: params.validation,
        processed: params.processed,
        escalation: params.escalation,
        hiddenFromCharts: params.hiddenFromCharts,
        ...updatedSaveRewards
      })

      if (params.user && params.user.id) {
        let updatedValues = {}
        const userClone = params.user
        const { id: blacklistId, blackListed } = userClone
        await User.updateOne({ id: blacklistId }).set({
          blackListed: blackListed
        })
        updatedValues['blacklisted'] = blackListed
        await Logs.create({
          by: currentUserToken.id,
          impersonatedBy: superUser && superUser.id ? superUser.id : null,
          action: 'update',
          type: 'update-user',
          relatedUser: blacklistId,
          collections: ['user'],
          values: updatedValues
        })
        updated = { ...updated, user: userClone }
      }
      let updatedValues = {}

      if (beforeUpdate.validation !== params.validation) {
        updatedValues['validation'] = params.validation
      }
      if (beforeUpdate.comment !== params.comment) {
        updatedValues['comment'] = params.comment
      }
      if (beforeUpdate.escalation !== params.escalation) {
        updatedValues['escalation'] = params.escalation
      }

      await Logs.create({
        by: currentUserToken.id,
        impersonatedBy: superUser && superUser.id ? superUser.id : null,
        action: 'update',
        type: 'photo-validation',
        relatedEnrollment: params.id,
        relatedSurvey: beforeUpdate.survey,
        collections: ['surveyenrollment', 'survey'],
        values: updatedValues
      })

      return res.ok(updated)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateSurveyEnrollementProductIncentives: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const params = req.body
      const currentSurvey = await Survey.findOne({
        id: params.surveyID
      }).populate('linkedSurveys', {
        select: ['id']
      })
      let updated = []
      if (currentSurvey.isScreenerOnly) {
        const allLinkedSurveyIds = R.pluck('id', currentSurvey.linkedSurveys)
        updated = await SurveyEnrollment.update({
          survey: { in: allLinkedSurveyIds },
          id: { in: params.enrollments }
        })
          .set({
            productIncentiveExported: true,
            paidIncentivesBy: currentUserToken.id,
            paidIncentivesOn: new Date()
          })
          .fetch()
      } else {
        updated = await SurveyEnrollment.update({
          survey: params.surveyID,
          id: { in: params.enrollments }
        })
          .set({
            productIncentiveExported: true,
            paidIncentivesBy: currentUserToken.id,
            paidIncentivesOn: new Date()
          })
          .fetch()
      }

      return res.ok({ updatedRecords: updated.length })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateSurveyEnrollementGiftCardIncentives: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const params = req.body
      const currentSurvey = await Survey.findOne({
        id: params.surveyID
      }).populate('linkedSurveys', {
        select: ['id']
      })
      let updated = []
      if (currentSurvey.isScreenerOnly) {
        const allLinkedSurveyIds = R.pluck('id', currentSurvey.linkedSurveys)
        updated = await SurveyEnrollment.update({
          survey: { in: allLinkedSurveyIds },
          id: { in: params.enrollments }
        })
          .set({
            giftCardIncentiveExported: true,
            paidIncentivesBy: currentUserToken.id,
            paidIncentivesOn: new Date()
          })
          .fetch()
      } else {
        updated = await SurveyEnrollment.update({
          survey: params.surveyID,
          id: { in: params.enrollments }
        })
          .set({
            giftCardIncentiveExported: true,
            paidIncentivesBy: currentUserToken.id,
            paidIncentivesOn: new Date()
          })
          .fetch()
      }
      return res.ok({ updatedRecords: updated.length })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateSurveyShareStatus: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const params = req.body
      const updated = await SurveyEnrollment.update({
        survey: params.surveyID,
        id: { in: params.enrollments }
      })
        .set({
          shareExported: true,
          paidShareBy: currentUserToken.id,
          paidShareOn: new Date()
        })
        .fetch()

      let userIds = R.pluck('user', updated)
      userIds = R.reject(R.isNil, userIds)
      if (userIds.length) {
        await User.update({ id: { in: userIds } })
          .set({
            completedReferredSurvey: true
          })
          .fetch()
      }
      return res.ok({ updatedRecords: updated.length })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  userEnrollements: async (req, res) => {
    try {
      const { userId } = req.params

      if (!req.token) res.unauthorized()

      const {
        user: { type }
      } = req.token

      if (!userRoleCheck(type)) {
        res.badRequest('user not allowed to perform this action')
      }

      const whereClause = {
        user: userId
      }

      const userEnrollements = await SurveyEnrollment.find({
        where: whereClause
      }).sort('createdAt DESC')

      const addExpiryToUserEnrollments =
        (userEnrollements &&
          userEnrollements.length &&
          userEnrollements.map(enrollment => {
            return {
              ...enrollment,
              expiredAt: moment(
                moment(enrollment.createdAt).add(
                  enrollment.allowedDaysToFillTheTasting,
                  'days'
                )
              ).format('x')
            }
          })) ||
        []

      res.send(addExpiryToUserEnrollments)
    } catch (error) {
      res.badRequest(error)
    }
  },
  updateUserEnrollementExpiry: async (req, res) => {
    try {
      const { id } = req.params
      const { allowedDaysToFillTheTasting } = req.body

      if (!req.token) res.unauthorized()

      const {
        user: { type }
      } = req.token

      if (!userRoleCheck(type)) {
        res.badRequest('user not allowed to perform this action')
      }
      let expiryDays = allowedDaysToFillTheTasting
      const surveyEnrollment = await SurveyEnrollment.findOne({ id: id })

      if (
        surveyEnrollment &&
        surveyEnrollment.allowedDaysToFillTheTasting &&
        Number(surveyEnrollment.allowedDaysToFillTheTasting)
      ) {
        expiryDays = expiryDays + surveyEnrollment.allowedDaysToFillTheTasting
      }

      const userEnrollementUpdated = await SurveyEnrollment.updateOne({
        id
      }).set({
        allowedDaysToFillTheTasting: expiryDays
      })

      res.send(userEnrollementUpdated)
    } catch (error) {
      res.badRequest(error)
    }
  },
  getFunnelValidation: async (req, res) => {
    try {
      const { query } = req.query
      const params = JSON.parse(query)
      const { productId, questionId } = params
      if (!productId) return res.badRequest('Not Product Id Provided')
      if (!questionId) return res.badRequest('Not Question Id Provided')

      const funnelAnswer = await getAnswerValidation(questionId, productId)
      if (funnelAnswer.length) {
        return res.ok({
          funnelAnswer
        })
      }

      return res.ok({
        funnelAnswer: []
      })
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
