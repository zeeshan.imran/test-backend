/* globals Survey, Question, Answer */
const R = require('ramda')
const { createQuestion, updateQuestion } = require('../../utils/questionUtils')
const { NO_NEXT_QUESTION } = require('../../utils/constants')
const logger = require('../../utils/logger')
const createLog = require('../../utils/createLog')
/**
 * QuestionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const syncSkipFlowSet = (idByClientGeneratedId, skipFlow) => {
  // NOTE: STAGFW-455 tested
  const syncRules = skipFlow => ({
    ...skipFlow,
    rules: skipFlow.rules.map(m => ({
      ...m,
      skipTo: idByClientGeneratedId[m.skipTo] || m.skipTo
    }))
  })

  // NOTE: STAGFW-455 tested
  const syncMultipleOptionRules = skipFlow => ({
    ...skipFlow,
    rules: skipFlow.rules.map(m => ({
      ...m,
      skipOptions: m.skipOptions.map(skipOption => ({
        ...skipOption,
        value: idByClientGeneratedId[skipOption.value] || skipOption.value
      }))
    }))
  })

  if (!skipFlow || !skipFlow.rules) {
    return {}
  }

  let syncPipe = syncRules

  if (skipFlow.type === 'MATCH_MULTIPLE_OPTION') {
    syncPipe = R.pipe(syncRules, syncMultipleOptionRules)
  }

  return { skipFlow: syncPipe(skipFlow) }
}

const syncRelatedQuestionsSet = (idByClientGeneratedId, question) => {
  if (!question.relatedQuestions) {
    return {}
  }

  // NOTE: STAGFW-455 tested
  return {
    relatedQuestions: question.relatedQuestions.map(
      id => idByClientGeneratedId[id] || id
    )
  }
}

const syncLikingQuestionSet = (idByClientGeneratedId, likingQuestion) => {
  if (!likingQuestion) {
    return {}
  }

  // NOTE: STAGFW-455 tested
  return {
    likingQuestion: idByClientGeneratedId[likingQuestion] || likingQuestion
  }
}

const removeUnusedQuestions = async (surveyId, keepIds) => {
  const survey = await Survey.findOne({ id: surveyId }).populate('questions')

  const lastIds = survey.questions.map(q => q.id)
  const unusedIds = R.without(keepIds)(lastIds)

  // it's safe because we only allow delete questions in draft
  if (unusedIds && unusedIds.length > 0) {
    await Answer.destroy({ question: { in: unusedIds } })
  }

  await Question.destroy({ id: { in: unusedIds } })
}

module.exports = {
  create: async (req, res) => {
    // NOTE: STAGFW-455 tested
    const questionInput = req.body
    res.send(await createQuestion(questionInput))
  },
  update: async (req, res) => {
    // NOTE: STAGFW-455 tested
    const id = req.params.id
    const questionInput = req.body
    res.send(await updateQuestion({ id, ...questionInput }))
  },
  findIds: async (req, res) => {
    try {
      const { ids } = req.body
      const questions = await Question.find({ id: { in: ids } }).populate(
        'pairs'
      )
      return res.ok(questions)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  replaceQuestions: async (req, res) => {
    try {
      const { user } = req.token
      const { questions } = req.body

      if (questions.length === 0) {
        return res.send([])
      }

      const surveyIds = R.pipe(
        R.map(q => q.survey),
        R.uniq
      )(questions)

      if (surveyIds.length > 1) {
        throw new Error('CAN_NOTREPLACE_QUESTIONS_ON_MULTIPLE_SURVEY')
      }

      const surveyId = surveyIds[0]
      const idByClientGeneratedId = {}
      for (const clientQuestion of questions) {
        const {
          clientGeneratedId,
          skipFlow,
          likingQuestion,
          ...question
        } = clientQuestion
        if (question.id) {
          const updatedQuestion = await updateQuestion(question, user.id)

          Object.assign(clientQuestion, updatedQuestion, {
            skipFlow,
            likingQuestion
          })
        } else {
          const createdQuestion = await createQuestion(question, user.id)
          idByClientGeneratedId[clientGeneratedId] = createdQuestion.id

          Object.assign(clientQuestion, createdQuestion, {
            skipFlow,
            likingQuestion
          })
        }
      }

      const payload = []
      for (let index = 0; index < questions.length; index++) {
        const {
          clientGeneratedId,
          skipFlow,
          likingQuestion,
          pairs,
          ...question
        } = questions[index]
        const nextQuestion = questions[index + 1]
        const updateSet = {
          nextQuestion: (nextQuestion && nextQuestion.id) || NO_NEXT_QUESTION,
          ...syncSkipFlowSet(idByClientGeneratedId, skipFlow),
          ...syncRelatedQuestionsSet(idByClientGeneratedId, question),
          ...syncLikingQuestionSet(idByClientGeneratedId, likingQuestion)
        }
        const oldQuestion = await Question.findOne(question.id).populate('nextQuestion')
        const updatedQuestion = await Question.updateOne({
          id: question.id
        }).set(updateSet)

        const newQuestion = await Question.findOne(question.id).populate('nextQuestion')
        createLog.question(user.id, question.survey, oldQuestion, newQuestion)

        payload.push({
          clientGeneratedId,
          question: {
            ...updatedQuestion.toJSON(),
            pairs,
            survey: updatedQuestion.survey,
            nextQuestion:
              updatedQuestion.nextQuestion && updatedQuestion.nextQuestion.id,
            likingQuestion:
              updatedQuestion.likingQuestion &&
              updatedQuestion.likingQuestion.id
          }
        })
      }

      // cleanup questions
      const keepIds = questions.map(q => q.id)
      await removeUnusedQuestions(surveyId, keepIds)

      res.send(payload)
    } catch (error) {
      logger.error(error)
      return res.badRequest(error)
    }
  },
  delete: async (req, res) => {
    try {
      const { questionId } = req.params
      await Question.destroy({ id: questionId })
      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
