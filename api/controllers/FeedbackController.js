/* global Feedback */
/**
 * FeedbackController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

 const wordpressToken = `${process.env.WORDPRESS_TOKEN}`

module.exports = {
  addFeedback: async (req, res) => {
    try {
      const { rating, email, reason, token } = req.body

      if (!token) {
        return res.badRequest('unauthorized')
      }
      if (token !== wordpressToken){
        return res.badRequest('unauthorized')
      }

      const newFeedback = await Feedback.create({rating, email, reason}).fetch()
      return res.ok(newFeedback)
    }catch (error) {
      return res.badRequest(error)
    }
  },  
}
