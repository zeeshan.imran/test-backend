/* globals Product */
/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const { createProduct, updateProduct } = require('../../utils/productUtils')
const createLog = require('../../utils/createLog')

module.exports = {
  create: async (req, res) => {
    try {
      const createdProduct = await createProduct(req.body)
      return res.ok(createdProduct)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  update: async (req, res) => {
    try {
      const updatedProduct = await updateProduct(req.params.id, req.body)

      return res.ok(updatedProduct)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  replaceProducts: async (req, res) => {
    const { user } = req.token
    const { surveyId, products } = req.body
    const payload = []
    const productsWithIds = products.map((product, index) => {
      if (!product.sortingOrderId) {
        return {
          ...product,
          sortingOrderId: index + 1
        }
      } else {
        return product
      }
    })

    for (let { clientGeneratedId, ...product } of productsWithIds) {
      if (product.id) {
        const { id, ...rest } = product
        const oldProduct = await Product.findOne({ id: id }).populate('brand')
        const updatedProduct = await updateProduct(id, rest)

        if (surveyId) {
          const newProduct = await Product.findOne({ id: id }).populate('brand')
          createLog.product(user.id, surveyId, oldProduct, newProduct)
        }

        payload.push({
          product: updatedProduct
        })
      } else {
        const newProduct = await createProduct(product)

        if (surveyId) {
          createLog.product(user.id, surveyId, null, newProduct)
        }

        payload.push({ clientGeneratedId, product: newProduct })
      }
    }

    res.send(payload)
  }
}
