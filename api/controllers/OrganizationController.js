/* globals Organization, User, Survey */
/**
 * OrganizationController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const ApiError = require('../../utils/ApiError')
const requestUtils = require('../../utils/requestUtils')
const {
  organizationSearch,
  createOrganizationSearchPipeline,
  createOrganizationSearchOrderBy
} = require('../services/searches')

module.exports = {
  organizations: async (req, res) => {
    try {
      const searchQuery = requestUtils.parseSearchQuery(req)
      const pipeline = createOrganizationSearchPipeline(searchQuery)
      const orderBy = createOrganizationSearchOrderBy(searchQuery)
      const { skip, limit } = searchQuery

      const sr = await organizationSearch.search(pipeline, {
        skip,
        limit,
        orderBy: orderBy || { name: 1 }
      })
      return res.ok(sr)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  countOrganizations: async (req, res) => {
    try {
      const searchQuery = requestUtils.parseSearchQuery(req)
      const pipeline = createOrganizationSearchPipeline(searchQuery)

      const countedOrganizations = await organizationSearch.count(pipeline)

      return res.ok(countedOrganizations)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  addOrganization: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      await Organization.create({
        createdBy: currentUser.id,
        name: req.body.name,
        uniqueName: req.body.uniqueName
      }).fetch()
      return res.ok(true)
    } catch (error) {
      if (error.code === 'E_UNIQUE') {
        throw new ApiError('E_ORG_UNIQUE')
      } else if (error.code === 'E_INVALID_NEW_RECORD') {
        throw new ApiError(error.code)
      }
      return res.badRequest(error)
    }
  },
  editOrganization: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      const newOrganization = await Organization.updateOne({
        id: req.body.id
      }).set({
        modifiedBy: currentUser.id,
        name: req.body.name
      })
      return res.ok(newOrganization)
    } catch (error) {
      if (error.code === 'E_UNIQUE') {
        throw new ApiError('E_ORG_UNIQUE')
      } else if (error.code === 'E_INVALID_NEW_RECORD') {
        throw new ApiError(error.code)
      }
      return res.badRequest(error)
    }
  },
  deleteOrganization: async (req, res) => {
    try {
      const organizationId = req.body.id

      // const users = await User.find({
      //   organization: organizationId,
      //   inactive: false,
      //   initialized: true
      // })
      // if (users.length) {
      //   throw new ApiError(
      //     'UNAUTHORIZED',
      //     'You are not allowed to delete this organization'
      //   )
      // }

      await Organization.updateOne({ id: organizationId }).set({
        inactive: true
      })

      await User.update({ organization: organizationId, inactive: false }).set({
        inactive: true
      })

      const surveys = await Survey.find({ owner: organizationId })

      for (let survey of surveys) {
        await Survey.updateOne({ uniqueName: survey.uniqueName }).set({
          state: 'deprecated',
          uniqueName: `${
            survey.uniqueName
          }-deprecated-at-${new Date().toISOString()}`
        })
      }

      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  unarchivedOrganization: async (req, res) => {
    try {
      const { id } = req.params

      await Organization.updateOne({ id: id }).set({ inactive: false })

      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  organization: async (req, res) => {
    const currentUser = await User.findOne({ id: req.token.user.id })
    let where = req.query && req.query.where && JSON.parse(req.query.where)
    where = Object.assign(
      where,
      !currentUser.isSuperAdmin && { id: currentUser.organization }
    )
    const organization = await Organization.find().where(where)
    res.ok(organization)
  },
  getUsers: async (req, res) => {
    try {
      const { types } = req.body
      const members = await User.find({
        organization: req.params.id,
        type: { in: types },
        inactive: false
      })

      return res.ok(members)
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
