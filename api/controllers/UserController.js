/* globals Logs, User, SurveyEnrollment, Survey, Organization, jwToken, process, emailService, Answer _ */
/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const bcrypt = require('bcryptjs')
const isPermanentAccount = require('../../utils/isPermanentAccount')
const toLower = require('../../utils/toLower')
const requestUtils = require('../../utils/requestUtils')
const ApiError = require('../../utils/ApiError')
const moment = require('moment')
const R = require('ramda')
const { getScreenerSurveys } = require('../services/surveyService')
const emailChecker = require('../../utils/emailChecker')

const {
  userSearch,
  createUserSearchPipeline,
  createUserSearchOrderBy
} = require('../services/searches')

const { getProductDisplay } = require('../../utils/ProductDisplayCombination')
const generateRandomEmail = salt => {
  return `mystery-${salt}@flavorwiki-${salt}.com`
}

const updateUserRecordIfDeleted = async (userData, currentUser, req, res) => {
  if (userData) {
    let { emailAddress } = userData
    emailAddress = toLower(emailAddress)
    const prevProfile = await User.findOne({ emailAddress: emailAddress })
    if (prevProfile && prevProfile.inactive) {
      userData['inactive'] = false

      if (currentUser.isSuperAdmin) {
        userData['organization'] = req.body.organization
      } else if (currentUser.type === 'power-user') {
        userData['organization'] = currentUser.organization
      }

      const newUser = await User.updateOne({ id: prevProfile.id }).set(userData)
      return res.ok(newUser)
    }
  }
  throw new ApiError('E_UNIQUE_USER')
}

const logUserToSurvey = async (req, res) => {
  const {
    email: inputEmail,
    survey: surveyId,
    referral,
    extsource,
    extid,
    password,
    country,
    browserInfo
  } = req.body
  let email = toLower(inputEmail)
  let surveyEnrollment
  let referralUser
  let isReferredUser = false
  let createEnrollment = false

  const populatedSurvey = await Survey.findOne(
    { id: surveyId },
    { questions: true, exclusiveTasters: true }
  ).populate('products', {
    select: ['id', 'reward', 'isAvailable', 'rejectedReward', 'amountInDollar', 'delayToNextProduct', 'extraDelayToNextProduct']
  })
  const { products, productRewardsRule = {}, dualCurrency } = populatedSurvey

  let rewards =
    products && products.length
      ? products
          .filter(product => product.isAvailable)
          .map(product => ({
            id: product.id,
            answered: false,
            reward:
              productRewardsRule.active && productRewardsRule.max
                ? productRewardsRule.max
                : product.reward,
            amountInDollar: dualCurrency ? product.amountInDollar : 0,
            delayToNextProduct: product.delayToNextProduct,
            extraDelayToNextProduct: product.extraDelayToNextProduct
          }))
      : []
          
  if (!populatedSurvey) {
    return res.badRequest('No survey found for the given id')
  }

  const {
    referralAmount: enrollmentReferralAmount,
    referralAmountInDollar
  } = populatedSurvey
  const enrollmentReferralAmountInDollar = dualCurrency
    ? referralAmountInDollar
    : 0
  if (referral) {
    referralUser = await User.findOne({ id: referral })
    isReferredUser = true
  }

  if (referralUser && referralUser.inactive) {
    return res.badRequest('Not a valid account')
  }

  if (referralUser && referralUser.emailAddress === email) {
    return res.badRequest(`Users can't refer themselves`)
  }

  if (populatedSurvey.authorizationType === 'selected') {
    if (!email) {
      return res.badRequest(`Need an e-mail to login`)
    }

    let foundTaster = populatedSurvey.exclusiveTasters.find(
      taster => taster.emailAddress === email
    )

    if (
      populatedSurvey.state === 'draft' &&
      req.token &&
      req.token.user.organization === populatedSurvey.owner
    ) {
      foundTaster = req.token.user
      populatedSurvey.allowRetakes = true
    }
    if (!foundTaster) {
      return res.forbidden("This taster isn't allowed to participate")
    }
    if (
      !populatedSurvey.allowRetakes &&
      populatedSurvey.authorizationType !== 'public'
    ) {
      const hasTakenPreviously = await SurveyEnrollment.find({
        survey: surveyId,
        user: foundTaster.id,
        state: ['finished', 'rejected']
      })
      if (hasTakenPreviously.length) {
        return res.badRequest(
          `Retakes not allowed - ${hasTakenPreviously[0]['state']}`
        )
      }
    }
    surveyEnrollment = await SurveyEnrollment.findOne({
      user: foundTaster.id,
      survey: surveyId,
      state: { nin: ['finished', 'rejected'] }
    })
      .populate('lastAnsweredQuestion')
      .populate('survey')
      .populate('user')
      .populate('selectedProducts')
    // logic for screener
    let hasScreener = false
    let rejectedEnrollment = false

    let referralToFill = referral
    if (foundTaster && foundTaster.completedReferredSurvey) {
      referralToFill = ''
    }

    if (!surveyEnrollment) {
      const checkScreener = await getScreenerSurveys(surveyId)
      hasScreener = checkScreener.length > 0
      if (hasScreener && req.token && req.token.user) {
        const screenerSurveyId = _.last(checkScreener)._id.toString()
        const rejectedSurvey = await SurveyEnrollment.find({
          where: { survey: screenerSurveyId, user: req.token.user.id },
          select: ['rejectedEnrollment']
        })
        const screenRejectedEnrollment =
          rejectedSurvey &&
          rejectedSurvey.length &&
          _.last(rejectedSurvey).rejectedEnrollment
        if (screenRejectedEnrollment) {
          rewards =
            products && products.length
              ? products
                  .filter(product => product.isAvailable)
                  .map(product => ({
                    id: product.id,
                    reward: product.rejectedReward,
                    amountInDollar: product.amountInDollar,
                    answered: false,
                    delayToNextProduct: product && product.delayToNextProduct,
                    extraDelayToNextProduct:
                      product && product.extraDelayToNextProduct
                  }))
              : []
          rejectedEnrollment = true
        }
      }
      const {
        productDisplay,
        productDisplayOrder,
        productDisplayType
      } = await getProductDisplay(surveyId)
      
      surveyEnrollment = await SurveyEnrollment.create({
        user: foundTaster.id,
        survey: surveyId,
        lastAnsweredQuestion: null,
        referral: referralToFill,
        savedRewards: rewards,
        browserInfo: browserInfo || [],
        productDisplay: productDisplay,
        productDisplayOrder: productDisplayOrder,
        productDisplayType: productDisplayType,
        allowedDaysToFillTheTasting:
          populatedSurvey.allowedDaysToFillTheTasting,
        state: 'waiting-for-product',
        productRewardsRule: productRewardsRule,
        enrollmentReferralAmount,
        enrollmentReferralAmountInDollar,
        rejectedEnrollment
      }).fetch()
    }
    const answers = await Answer.find({
      enrollment: surveyEnrollment.id
    })
    const expired =
      moment(surveyEnrollment.createdAt).add(
        surveyEnrollment.allowedDaysToFillTheTasting,
        'days'
      ) < moment()
    if (expired && !populatedSurvey.allowRetakes) {
      return res.badRequest('Survey expired')
    }
    return res.ok({
      ...surveyEnrollment,
      answers
    })
  }
  // Mystery Shoppers
  if (populatedSurvey.authorizationType === 'public' && extsource && extid) {
    // Not by survey but overall to find the user
    const mysteryEnrollments = await SurveyEnrollment.find({
      extsource,
      extid,
      user: {
        nin: ['']
      }
    }).populate('user')

    let mysteryUser = {}

    if (mysteryEnrollments.length) {
      mysteryUser = mysteryEnrollments[0].user
    }

    if (!Object.keys(mysteryUser).length) {
      const mysteryEmail = generateRandomEmail(extid)
      mysteryUser = await User.create({
        emailAddress: mysteryEmail,
        type: 'taster',
        isTaster: true,
        isReferredUser,
        source: extsource || '',
        isMysteryShopper: true
      }).fetch()
    }
    email = mysteryUser.emailAddress
  }

  if (!email) {
    const {
      productDisplay,
      productDisplayOrder,
      productDisplayType
    } = await getProductDisplay(surveyId)
    
    surveyEnrollment = await SurveyEnrollment.create({
      survey: surveyId,
      lastAnsweredQuestion: null,
      referral: referral,
      savedRewards: rewards,
      browserInfo: browserInfo || [],
      productDisplay: productDisplay,
      productDisplayOrder: productDisplayOrder,
      productDisplayType: productDisplayType,
      productRewardsRule: productRewardsRule,
      allowedDaysToFillTheTasting: populatedSurvey.allowedDaysToFillTheTasting,
      enrollmentReferralAmount,
      enrollmentReferralAmountInDollar
    }).fetch()
    return res.ok(surveyEnrollment)
  }

  let user = await User.findOne({ emailAddress: email })

  if (!user) {
    user = await User.create({
      emailAddress: email,
      password,
      country,
      type: 'taster',
      isTaster: true,
      isReferredUser
    }).fetch()
    if (user && user.password && user.password !== '') {
      await emailService.sendWelcomeEmail({
        email,
        language: user.language
      })
    }
  } else {
    if (!user.password || user.password === '') {
      const updatedUser = await User.updateOne({ id: user.id }).set({
        password,
        country,
        isReferredUser,
        type: user.type === 'respondent' ? `taster` : user.type
      })
      if (updatedUser && updatedUser.password && updatedUser.password !== '') {
        await emailService.sendWelcomeEmail({
          email,
          language: user.language
        })
      }
    }
    if (
      !populatedSurvey.allowRetakes &&
      populatedSurvey.authorizationType !== 'public'
    ) {
      const hasTakenPreviously = await SurveyEnrollment.find({
        survey: surveyId,
        user: user.id,
        state: ['finished', 'rejected']
      })
      if (hasTakenPreviously.length) {
        return res.badRequest(
          `Retakes not allowed - ${hasTakenPreviously[0]['state']}`
        )
      }
    }
  }
  surveyEnrollment = await SurveyEnrollment.findOne({
    user: user.id,
    survey: surveyId,
    state: { nin: ['finished', 'rejected'] }
  })
    .populate('lastAnsweredQuestion')
    .populate('survey')
    .populate('user')
    .populate('selectedProducts')

  createEnrollment = false
  if (
    surveyEnrollment &&
    (populatedSurvey.authorizationType === 'enrollment' ||
      populatedSurvey.authorizationType === 'selected')
  ) {
    const expired =
      moment(surveyEnrollment.createdAt).add(
        surveyEnrollment.allowedDaysToFillTheTasting,
        'days'
      ) < moment()
    if (expired) {
      if (!populatedSurvey.allowRetakes) {
        return res.badRequest('Survey expired')
      }
      // Need to ask Luca WHY?
      SurveyEnrollment.updateOne({ id: surveyEnrollment.id }).set({
        state: 'rejected'
      })
      createEnrollment = true
    }
  }

  if (!surveyEnrollment || createEnrollment) {
    const {
      productDisplay,
      productDisplayOrder,
      productDisplayType
    } = await getProductDisplay(surveyId)
    let referredByEnrollment = referral
    if (user.isReferredUser && user.referredByEnrollment) {
      const referredEnrollment = await SurveyEnrollment.findOne({
        id: user.referredByEnrollment
      })

      // GETTING TASTINGS IF SURVEY IS A SCREENER
      let searchSurveyList = [surveyId]
      const mainSurvey = await Survey.findOne({
        id: surveyId
      }).populate('linkedSurveys', {
        select: ['id']
      })

      if (mainSurvey && mainSurvey.isScreenerOnly) {
        searchSurveyList = searchSurveyList.concat(
          R.pluck('id', mainSurvey.linkedSurveys)
        )
      }

      referredByEnrollment =
        referredEnrollment &&
        referredEnrollment.survey &&
        searchSurveyList.indexOf(referredEnrollment.survey) > -1
          ? referredEnrollment.id
          : referral
    }
    if (user && user.completedReferredSurvey) {
      referredByEnrollment = ''
    }
    
    surveyEnrollment = await SurveyEnrollment.create({
      user: user.id,
      survey: surveyId,
      lastAnsweredQuestion: null,
      referral: referredByEnrollment,
      savedRewards: rewards,
      browserInfo: browserInfo || [],
      productDisplay: productDisplay,
      productDisplayOrder: productDisplayOrder,
      productDisplayType: productDisplayType,
      productRewardsRule: productRewardsRule,
      allowedDaysToFillTheTasting: populatedSurvey.allowedDaysToFillTheTasting,
      enrollmentReferralAmount,
      extsource,
      extid,
      enrollmentReferralAmountInDollar
    }).fetch()
  }

  const answers = await Answer.find({
    enrollment: surveyEnrollment.id
  })
  return res.ok({
    ...surveyEnrollment,
    answers
  })
}

const validCountFromUser = userId => {
  return new Promise(async (resolve, reject) => {
    if (userId) {
      const validCount = SurveyEnrollment.count({
        user: userId,
        validation: 'valid',
        processed: true
      })

      validCount.then(validCount => resolve({ validCount }))
    } else {
      resolve({ validCount: 0 })
    }
  })
}

const invalidCountFromUser = userId => {
  return new Promise(async (resolve, reject) => {
    if (userId) {
      const invalidCount = SurveyEnrollment.count({
        user: userId,
        validation: 'invalid',
        processed: true
      })

      invalidCount.then(invalidCount => resolve({ invalidCount }))
    } else {
      resolve({ invalidCount: 0 })
    }
  })
}
const countOfPayPal = users => {
  return new Promise((resolve, reject) => {
    if (users.paypalEmailAddress) {
      const payPalCount = User.count({
        paypalEmailAddress: users.paypalEmailAddress
      })
      return resolve(payPalCount)
    } else return resolve(0)
  })
}

module.exports = {
  me: async (req, res) => {
    try {
      const { user } = req.token

      return res.ok(user)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  tasterDashboard: async (req, res) => {
    /* Logic implemented
      1 - Survey has to be active
      2 - Survey's country has to match the users country
      3 - User will see all the screenerOnly surveys from its country only
      4 - User will see the linkedSurvey only if they complete and qualify from screenerOnly survey
      5 - User can see all the surveys he's been selected
      6 - Compulsory surveys for country
    */
    try {
      let { user: currentUserToken } = req.token
      const user = await User.findOne({
        id: currentUserToken.id
      }).populate('exclusiveSurveys')

      user.surveyEnrollments = await SurveyEnrollment.find({
        user: user.id
      }).sort([{ createdAt: 'DESC' }])

      let compulsorySurveys = []

      if (!user.isCompulsorySurveyTaken && user.type === 'taster') {
        compulsorySurveys = await Survey.find({
          where: {
            or: [
              {
                authorizationType: 'enrollment',
                state: 'active',
                owner: user.organization,
                country: user.country,
                compulsorySurvey: true,
                showOnTasterDashboard: true
              }
            ]
          }
        }).sort([{ createdAt: 'DESC' }])
      }

      if (compulsorySurveys.length) {
        user.compulsorySurveyId = compulsorySurveys[0].id
      }
      const exclusivIds = user.exclusiveSurveys.map(exclusiveSurvey => {
        return exclusiveSurvey.id
      })
      const screenerSurveys = await Survey.find({
        isScreenerOnly: true,
        owner: user.organization,
        state: 'active'
      }).populate('linkedSurveys')
      let linkedSurveyIds = []
      screenerSurveys.forEach(survey => {
        if (survey.linkedSurveys) {
          return survey.linkedSurveys.forEach(linkedSurvey => {
            linkedSurveyIds.push(linkedSurvey.id)
          })
        }
      })
      user.exclusiveSurveys = await Survey.find({
        where: {
          or: [
            {
              id: { in: exclusivIds },
              state: 'active',
              owner: user.organization,
              country: user.country
            },
            {
              authorizationType: 'public',
              state: 'active',
              owner: user.organization,
              showOnTasterDashboard: true,
              compulsorySurvey: false
            },
            {
              isScreenerOnly: true,
              authorizationType: { nin: ['public'] },
              state: 'active',
              owner: user.organization,
              country: user.country,
              id: { nin: linkedSurveyIds },
              showOnTasterDashboard: true
            },
            {
              state: 'active',
              authorizationType: { nin: ['public'] },
              country: user.country,
              owner: user.organization,
              id: { nin: linkedSurveyIds },
              showOnTasterDashboard: true
            }
          ]
        }
      })
        .populate('products')
        .populate('linkedSurveys', { state: 'active' })
        .sort([{ createdAt: 'DESC' }])
      //  ********************************************* Need to refactor this *********************************************//
      let linkedSurveys = []
      user.exclusiveSurveys.forEach(survey => {
        if (survey.isScreenerOnly && survey.linkedSurveys) {
          return survey.linkedSurveys.forEach(linkedSurvey => {
            linkedSurveys.push(linkedSurvey.id)
          })
        }
      })
      const foundLinkedSurveys = await Survey.find({
        id: { in: linkedSurveys }
      }).populate('products')
      user.exclusiveSurveys.forEach(survey => {
        if (survey.isScreenerOnly && survey.linkedSurveys) {
          survey.linkedSurveys.forEach(ls => {
            const l = foundLinkedSurveys.find(s => s.id === ls.id)
            ls.products = l.products
          })
        }
      })
      //  ********************************************* Need to refactor this *********************************************//

      return res.ok(user)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  users: async (req, res) => {
    try {
      let { user: currentUserToken, superUser } = req.token
      if (superUser) {
        currentUserToken = superUser
      }
      const currentUser = await User.findOne({ id: currentUserToken.id })

      const searchQuery = requestUtils.parseSearchQuery(req)
      const { skip, limit } = searchQuery

      if (!currentUser.isSuperAdmin) {
        searchQuery.organization = currentUser.organization
      }

      const pipeline = createUserSearchPipeline(searchQuery)
      const orderBy = createUserSearchOrderBy(searchQuery)

      const sr = await userSearch.search(pipeline, {
        skip,
        limit,
        orderBy: orderBy || false
      })

      return res.ok(sr)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  countUsers: async (req, res) => {
    try {
      let { user: currentUserToken, superUser } = req.token
      if (superUser) {
        currentUserToken = superUser
      }
      const currentUser = await User.findOne({ id: currentUserToken.id })

      const searchQuery = requestUtils.parseSearchQuery(req)

      if (!currentUser.isSuperAdmin) {
        searchQuery.organization = currentUser.organization
      }

      const pipeline = createUserSearchPipeline(searchQuery)

      const sr = await userSearch.count(pipeline)
      return res.ok(sr)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  addUser: async (req, res) => {
    let currentUser
    let newUserData
    try {
      const { user: currentUserToken } = req.token
      currentUser = await User.findOne({ id: currentUserToken.id })
      newUserData = {
        emailAddress: toLower(req.body.emailAddress),
        fullName: req.body.fullName,
        type: req.body.type,
        gender: req.body.gender && req.body.gender,
        country: req.body.country && req.body.country,
        state: req.body.state && req.body.state,
        language: req.body.language && req.body.language,
        paypalEmailAddress: toLower(req.body.paypalEmailAddress)
      }
      if (currentUser.isSuperAdmin) {
        newUserData = {
          ...newUserData,
          inactive: false,
          organization: req.body.organization,
          isSuperAdmin: req.body.isSuperAdmin,
          isTaster: req.body.isTaster
        }
      } else if (currentUser.type === 'power-user') {
        newUserData = {
          ...newUserData,
          inactive: false,
          type: 'analytics',
          organization: currentUser.organization
        }
      } else {
        throw new Error('Not alowed')
      }

      Object.keys(newUserData).forEach(key => {
        if (newUserData[key] === null) {
          delete newUserData[key]
        }
      })

      const newUser = await User.create(newUserData).fetch()
      return res.ok(newUser)
    } catch (error) {
      if (error.code === 'E_INVALID_NEW_RECORD') {
        throw new ApiError(error.code)
      }
      if (error.code === 'E_UNIQUE') {
        const updatedRecord = await updateUserRecordIfDeleted(
          newUserData,
          currentUser,
          req,
          res
        )
        return updatedRecord
      } else {
        return res.badRequest(error)
      }
    }
  },
  editUser: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      let newUserData = {
        emailAddress: toLower(req.body.emailAddress),
        fullName: req.body.fullName,
        type: req.body.type,
        gender: req.body.gender && req.body.gender,
        country: req.body.country && req.body.country,
        state: req.body.state && req.body.state,
        language: req.body.language && req.body.language,
        paypalEmailAddress: toLower(req.body.paypalEmailAddress)
      }

      if (currentUser.isSuperAdmin) {
        newUserData = {
          ...newUserData,
          inactive: false,
          organization: req.body.organization,
          isSuperAdmin: req.body.isSuperAdmin,
          isTaster: req.body.isTaster
        }
      } else if (currentUser.type === 'power-user') {
        newUserData = {
          ...newUserData,
          inactive: false,
          type: 'analytics',
          organization: currentUser.organization
        }
      } else {
        throw new Error('Not alowed')
      }

      Object.keys(newUserData).forEach(key => {
        if (newUserData[key] === null) {
          delete newUserData[key]
        }
      })

      const newUser = await User.updateOne({ id: req.body.id }).set(newUserData)
      return res.ok(newUser)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateUser: async (req, res) => {
    try {
      const { user } = req.token
      if (!user) {
        return res.forbidden('Not authorized')
      }

      const currentLoggedInUser = await User.findOne({
        emailAddress: toLower(user.emailAddress)
      })

      const { body } = req
      const { id } = req.params

      if (
        id === currentLoggedInUser.id ||
        currentLoggedInUser.isSuperAdmin ||
        currentLoggedInUser.type === 'power-user'
      ) {
        const userUpdated = await User.updateOne({ id: id }).set(body)
        return res.ok(userUpdated)
      } else {
        return res.forbidden('Not authorized')
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  deleteUser: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      if (currentUser.isSuperAdmin) {
        await User.updateOne({ id: req.body.id }).set({ inactive: true })
      } else if (currentUser.type === 'power-user') {
        await User.updateOne({
          id: req.body.id,
          organization: currentUser.organization
        }).set({ inactive: true })
      } else {
        throw new Error('Not alowed')
      }
      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  unarchivedUser: async (req, res) => {
    try {
      const { id } = req.params

      await User.updateOne({ id }).set({ inactive: false })

      return res.ok(true)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  // Black list user logic
  blackListUser: async (req, res) => {
    const { user: currentUserToken, superUser } = req.token
    const { id } = req.body
    const currentUser = await User.findOne({ id: currentUserToken.id })
    const blacListedUser = await User.findOne({ id: id })
    let updatedValues = {}
    if (currentUser.isSuperAdmin) {
      await User.updateOne({ id: req.body.id }).set({
        blackListed: !blacListedUser.blackListed
      })
      updatedValues['blacklisted'] = !blacListedUser.blackListed
    } else if (currentUser.type === 'power-user') {
      await User.updateOne({
        id: id,
        organization: currentUser.organization
      }).set({ blackListed: !blacListedUser.blackListed })
      updatedValues['blacklisted'] = !blacListedUser.blackListed
    } else {
      throw new Error('Not alowed')
    }
    await Logs.create({
      by: currentUserToken.id,
      impersonatedBy: superUser && superUser.id ? superUser.id : null,
      action: 'update',
      type: 'update-user',
      relatedUser: id,
      collections: ['user'],
      values: updatedValues
    })
    return res.ok(true)
  },
  loginToSurvey: async (req, res) => {
    try {
      return await logUserToSurvey(req, res)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  loginToSurveyWithAuth: async (req, res) => {
    try {
      return await logUserToSurvey(req, res)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  loginUser: async (req, res) => {
    try {
      const { email: inputEmail, password } = req.body
      const email = toLower(inputEmail)
      const user = await User.findOne({ emailAddress: email })
      if (!user) {
        return res.badRequest('INVALID:Username or password are not correct.')
      }

      if (user && user.inactive) {
        return res.badRequest('user.inactive')
      }

      if (!isPermanentAccount(user.type)) {
        return res.badRequest('Not a valid account')
      }

      if (password && bcrypt.compareSync(password, user.password)) {
        const token = jwToken.issue(
          Object.assign(
            {},
            _.pick(user, [
              'id',
              'emailAddress',
              'fullName',
              'organization',
              'type',
              'country'
            ]),
            {
              isSuperAdmin: user.isSuperAdmin ? true : undefined,
              apiVersion: process.env.USER_API_VERSION
            }
          )
        )
        const response = {
          user: { ...user, isSuperAdmin: user.isSuperAdmin },
          token
        }
        return res.ok(response)
      } else {
        return res.badRequest('INVALID:Username or password are not correct.')
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  createTasterAccount: async (req, res) => {
    let {
      emailAddress,
      password,
      type,
      country,
      state,
      language,
      isDummyTaster,
      referral,
      source
    } = req.body

    emailAddress = toLower(emailAddress)

    const isValidEmail = await emailChecker('create-taster', req)
    if (!isValidEmail) {
      return res.badRequest('email.invalid')
    }

    const existUser = await User.findOne({ emailAddress })
    if (existUser && existUser.inactive) {
      return res.badRequest(
        'Your account was disabled, Please contact support for more information'
      )
    }

    try {
      let newUserData = {
        emailAddress: emailAddress,
        password: password,
        type: type,
        country: country,
        state: state,
        language: language,
        sendPromotionalEmail: true,
        source
      }
      if (isDummyTaster) {
        newUserData.type = 'operator'
        newUserData.isDummyTaster = isDummyTaster
        newUserData.isTaster = true
      }
      if (referral) {
        newUserData.isReferredUser = true
        newUserData.referredByEnrollment = referral
      }
      const hasOperatorAccount = await User.findOne({
        emailAddress,
        type: 'operator'
      })
      if (hasOperatorAccount) {
        const superAdmin = await User.find({ isSuperAdmin: true })

        if (superAdmin.length) {
          await emailService.sendOperatorRequestTasterAccountEmail({
            adminEmail: toLower(superAdmin[0].emailAddress),
            emailAddress
          })
        }

        return res.badRequest('Operator Account Exists')
      }
      const org = await Organization.findOne({
        uniqueName: isDummyTaster ? 'demo-company' : 'flavor-wiki'
      })
      if (!newUserData.organization) {
        newUserData.organization = org.id
      }
      const newUser = await User.create(newUserData).fetch()
      if (newUser) {
        const token = jwToken.issue(
          Object.assign(
            {},
            _.pick(newUser, [
              'id',
              'emailAddress',
              'fullName',
              'organization',
              'type'
            ]),
            {
              isSuperAdmin: newUser.isSuperAdmin ? true : undefined,
              apiVersion: process.env.USER_API_VERSION
            }
          )
        )
        if (
          (isDummyTaster || newUser.type === 'taster') &&
          newUser.password !== ''
        ) {
          const salt = bcrypt.genSaltSync(10)
          const verificationToken = bcrypt.hashSync(emailAddress, salt)

          await User.updateOne({ emailAddress: emailAddress }).set({
            verificationToken: verificationToken,
            verificationTokenExpiresAt: Date.now() + 24 * 60 * 60 * 1000 // now + 1 day in ms
          })

          const verificationLink = `${
            process.env.APP_URL
          }/onboarding/verify-email?token=${verificationToken}&email=${encodeURIComponent(
            emailAddress
          )}`
          await emailService.sendVerificationEmail({
            email: newUser.emailAddress,
            verifyLink: verificationLink,
            language: newUser.language || 'en'
          })
        }

        const response = {
          user: newUser,
          token
        }
        return res.ok(response)
      } else {
        return res.badRequest('INVALID:Username or password are not correct.')
      }
    } catch (error) {
      if (error.code === 'E_UNIQUE') {
        return res.badRequest('Email Address already in use.')
      } else return res.badRequest(error)
    }
  },
  tasterResendVerificationEmail: async (req, res) => {
    try {
      let { emailAddress } = req.body
      if (!emailAddress) return res.badRequest('Email address is mandatory.')
      emailAddress = toLower(emailAddress)
      const userDetail = await User.findOne({
        emailAddress
      })
      if (!userDetail) {
        return res.badRequest('Email address not found.')
      } else if (userDetail.type === 'taster') {
        const salt = bcrypt.genSaltSync(10)
        const verificationToken = bcrypt.hashSync(emailAddress, salt)

        await User.updateOne({ emailAddress: emailAddress }).set({
          verificationToken: verificationToken,
          verificationTokenExpiresAt: Date.now() + 24 * 60 * 60 * 1000 // now + 1 day in ms
        })

        const verificationLink = `${
          process.env.APP_URL
        }/onboarding/verify-email?token=${verificationToken}&email=${encodeURIComponent(
          emailAddress
        )}`
        await emailService.sendVerificationEmail({
          email: userDetail.emailAddress,
          verifyLink: verificationLink,
          language: userDetail.language || 'en'
        })
      }
      const response = {
        user: userDetail
      }
      return res.ok(response)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  updateTasterAccount: async (req, res) => {
    try {
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      req.body['initialized'] = true
      if (currentUser.id === req.body.id) {
        const formData = {
          ...req.body
        }

        if (formData.hasChildren === 'No' || req.body.numberOfChildren === '') {
          formData.numberOfChildren = 0
          formData.childrenAges = ''
        }

        const newUser = await User.updateOne({ id: req.body.id }).set(formData)
        return res.ok(newUser)
      } else {
        return res.forbidden('Not authorized')
      }
    } catch (error) {
      return res.badRequest(error.message)
    }
  },
  updateUserPassword: async (req, res) => {
    try {
      const { currentPassword, newPassword } = req.body
      const { user: currentUserToken } = req.token
      const currentUser = await User.findOne({ id: currentUserToken.id })
      if (!currentUser) {
        throw new Error('Not Authorized')
      }
      if (bcrypt.compareSync(currentPassword, currentUser.password)) {
        await User.updateOne({ id: currentUserToken.id }).set({
          password: newPassword
        })
        return res.ok(true)
      } else {
        return res.ok(false)
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  dispatchRequestAccountEmail: async (req, res) => {
    try {
      let {
        email,
        userType,
        firstName,
        lastName,
        companyName,
        phoneNumber
      } = req.body
      email = toLower(email)

      const hasTasterAccount = await User.findOne({
        emailAddress: email,
        type: 'taster'
      })
      if (hasTasterAccount) {
        const superAdmin = await User.find({ isSuperAdmin: true })

        if (superAdmin.length) {
          await emailService.sendTasterRequestOperatorAccountEmail({
            adminEmail: superAdmin[0].emailAddress,
            emailAddress: email
          })
        }

        return res.badRequest('Taster Account Exists')
      }

      const user =
        hasTasterAccount ||
        (await User.findOne({ emailAddress: email, inactive: false }))
      if (user) {
        throw new ApiError('E_USER_IS_EXISTED')
      }

      await emailService.sendRequestAccountEmail({
        requesterEmail: email,
        userType,
        firstName,
        lastName,
        companyName,
        phoneNumber
      })
      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  forgotPassword: async (req, res) => {
    try {
      let { email } = req.body
      const emailAddress = toLower(email)

      const isValidEmail = await emailChecker('forgot-password', req)
      if (!isValidEmail) {
        return res.badRequest('email.invalid')
      }

      const existUser = await User.findOne({ emailAddress })
      if (existUser && existUser.inactive) {
        return res.badRequest('user.inactive')
      }

      if (!existUser) {
        return res.badRequest('That email is not registered.')
      }

      if (!isPermanentAccount(existUser.type)) {
        return res.badRequest('Not a valid account')
      }

      const salt = bcrypt.genSaltSync(10)
      const resetToken = bcrypt.hashSync(existUser.emailAddress, salt)

      const hours =
        process.env.APP_URL &&
        (process.env.APP_URL.includes('chr-hansen') ||
          process.env.APP_URL.includes('staging5'))
          ? 48
          : 24

      await User.updateOne({ emailAddress: email }).set({
        passwordResetToken: resetToken,
        passwordResetTokenExpiresAt: Date.now() + hours * 60 * 60 * 1000 // now + x hours in ms
      })

      const resetLink = `${
        process.env.APP_URL
      }/onboarding/reset-password?token=${resetToken}&email=${encodeURIComponent(
        email
      )}`

      await emailService.sendResetPasswordEmail({
        email,
        resetLink,
        language: existUser.language,
        hours
      })

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  isResetPasswordTokenValid: async (req, res) => {
    try {
      let { email, token } = req.body
      email = toLower(email)
      const user = await User.findOne({ emailAddress: email })

      if (!user) {
        return res.badRequest('That email is not registered.')
      }

      if (token !== user.passwordResetToken) {
        return res.forbidden('Not authorized')
      }

      return res.ok(Date.now() <= user.passwordResetTokenExpiresAt)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  resetPassword: async (req, res) => {
    try {
      let { email, password, token } = req.body
      email = toLower(email)
      const user = await User.findOne({ emailAddress: email })

      if (!user) {
        return res.badRequest('That email is not registered.')
      }

      if (!isPermanentAccount(user.type)) {
        return res.badRequest('Not a valid account')
      }

      if (token !== user.passwordResetToken) {
        return res.forbidden('Not authorized')
      }

      if (Date.now() >= user.passwordResetTokenExpiresAt) {
        return res.badRequest('expired')
      }
      await User.updateOne({ emailAddress: email }).set({
        password,
        passwordResetToken: '',
        passwordResetTokenExpiresAt: 0,
        isVerified: true,
        verificationToken: '',
        verificationTokenExpiresAt: 0,
        verifiedAt: Date.now()
      })
      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  isRegistered: async (req, res) => {
    try {
      let { email } = req.body
      email = toLower(email)
      const user = await User.findOne({ emailAddress: email })

      if (!user) {
        await User.create({ emailAddress: email })
        return res.badRequest()
      }

      if (!user.password || user.password === '') {
        return res.badRequest()
      }

      return res.ok()
    } catch (error) {
      return res.badRequest(error)
    }
  },
  verifyTaster: async (req, res) => {
    try {
      let { emailAddress, token } = req.body
      emailAddress = toLower(emailAddress)
      const user = await User.findOne({ emailAddress: emailAddress })
      if (!user) {
        return res.badRequest('notRegistered')
      }

      if (!isPermanentAccount(user.type)) {
        return res.badRequest('notValidAccount')
      }

      if (user.isVerified) {
        return res.badRequest('alreadyVerified')
      }

      if (
        token === user.verificationToken &&
        Date.now() < user.verificationTokenExpiresAt
      ) {
        await User.updateOne({ emailAddress: emailAddress }).set({
          isVerified: true,
          passwordResetTokenExpiresAt: 0,
          isTaster: true,
          verificationToken: '',
          verificationTokenExpiresAt: 0,
          verifiedAt: Date.now()
        })
        const token = jwToken.issue(
          Object.assign(
            {},
            _.pick(user, [
              'id',
              'emailAddress',
              'fullName',
              'organization',
              'type'
            ]),
            {
              isTaster: user.isSuperAdmin ? true : undefined,
              apiVersion: process.env.USER_API_VERSION
            }
          )
        )

        const resUser = await User.findOne({
          emailAddress: emailAddress
        }).populate('organization')
        const response = {
          user: { ...resUser },
          token
        }
        return res.ok(response)
      } else {
        return res.forbidden('Not authorized')
      }
    } catch (error) {
      return res.badRequest(error)
    }
  },
  superAdminUsersByOrganization: async (req, res) => {
    try {
      if (!req.token) {
        return res.unauthorized()
      }
      const {
        user: { organization: owner }
      } = req.token
      const users = await User.find({
        where: { organization: owner, isSuperAdmin: true }
      })
      return res.ok(users)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getUserValidations: async (req, res) => {
    try {
      const { userId } = req.params

      const surveyEnrollments = await SurveyEnrollment.find({
        user: userId,
        processed: true
      })

      const surveyEnrollementIds = surveyEnrollments.map(surveyEnrollment => {
        return surveyEnrollment.id.toString()
      })

      const answers = await Answer.find({
        enrollment: { in: surveyEnrollementIds }
      })

      const validSurveyEnrollments = []
      const validAnswers = []
      const invalidSurveyEnrollments = []
      const invalidAnswers = []

      surveyEnrollments.forEach(surveyEnrollment => {
        if (surveyEnrollment.validation === 'valid') {
          const uploadAnswers = answers.filter(answer => {
            return (
              answer.enrollment === surveyEnrollment.id &&
              answer.value &&
              answer.value[0] &&
              answer.value[0].toString().startsWith('https://s3')
            )
          })

          validSurveyEnrollments.push(surveyEnrollment)
          validAnswers.push(...uploadAnswers)
        }

        if (surveyEnrollment.validation === 'invalid') {
          const uploadAnswers = answers.filter(answer => {
            return (
              answer.enrollment === surveyEnrollment.id &&
              answer.value &&
              answer.value[0] &&
              answer.value[0].toString().startsWith('https://s3')
            )
          })

          invalidSurveyEnrollments.push(surveyEnrollment)
          invalidAnswers.push(...uploadAnswers)
        }
      })

      return res.ok({
        validCount: validSurveyEnrollments.length,
        validAnswers,
        invalidCount: invalidSurveyEnrollments.length,
        invalidAnswers
      })
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getPaypalUsers: async (req, res) => {
    try {
      const { paypalEmailAddress } = req.params

      const paypalUsers = await User.find({
        paypalEmailAddress: paypalEmailAddress
      }).populate('surveyEnrollments')

      return res.ok(paypalUsers)
    } catch (error) {
      return res.badRequest(error)
    }
  }
}
