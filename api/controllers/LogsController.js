/* global Logs */
/**
 * LogsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const requestUtils = require('../../utils/requestUtils')
const {
  logsSearch,
  createLogsSearchPipeline,
  createLogsSearchOrderBy
} = require('../services/searches')

module.exports = {
  getSurveyLogs: async (req, res) => {
    try {
      const { surveyId } = req.params
      const searchQuery = { ...requestUtils.parseSearchQuery(req), surveyId }
      const pipeline = createLogsSearchPipeline(searchQuery)
      const orderBy = createLogsSearchOrderBy(searchQuery)
      const { skip, limit } = searchQuery

      const logs = await logsSearch.search(pipeline, {
        skip,
        limit,
        orderBy: orderBy || { createdAt: -1 }
      })

      return res.ok(logs)
    } catch (error) {
      res.badRequest(error)
    }
  },
  getCountSurveyLogs: async (req, res) => {
    try {
      const { surveyId } = req.params
      const searchQuery = { ...requestUtils.parseSearchQuery(req), surveyId }
      const pipeline = createLogsSearchPipeline(searchQuery)
      const countSurveyLogs = await logsSearch.count(pipeline)

      return res.ok(countSurveyLogs)
    } catch (error) {
      return res.badRequest(error)
    }
  },
  getPhotoValidationLogs: async (req, res) => {
    try {
      const { surveyEnrollmentId } = req.params

      const logs = await Logs.find({
        where: {
          relatedEnrollment: surveyEnrollmentId,
          type: 'photo-validation'
        },
        sort: 'createdAt DESC'
      })
        .populate('by')
        .populate('impersonatedBy')

      return res.ok(logs)
    } catch (error) {
      res.badRequest(error)
    }
  }
}
