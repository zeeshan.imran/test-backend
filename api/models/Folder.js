/**
 * Folder.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true
    },

    owner: {
      model: 'Organization',
      required: true
    },

    // Add one side of a plural reflexive association
    parent: {
      model: 'folder'
    },

    // Add the other side of a plural reflexive association
    children: {
      collection: 'folder',
      via: 'parent'
    },

    surveys: {
      collection: 'Survey',
      via: 'folder'
    },

    status: {
      type: 'string',
      isIn: ['active', 'deleted'],
      defaultsTo: 'active'
    },

    type: {
      type: 'string',
      defaultsTo: 'folder'
    }

  }
}
