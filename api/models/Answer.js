/**
 * Answer.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    question: {
      model: 'Question',
      required: true
    },

    value: {
      type: 'json',
      required: true
    },

    pairQuestion: {
      model: 'PairQuestion'
    },

    startedAt: {
      type: 'string',
      required: true
    },

    timeToAnswer: {
      type: 'number',
      required: true
    },

    product: {
      model: 'Product'
    },

    enrollment: {
      model: 'SurveyEnrollment',
      required: true
    }
  }
}
