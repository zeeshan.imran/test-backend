/**
 * Logs.js
 * This is used to create history
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    rating: {
      type: 'number',
      required: true,
    },
    email: {
      type: 'string',
      required: true,
      isEmail: true,
      maxLength: 200
    },
    reason: {
      type: 'string',
      required: true
    }
  }
}
