module.exports = {
  attributes: {
    name: {
      type: 'string',
      required: true,
      unique: true
    },
    uniqueName: {
      type: 'string',
      required: true,
      unique: true
    },
    members: {
      collection: 'User',
      via: 'organization'
    },
    createdBy: {
      model: 'User'
    },
    modifiedBy: {
      model: 'User'
    },
    inactive: {
      type: 'boolean',
      defaultsTo: false
    }
  }
}
