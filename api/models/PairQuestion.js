/**
 * PairQuestion.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const imageCreation = require('../../utils/imageCreation')

const imageCreationHook = async (values, cb) => {
  const { leftAttribute, rightAttribute } = values
  if (leftAttribute && leftAttribute.image800) {
    leftAttribute.image800 = await imageCreation(leftAttribute.image800)
  }
  if (rightAttribute && rightAttribute.image800) {
    rightAttribute.image800 = await imageCreation(rightAttribute.image800)
  }
  cb()
}

module.exports = {
  attributes: {
    question: {
      model: 'Question'
    },

    leftAttribute: {
      type: 'json',
      required: true
    },

    rightAttribute: {
      type: 'json',
      required: true
    },

    leftAttributeCounter: {
      type: 'number',
      defaultsTo: 0
    },

    rightAttributeCounter: {
      type: 'number',
      defaultsTo: 0
    },

    answers: {
      collection: 'Answer',
      via: 'pairQuestion'
    }
  },
  beforeCreate: imageCreationHook,
  beforeUpdate: imageCreationHook
}
