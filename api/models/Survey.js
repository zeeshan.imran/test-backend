/**
 * Survey.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const defaultButtons = require('../../utils/defaultButtons')
const defaultInstructions = require('../../utils/defaultInstructions')

module.exports = {
  attributes: {
    openedAt: {
      type: 'number',
      defaultsTo: 0
    },

    showUserProfileDemographics: {
      type: 'boolean',
      defaultsTo: false
    },

    coverPhoto: {
      type: 'string'
    },

    name: {
      type: 'string',
      required: true
    },

    title: {
      type: 'string',
      required: true
    },

    owner: {
      model: 'Organization',
      required: true
    },

    creator: {
      model: 'User'
    },

    lastModifier: {
      model: 'User'
    },

    state: {
      type: 'string',
      isIn: ['deprecated', 'draft', 'active', 'suspended'],
      defaultsTo: 'draft'
    },

    minimumProducts: {
      type: 'number',
      defaultsTo: 1
    },

    maximumProducts: {
      type: 'number',
      defaultsTo: 1
    },

    surveyLanguage: {
      type: 'string',
      defaultsTo: 'en'
    },

    customButtons: {
      type: 'json',
      defaultsTo: defaultButtons
    },

    instructionsText: {
      type: 'json'
    },

    instructionSteps: {
      type: 'json',
      columnType: 'array',
      defaultsTo: defaultInstructions
    },

    thankYouText: {
      type: 'json'
    },

    rejectionText: {
      type: 'json'
    },

    screeningText: {
      type: 'json'
    },

    customizeSharingMessage: {
      type: 'json'
    },

    loginText: {
      type: 'json'
    },

    pauseText: {
      type: 'json'
    },

    questions: {
      collection: 'Question',
      via: 'survey'
    },

    products: {
      collection: 'Product',
      via: 'surveys'
    },

    enrollments: {
      collection: 'SurveyEnrollment',
      via: 'survey'
    },

    uniqueName: {
      type: 'string',
      unique: true
    },

    authorizationType: {
      type: 'string',
      isIn: ['enrollment', 'public', 'selected'],
      defaultsTo: 'public'
    },

    maximumReward: {
      type: 'number',
      defaultsTo: 0
    },

    exclusiveTasters: {
      collection: 'User',
      via: 'exclusiveSurveys'
    },

    sharedStatsUsers: {
      collection: 'User',
      via: 'sharedStatsSurveys'
    },

    isScreenerShownInShare: {
      type: 'boolean',
      defaultsTo: false
    },

    isCompulsorySurveyShownInShare: {
      type: 'boolean',
      defaultsTo: false
    },

    allowRetakes: {
      type: 'boolean',
      defaultsTo: false
    },

    isScreenerOnly: {
      type: 'boolean',
      defaultsTo: false
    },

    showSurveyProductScreen: {
      type: 'boolean',
      defaultsTo: false
    },

    showIncentives: {
      type: 'boolean',
      defaultsTo: true
    },

    excludeSkipPairs: {
      type: 'boolean',
      defaultsTo: false
    },

    showGeneratePdf: {
      type: 'boolean',
      defaultsTo: false
    },

    forcedAccount: {
      type: 'boolean',
      defaultsTo: false
    },

    forcedAccountLocation: {
      type: 'string',
      isIn: ['start', 'end'],
      defaultsTo: 'start'
    },

    linkedSurveys: {
      collection: 'Survey'
    },

    referralAmount: {
      type: 'number'
    },

    isGiftCardSelected: {
      type: 'boolean',
      defaultsTo: false
    },

    isPaypalSelected: {
      type: 'boolean',
      defaultsTo: false
    },

    sharingButtons: {
      type: 'boolean',
      defaultsTo: false
    },

    showSharingLink: {
      type: 'boolean',
      defaultsTo: true
    },

    validatedData: {
      type: 'boolean',
      defaultsTo: false
    },

    compulsorySurvey: {
      type: 'boolean',
      defaultsTo: false
    },

    showSurveyScore: {
      type: 'boolean',
      defaultsTo: false
    },

    showOnTasterDashboard: {
      type: 'boolean',
      defaultsTo: false
    },

    showInternalNameInReports: {
      type: 'boolean',
      defaultsTo: false
    },

    reduceRewardInTasting: {
      type: 'boolean',
      defaultsTo: false
    },

    includeCompulsorySurveyDataInStats: {
      type: 'boolean',
      defaultsTo: false
    },

    showInPreferedLanguage: {
      type: 'boolean',
      defaultsTo: false
    },

    addDelayToSelectNextProductAndNextQuestion: {
      type: 'boolean',
      defaultsTo: false
    },

    autoAdvanceSettings: {
      type: 'json',
      defaultsTo: {
        active: false,
        debounce: 0, // in seconds
        hideNextButton: false
      }
    },

    pdfFooterSettings: {
      type: 'json',
      defaultsTo: {
        active: false,
        footerNote: ''
      }
    },

    promotionSettings: {
      type: 'json',
      defaultsTo: {
        active: false,
        showAfterTaster: false,
        code: ''
      }
    },
    promotionOptions: {
      type: 'json',
      defaultsTo: {
        promoProducts: []
      }
    },

    referralReward: {
      type: 'number'
    },

    referralRewardCurrency: {
      type: 'string'
    },

    settings: {
      type: 'json',
      defaultsTo: {}
    },

    enabledEmailTypes: {
      type: 'json',
      columnType: 'array',
      defaultsTo: []
    },

    country: {
      type: 'string',
      defaultsTo: ''
    },

    maxProductStatCount: {
      type: 'number',
      defaultsTo: 6
    },

    allowedDaysToFillTheTasting: {
      type: 'number',
      defaultsTo: 5
    },

    tastingNotes: {
      type: 'json',
      defaultsTo: {
        tastingId: null,
        tastingLeader: null,
        customer: null,
        country: null,
        dateOfTasting: null,
        otherInfo: null
      }
    },

    folder: {
      model: 'folder'
    },

    emails: {
      type: 'json',
      defaultsTo: {
        surveyWaiting: {
          subject: null,
          html: null,
          text: null
        },
        surveyCompleted: {
          subject: null,
          html: null,
          text: null
        },
        surveyRejected: {
          subject: null,
          html: null,
          text: null
        }
      }
    },

    productDisplayType: {
      type: 'string',
      isIn: ['reverse', 'permutation', 'forced', 'none'],
      defaultsTo: 'none'
    },

    productRewardsRule: {
      type: 'json',
      defaultsTo: {
        active: false
      }
    },
    screenOut: {
      type: 'boolean',
      defaultsTo: false
    },
    screenOutSettings: {
      type: 'json',
      defaultsTo: {
        reject: false,
        rejectAfterStep: 1
      }
    },
    dualCurrency: {
      type: 'boolean',
      defaultsTo: false
    },
    referralAmountInDollar: {
      type: 'number'
    },
    retakeAfter: {
      type: 'number',
      defaultsTo: 0
    },
    maxRetake: {
      type: 'number',
      defaultsTo: 0
    },

    publishedAt: {
      type: 'string'
    }
  },
  beforeCreate: async (values, cb) => {
    const hasEmailService = values.settings.hasOwnProperty('emailService')
    if (!hasEmailService) {
      values.settings['emailService'] = false
    }
    const hasRecaptchaSetting = values.settings.hasOwnProperty('recaptcha')
    if (!hasRecaptchaSetting) {
      values.settings['recaptcha'] = true
    }
    values.openedAt = new Date().valueOf()
    cb()
  },
  beforeUpdate: async (values, cb) => {
    if (values && values.enabledEmailTypes && values.enabledEmailTypes.length) {
      values.settings['emailService'] = true
    }

    cb()
  }
}
