/**
 * SurveyEnrollment.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    user: {
      model: 'User'
    },

    survey: {
      model: 'Survey',
      required: true
    },

    state: {
      type: 'string',
      isIn: [
        'waiting-for-screening',
        'waiting-for-product',
        'finished',
        'paid',
        'rejected'
      ],
      defaultsTo: 'waiting-for-screening'
    },

    selectedProducts: {
      collection: 'Product',
      via: 'surveyEnrollments'
    },

    answers: {
      collection: 'Answer',
      via: 'enrollment'
    },

    lastAnsweredQuestion: {
      model: 'Question'
    },

    paypalEmail: {
      type: 'string',
      defaultsTo: ''
    },

    finishedAt: {
      type: 'string'
    },

    referral: {
      type: 'string'
    },

    extsource: {
      type: 'string'
    },

    extid: {
      type: 'string'
    },

    enrollmentReferralAmount: {
      type: 'number'
    },

    savedRewards: {
      type: 'JSON',
      defaultsTo: []
    },

    savedQuestions: {
      type: 'JSON',
      defaultsTo: []
    },

    productRewardsRule: {
      type: 'JSON',
      defaultsTo: {
        active: false
      }
    },

    processed: {
      type: 'boolean',
      defaultsTo: false
    },

    validation: {
      type: 'string',
      required: false
    },

    comment: {
      type: 'string',
      required: false
    },

    escalation: {
      type: 'boolean',
      defaultsTo: false
    },

    validationLogs: {
      collection: 'Logs',
      via: 'relatedEnrollment'
    },

    shareExported: {
      type: 'boolean',
      required: false
    },

    productIncentiveExported: {
      type: 'boolean',
      required: false
    },

    giftCardIncentiveExported: {
      type: 'boolean',
      required: false,
      defaultsTo: false
    },

    browserInfo: {
      type: 'JSON',
      defaultsTo: []
    },

    lastSelectedProduct: {
      type: 'string',
      defaultsTo: ''
    },

    productDisplay: {
      model: 'ProductDisplay'
    },

    productDisplayOrder: {
      type: 'json',
      columnType: 'array',
      defaultsTo: []
    },

    productDisplayType: {
      type: 'string',
      defaultsTo: 'none'
    },

    allowedDaysToFillTheTasting: {
      type: 'number',
      defaultsTo: 5
    },

    refferedUsers: {
      collection: 'user',
      via: 'referredByEnrollment'
    },

    socioEconomicRating: {
      type: 'json'
    },
    rejectedQuestion: {
      type: 'JSON',
      columnType: 'array',
      defaultsTo: []
    },
    rejectedEnrollment: {
      type: 'boolean',
      required: false,
      defaultsTo: false
    },
    paidIncentivesOn: {
      type: 'number',
      description:
        'A JS timestamp (epoch ms) representing the moment when this user was `paid` for product incentive.',
      example: 1502844074211
    },

    paidIncentivesBy: {
      model: 'User'
    },

    paidShareOn: {
      type: 'number',
      description:
        'A JS timestamp (epoch ms) representing the moment when this user was `paid` for share incentive.',
      example: 1502844074211
    },

    paidShareBy: {
      model: 'User'
    },

    enrollmentReferralAmountInDollar: {
      type: 'number'
    },

    hiddenFromCharts: {
      type: 'boolean',
      defaultsTo: false
    }
  }
}
