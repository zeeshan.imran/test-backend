/**
 * CountrySurveyGrading.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    country: {
      type: 'string',
      unique: true,
      required: true
    },

    createdBy: {
      model: 'User',
      required: true
    },

    updatedBy: {
      model: 'User',
      required: false
    },

    owner: {
      model: 'Organization',
      required: true
    },

    // grades: contains grades and with points range
    // for example: [{grade: A, scoreRange: {start: 85, end: 100}}]
    grades: {
      type: 'json'
    }
  }
}
