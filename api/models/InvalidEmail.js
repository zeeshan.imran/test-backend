/**
 * InvalidEmail.js
 * This is used to create history
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'invalid_email',
  attributes: {
    type: {
      type: 'string',
      required: true
    },

    email: {
      type: 'string',
      required: true
    },

    ipAddress: {
      type: 'string',
      required: true
    },

    emailCheckerData: {
      type: 'json',
      required: true
    }
  }
}
