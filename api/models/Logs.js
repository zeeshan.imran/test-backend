/**
 * Logs.js
 * This is used to create history
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    action: {
      type: 'string'
    },

    type: {
      type: 'string'
    },

    values: {
      type: 'json',
      required: false
    },

    collections: {
      type: 'json',
      required: false
    },

    by: {
      model: 'User'
    },

    impersonatedBy: {
      model: 'User',
      required: false
    },

    relatedSurvey: {
      model: 'Survey',
      required: false
    },

    relatedEnrollment: {
      model: 'SurveyEnrollment',
      required: false
    },

    relatedQuestion: {
      model: 'Question',
      required: false
    },

    relatedProduct: {
      model: 'Product',
      required: false
    },

    relatedUser: {
      model: 'User',
      required: false
    },

    relatedOrganization: {
      model: 'Organization',
      required: false
    },

    relatedAnswer: {
      model: 'Answer',
      required: false
    }
  }
}
