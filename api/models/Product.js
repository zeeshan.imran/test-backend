/**
 * Product.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    photo: {
      type: 'string'
    },

    brand: {
      model: 'Brand'
    },

    name: {
      type: 'string',
      required: true
    },

    price: {
      type: 'number'
    },

    currency: {
      type: 'string'
    },

    isInternal: {
      type: 'boolean',
      defaultsTo: false
    },

    isAvailable: {
      type: 'boolean',
      defaultsTo: false
    },

    size: {
      model: 'Size'
    },

    barcode: {
      type: 'string'
    },

    surveyEnrollments: {
      collection: 'SurveyEnrollment',
      via: 'selectedProducts'
    },

    surveys: {
      collection: 'Survey',
      via: 'products'
    },

    reward: {
      type: 'number'
    },
    rejectedReward: {
      type: 'number'
    },
    delayToNextProduct: {
      type: 'string',
      defaultsTo: '',
      allowNull: true
    },
    extraDelayToNextProduct: {
      type: 'string',
      defaultsTo: '',
      allowNull: true
    },
    isSurveyCover: {
      type: 'boolean'
    },

    sortingOrderId: {
      type: 'number'
    },
    amountInDollar: {
      type: 'number'
    },
    internalName: {
      type: 'string',
      defaultsTo: ''
    }
  }
}
