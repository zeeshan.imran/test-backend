/**
 * Test environment settings
 **/
Object.assign(process.env, {
  APP_URL: 'http://test.flavorwiki.com',
  BUCKET_NAME: 'flavorwiki-storage',
  BUCKET_URL: 'https://test.amazonaws.com/flavorwiki-storage',
  AWS_ACCESS_KEY_ID: 'aws-id',
  AWS_SECRET_ACCESS_KEY: 'aws-key'
})

module.exports = {
  datastores: {
    default: {
      adapter: 'sails-mongo',
      url: `${global.__MONGO_URI__}`
    }
  }
}
