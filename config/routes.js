/* globals sails */
/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

const fs = require('fs')
const path = require('path')
const uploadService = require('../api/services/uploadService')

module.exports.routes = {
  'GET /': (req, res) => res.send({ health: 'ok' }),

  'GET /user/me': {
    controller: 'UserController',
    action: 'me'
  },

  'GET /user/:id': {
    controller: 'UserController',
    action: 'tasterDashboard'
  },

  'GET /users': {
    controller: 'UserController',
    action: 'users'
  },

  'GET /user/count': {
    controller: 'UserController',
    action: 'countUsers'
  },

  'POST /user/add': {
    controller: 'UserController',
    action: 'addUser'
  },

  'POST /user/edit/:id': {
    controller: 'UserController',
    action: 'editUser'
  },

  'PATCH /user/:id': {
    controller: 'UserController',
    action: 'updateUser'
  },

  'POST /update-user-password': {
    controller: 'UserController',
    action: 'updateUserPassword'
  },

  'GET /organizations': {
    controller: 'OrganizationController',
    action: 'organizations'
  },

  'GET /organizationsNumber': {
    controller: 'OrganizationController',
    action: 'countOrganizations'
  },

  'POST /organization/add': {
    controller: 'OrganizationController',
    action: 'addOrganization'
  },

  'POST /organization/edit/:id': {
    controller: 'OrganizationController',
    action: 'editOrganization'
  },

  'DELETE /organization/:id': {
    controller: 'OrganizationController',
    action: 'deleteOrganization'
  },

  'PATCH /organization/:id/unarchived': {
    controller: 'OrganizationController',
    action: 'unarchivedOrganization'
  },

  'POST /organization/:id/users': {
    controller: 'OrganizationController',
    action: 'getUsers'
  },

  'GET /organization': {
    controller: 'OrganizationController',
    action: 'organization'
  },

  'DELETE /user/:id': {
    controller: 'UserController',
    action: 'deleteUser'
  },

  'PATCH /user/:id/unarchived': {
    controller: 'UserController',
    action: 'unarchivedUser'
  },

  'POST /user/blackList': {
    controller: 'UserController',
    action: 'blackListUser'
  },

  'POST /email': {
    controller: 'SurveyController',
    action: 'dispatchEmail'
  },

  'POST /user/createtasteraccount': {
    controller: 'UserController',
    action: 'createTasterAccount'
  },

  'POST /user/tasterResendVerificationEmail': {
    controller: 'UserController',
    action: 'tasterResendVerificationEmail'
  },

  'POST /user/updatetasteraccount': {
    controller: 'UserController',
    action: 'updateTasterAccount'
  },

  'POST /user/requestaccount': {
    controller: 'UserController',
    action: 'dispatchRequestAccountEmail'
  },

  'GET /compulsory-survey-country': {
    controller: 'SurveyController',
    action: 'getCompulsorySurveyByCountry'
  },

  'GET /survey/getsurveysbyowner': {
    controller: 'SurveyController',
    action: 'getSurveysByOwner'
  },

  'GET /survey/getquickaccesssurveys': {
    controller: 'SurveyController',
    action: 'getQuickAccessSurveysByOwner'
  },

  'POST /survey/getCoversOfSurveys': {
    controller: 'SurveyController',
    action: 'getCoversOfSurveys'
  },

  'POST /survey/isDuplicate': {
    controller: 'SurveyController',
    action: 'isUniqueNameDuplicated'
  },

  'POST /survey/share/stats': {
    controller: 'SurveyController',
    action: 'shareSurveyStats'
  },

  'GET /survey/count': {
    controller: 'SurveyController',
    action: 'countSurveys'
  },

  'GET /share/:uniqueName': {
    controller: 'SurveyController',
    action: 'getSurveyShareContent'
  },

  'POST /survey-pdf/operator/:surveyId/:jobGroupId': {
    controller: 'SurveyController',
    action: 'getSurveyPdf'
  },
  'POST /surveyStatsPpt/:surveyId': {
    controller: 'SurveyController',
    action: 'getSurveyPpt'
  },
  'POST /survey-pdf/taster/:surveyId/:jobGroupId': {
    controller: 'SurveyController',
    action: 'getTasterPdf'
  },

  'POST /survey': {
    controller: 'SurveyController',
    action: 'createSurvey'
  },

  'GET /pdf/:id': {
    controller: 'SurveyController',
    action: 'downloadPdf'
  },
  'GET /ppt/:id': {
    controller: 'SurveyController',
    action: 'downloadPpt'
  },

  'PATCH /survey/:id': {
    controller: 'SurveyController',
    action: 'updateSurvey'
  },

  'POST /system/resetShareLinks': {
    controller: 'SystemController',
    action: 'resetShareLinks'
  },

  'POST /user/logintosurvey': {
    controller: 'UserController',
    action: 'loginToSurvey'
  },

  'POST /user/logintosurveywithauth': {
    controller: 'UserController',
    action: 'loginToSurveyWithAuth'
  },

  'POST /user/forgotpassword': {
    controller: 'UserController',
    action: 'forgotPassword'
  },

  'POST /user/isregistered': {
    controller: 'UserController',
    action: 'isRegistered'
  },

  'POST /user/isResetPasswordTokenValid': {
    controller: 'UserController',
    action: 'isResetPasswordTokenValid'
  },

  'POST /user/resetpassword': {
    controller: 'UserController',
    action: 'resetPassword'
  },

  'POST /user/verifyTaster': {
    controller: 'UserController',
    action: 'verifyTaster'
  },

  'POST /getsignedurl': uploadService.getSignedUrl,

  'GET /qrCode/getTargetLink': {
    controller: 'QrCodeController',
    action: 'getTargetLink'
  },

  'GET /qrCode/:id/download': {
    controller: 'QrCodeController',
    action: 'download'
  },

  'POST /surveyenrollment/submitanswer': {
    controller: 'SurveyEnrollmentController',
    action: 'submitAnswer'
  },

  'POST /surveyenrollement/signuptosurvey': {
    controller: 'SurveyEnrollmentController',
    action: 'submitForcedSignUp'
  },

  'POST /surveyenrollment/saveQuestions': {
    controller: 'SurveyEnrollmentController',
    action: 'saveQuestions'
  },

  'POST /surveyenrollment/saveRewards': {
    controller: 'SurveyEnrollmentController',
    action: 'saveRewards'
  },

  'POST /survey/clone/:id': {
    controller: 'SurveyController',
    action: 'cloneSurvey'
  },

  'POST /survey/deprecate/:id': {
    controller: 'SurveyController',
    action: 'deprecateSurvey'
  },

  'POST /surveyenrollment/selectedproducts': {
    controller: 'SurveyEnrollmentController',
    action: 'setSurveySelectedProducts'
  },

  'POST /surveyenrollment/paypalemail': {
    controller: 'SurveyEnrollmentController',
    action: 'setPaypalEmail'
  },

  'POST /surveyenrollment/state': {
    controller: 'SurveyEnrollmentController',
    action: 'setState'
  },

  'POST /surveyenrollment/finishsurvey': {
    controller: 'SurveyEnrollmentController',
    action: 'finishSurvey'
  },

  'POST /surveyenrollment/rejectSurvey': {
    controller: 'SurveyEnrollmentController',
    action: 'rejectSurvey'
  },

  'POST /surveyenrollment/finishsurveyscreening': {
    controller: 'SurveyEnrollmentController',
    action: 'finishSurveyScreening'
  },

  'GET /loaderio-ac0a440dc991cf359fc392b6ccd38962': (req, res) => {
    const readStream = fs.createReadStream(
      path.resolve(
        sails.config.appPath,
        'loaderio-ac0a440dc991cf359fc392b6ccd38962.txt'
      )
    )
    readStream.pipe(res)
  },

  'GET /demosurvey/:demoName': {
    controller: 'SurveyController',
    action: 'getDemoSurvey'
  },

  'GET /survey/:identifier': {
    controller: 'SurveyController',
    action: 'getSurvey'
  },

  'GET /survey/download/shares/:id': {
    controller: 'SurveyController',
    action: 'getSurveyShareStats'
  },

  'GET /survey/download/gift-card-shares/:id': {
    controller: 'SurveyController',
    action: 'getGiftCardShares'
  },

  'GET /survey/download/product-incentives/:id': {
    controller: 'SurveyController',
    action: 'getProductIncentiveStats'
  },

  'GET /survey/download/gift-card-incentives/:id': {
    controller: 'SurveyController',
    action: 'getGiftCardIncentives'
  },

  'GET /surveybyorganization/:identifier': {
    controller: 'SurveyController',
    action: 'getSurveyByOrganization'
  },

  'POST /openedSurvey/:identifier': {
    controller: 'SurveyController',
    action: 'openedSurvey'
  },
  'POST /login': {
    controller: 'UserController',
    action: 'loginUser'
  },

  'POST /question/findids': {
    controller: 'QuestionController',
    action: 'findIds'
  },

  'DELETE /question/:questionId': {
    controller: 'QuestionController',
    action: 'delete'
  },

  'POST /question/replaceQuestions': {
    controller: 'QuestionController',
    action: 'replaceQuestions'
  },

  'POST /product/replaceProducts': {
    controller: 'ProductController',
    action: 'replaceProducts'
  },

  'POST /pairquestion/generatepairs': {
    controller: 'PairQuestion',
    action: 'generatePairs'
  },

  'POST /pairquestion/updatepairs': {
    controller: 'PairQuestion',
    action: 'updatePairs'
  },

  'GET /qrCode/count': {
    controller: 'QrCodeController',
    action: 'count'
  },

  'GET /surveyenrollement/funnel': {
    controller: 'SurveyEnrollmentController',
    action: 'getSurveyFunnel'
  },

  'POST /surveyenrollement/sendSurveyStats': {
    controller: 'SurveyEnrollmentController',
    action: 'sendSurveyStats'
  },

  'GET /survey/:surveyId/:isShared/screeners': {
    controller: 'SurveyController',
    action: 'getScreenerSurveys'
  },

  'GET /survey/:surveyId/:isShared/compulsory': {
    controller: 'SurveyController',
    action: 'getCompulsorySurveys'
  },

  'GET /surveyAnswers/:enrollmentId': {
    controller: 'AnswerController',
    action: 'getSurveyAnswers'
  },
  'POST /surveyenrollement/validation/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyEnrollementValidation'
  },

  'POST /surveyenrollement/product-incentive-payment/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyEnrollementProductIncentives'
  },

  'POST /surveyenrollement/gift-card-incentive-payment/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyEnrollementGiftCardIncentives'
  },

  'POST /surveyenrollement/survey-shares-payment/:id': {
    controller: 'SurveyEnrollmentController',
    action: 'updateSurveyShareStatus'
  },

  'GET /surveyenrollement/user/:userId': {
    controller: 'SurveyEnrollmentController',
    action: 'userEnrollements'
  },

  'PATCH /surveyenrollement/:id/expiry': {
    controller: 'SurveyEnrollmentController',
    action: 'updateUserEnrollementExpiry'
  },

  'GET /qrCodeByOrganization/:identifier': {
    controller: 'QrCodeController',
    action: 'qrCodeByOrganization'
  },

  'POST /image/add': {
    controller: 'ImageController',
    action: 'addImage'
  },

  'GET /settings/charts/:surveyId': {
    controller: 'SettingsController',
    action: 'getChartsSettings'
  },

  'POST /settings/charts': {
    controller: 'SettingsController',
    action: 'saveChartsSettings'
  },

  'GET /folders': {
    controller: 'FoldersController',
    action: 'listFolders'
  },

  'GET /folder/survey/listings': {
    controller: 'FoldersController',
    action: 'showFolder'
  },

  'GET /folders/:id/path': {
    controller: 'FoldersController',
    action: 'getFolderPath'
  },

  'PUT /folders/:id': {
    controller: 'FoldersController',
    action: 'updateFolder'
  },

  'POST /folders': {
    controller: 'FoldersController',
    action: 'createFolder'
  },

  'DELETE /folders/:id': {
    controller: 'FoldersController',
    action: 'destroyFolder'
  },

  'PUT /folders/moveToFolder/:id': {
    controller: 'FoldersController',
    action: 'moveSurveyToFolder'
  },

  'POST /country/survey/grading': {
    controller: 'CountrySurveyGradingController',
    action: 'addCountrySurveyGrading'
  },

  'PATCH /country/survey/grading/:id': {
    controller: 'CountrySurveyGradingController',
    action: 'updateCountrySurveyGrading'
  },

  'GET /country/survey/grading': {
    controller: 'CountrySurveyGradingController',
    action: 'getCountrySurveyGrading'
  },

  'GET /surveyenrollement/funnelValidation': {
    controller: 'SurveyEnrollmentController',
    action: 'getFunnelValidation'
  },

  'GET /survey/:surveyId/logs': {
    controller: 'LogsController',
    action: 'getSurveyLogs'
  },

  'GET /survey/:surveyId/countLogs': {
    controller: 'LogsController',
    action: 'getCountSurveyLogs'
  },
  'GET /getQuestionWithOrder/:surveyId': {
    controller: 'SurveyController',
    action: 'getQuestionWithOrder'
  },

  'GET /surveyenrollment/:surveyEnrollmentId/logs': {
    controller: 'LogsController',
    action: 'getPhotoValidationLogs'
  },

  'GET /users/operator': {
    controller: 'UserController',
    action: 'superAdminUsersByOrganization'
  },

  'GET /survey/:surveyId/survey-enrollment-validations': {
    controller: 'SurveyEnrollmentController',
    action: 'getSurveyEnrollmentValidations'
  },

  'GET /survey/:surveyId/count-survey-enrollment-validations': {
    controller: 'SurveyEnrollmentController',
    action: 'countSurveyEnrollmentValidations'
  },

  'GET /user/:userId/validations': {
    controller: 'UserController',
    action: 'getUserValidations'
  },

  'GET /paypal/:paypalEmailAddress/users': {
    controller: 'UserController',
    action: 'getPaypalUsers'
  },

  'POST /feedback': {
    controller: 'FeedbackController',
    action: 'addFeedback'
  }
}
