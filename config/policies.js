/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  '*': false,

  ProductController: {
    find: true,
    findOne: true,
    replaceProducts: ['authToken', 'isOperatorOrPowerUser'],
    create: ['authToken', 'isOperatorOrPowerUser'],
    update: ['authToken', 'isOperatorOrPowerUser']
  },

  BrandController: {
    find: true,
    findOne: true
  },

  OrganizationController: {
    find: ['authToken', 'isOperatorOrPowerUser'],
    organization: ['authToken'],
    findOne: ['authToken', 'isOperatorOrPowerUser'],
    organizations: ['authToken', 'isOperatorOrPowerUser'],
    countOrganizations: ['authToken', 'isSuperAdmin'],
    addOrganization: ['authToken', 'isSuperAdmin'],
    editOrganization: ['authToken', 'isSuperAdmin'],
    deleteOrganization: ['authToken', 'isSuperAdmin'],
    unarchivedOrganization: ['authToken', 'isSuperAdmin'],
    getUsers: ['authToken', 'isOperatorOrPowerUser']
  },

  SurveyController: {
    find: true,
    update: ['authToken', 'isOperatorOrPowerUser'],
    openedSurvey: true,
    getScreenerSurveys: true,
    getCompulsorySurveys: true,
    getCompulsorySurveyByCountry: ['authToken'],
    getCoversOfSurveys: true,
    createSurvey: ['authToken', 'isOperatorOrPowerUser'],
    updateSurvey: ['authToken', 'isOperatorOrPowerUser'],
    findOne: true,
    dispatchEmail: true,
    getDemoSurvey: true,
    getSurvey: true,
    getSurveyByOrganization: ['authToken'],
    cloneSurvey: ['authToken', 'isOperatorOrPowerUser'],
    deprecateSurvey: ['authToken', 'isOperatorOrPowerUser'],
    getSurveysByOwner: ['authToken', 'isOperatorOrAnalyticsOrPowerUser'],
    getQuickAccessSurveysByOwner: ['authToken', 'isOperatorOrAnalyticsOrPowerUser'],
    countSurveys: ['authToken', 'isOperatorOrAnalyticsOrPowerUser'],
    isUniqueNameDuplicated: ['authToken', 'isOperatorOrPowerUser'],
    getSurveyShareStats: ['authToken'],
    getGiftCardShares: ['authToken'],
    getProductIncentiveStats: ['authToken'],
    getGiftCardIncentives: ['authToken'],
    shareSurveyStats: ['authToken', 'isOperatorOrPowerUser'],
    getSurveyPdf: ['authToken'],
    getSurveyPpt: ['authToken'],
    getTasterPdf: true,
    downloadPdf: true,
    downloadPpt: true,
    getSurveyShareContent: true,
    getQuestionWithOrder: true
  },

  SystemController: {
    resetShareLinks: ['authToken', 'isSuperAdmin']
  },

  QuestionController: {
    findOne: true,
    replaceQuestions: ['authToken', 'isOperatorOrPowerUser'],
    create: ['authToken', 'isOperatorOrPowerUser'],
    findIds: ['authToken', 'isOperatorOrPowerUser'],
    update: ['authToken', 'isOperatorOrPowerUser'],
    delete: ['authToken', 'isOperatorOrPowerUser']
  },

  PairQuestionController: {
    create: ['authToken', 'isOperatorOrPowerUser'],
    generatePairs: ['authToken', 'isOperatorOrPowerUser'],
    updatePairs: ['authToken', 'isOperatorOrPowerUser']
  },

  UserController: {
    loginToSurvey: true,
    loginToSurveyWithAuth: ['authToken'],
    loginUser: true,
    find: ['authToken'],
    findOne: ['authToken'],
    update: ['authToken', 'isLoggedIn'],
    updateUserPassword: ['authToken', 'isLoggedIn'],
    dispatchRequestAccountEmail: true,
    forgotPassword: true,
    isResetPasswordTokenValid: true,
    resetPassword: true,
    me: ['authToken'],
    tasterDashboard: ['authToken'],
    updateUser: ['authToken'],
    users: ['authToken', 'isSuperAdminOrPowerUserOrOperator'],
    addUser: ['authToken', 'isSuperAdminOrPowerUser'],
    editUser: ['authToken', 'isSuperAdminOrPowerUser'],
    deleteUser: ['authToken', 'isSuperAdminOrPowerUser'],
    unarchivedUser: ['authToken', 'isSuperAdminOrPowerUser'],
    countUsers: ['authToken', 'isSuperAdminOrPowerUser'],
    blackListUser: ['authToken', 'isSuperAdminOrPowerUser'],

    createTasterAccount: true,
    tasterResendVerificationEmail: true,
    updateTasterAccount: ['authToken'],
    isRegistered: true,
    verifyTaster: true,
    superAdminUsersByOrganization: ['authToken', 'isOperatorOrPowerUser'],
    getUserValidations: ['authToken', 'isOperatorOrPowerUser'],
    getPaypalUsers: ['authToken', 'isOperatorOrPowerUser']
  },

  SurveyEnrollmentController: {
    saveRewards: true,
    saveQuestions: true,
    setSurveySelectedProducts: true,
    find: true,
    findOne: true,
    setPaypalEmail: true,
    submitAnswer: true,
    submitForcedSignUp: true,
    setState: true,
    finishSurvey: true,
    finishSurveyScreening: true,
    getSurveyEnrollmentValidations: ['authToken', 'isOperatorOrPowerUser'],
    countSurveyEnrollmentValidations: ['authToken', 'isOperatorOrPowerUser'],
    updateSurveyEnrollementValidation: ['authToken', 'isOperatorOrPowerUser'],
    updateSurveyEnrollementProductIncentives: [
      'authToken',
      'isOperatorOrPowerUser'
    ],
    updateSurveyShareStatus: ['authToken', 'isOperatorOrPowerUser'],
    rejectSurvey: true,
    getSurveyFunnel: ['authToken', 'isOperatorOrPowerUser'],
    getFunnelValidation: ['authToken', 'isOperatorOrPowerUser'],
    sendSurveyStats: ['authToken', 'isOperatorOrPowerUser'],
    updateSurveyEnrollementGiftCardIncentives: [
      'authToken',
      'isOperatorOrPowerUser'
    ],
    userEnrollements: ['authToken'],
    updateUserEnrollementExpiry: ['authToken']
  },

  QrCodeController: {
    findOne: ['authToken', 'isOperatorOrPowerUser'],
    download: ['authToken', 'isOperatorOrPowerUser', 'isBelongToOrganization'],
    find: ['authToken', 'isOperatorOrPowerUser', 'isBelongToOrganization'],
    count: ['authToken', 'isOperatorOrPowerUser', 'isBelongToOrganization'],
    create: ['authToken', 'isOperatorOrPowerUser', 'isBelongToOrganization'],
    update: ['authToken', 'isOperatorOrPowerUser', 'isBelongToOrganization'],
    destroy: ['authToken', 'isOperatorOrPowerUser', 'isBelongToOrganization'],
    qrCodeByOrganization: ['authToken'],
    getTargetLink: true
  },

  ImageController: {
    addImage: ['authToken', 'isOperatorOrPowerUser']
  },

  LogsController: {
    getSurveyLogs: ['authToken', 'isSuperAdmin'],
    getCountSurveyLogs: ['authToken', 'isSuperAdmin'],
    getPhotoValidationLogs: ['authToken', 'isOperatorOrPowerUser']
  },

  SettingsController: {
    saveChartsSettings: ['authToken'],
    getChartsSettings: true
  },

  AnswerController: {
    getSurveyAnswers: true
  },

  FoldersController: {
    listFolders: ['authToken'],
    createFolder: ['authToken'],
    showFolder: ['authToken'],
    getFolderPath: ['authToken'],
    updateFolder: ['authToken'],
    destroyFolder: ['authToken'],
    moveSurveyToFolder: ['authToken']
  },

  CountrySurveyGradingController: {
    addCountrySurveyGrading: ['authToken'],
    updateCountrySurveyGrading: ['authToken'],
    getCountrySurveyGrading: ['authToken']
  },

  FeedbackController: {
    addFeedback: true
  }
}
