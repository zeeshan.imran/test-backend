/* globals sails */
/* eslint-disable */
const defaultButtons = require('../../utils/defaultButtons')
const defaultInstructions = require('../../utils/defaultInstructions')

const {
  createSet,
  generatePairs,
  generateQuestionSection,
  generateProfileRelatedQuestions,
} = require('../generators')
/* eslint-enable */

const flavorPairs = generatePairs([
  'Caramelic',
  'Vanilla',
  'Roasted',
  'Creamy',
  'Bitter',
  'Sweet'
])

const texturePairs = generatePairs([
  'Crunchy',
  'Moist',
  'Grainy',
  'Sticky',
  'Chewy'
])

const instructionsText = `
  When you are ready please click on "Start Survey" to start the survey. Enjoy the tasting!
`

const screeningQuestions = []

const productQuestions = generateQuestionSection(
  [
    {
      typeOfQuestion: 'choose-product',
      prompt: 'Which of these products did you buy?',
      secondaryPrompt: "Or just choose the one you'll be tasting"
    },
    {
      typeOfQuestion: 'vertical-rating',
      prompt: 'How do you like the APPEARANCE of the cereal bar overall?',
      range: {
        min: 1,
        max: 9,
        labels: [
          'Very bad',
          'Bad',
          'Neither good nor bad',
          'Good',
          'Very good'
        ]
      }
    },
    {
      typeOfQuestion: 'vertical-rating',
      prompt: 'How do you like the FLAVOR of the cereal bar overall?',
      range: {
        min: 1,
        max: 9,
        labels: [
          'Very bad',
          'Bad',
          'Neither good nor bad',
          'Good',
          'Very good'
        ]
      }
    },
    {
      typeOfQuestion: 'vertical-rating',
      prompt: 'How do you like the TEXTURE of the cereal bar overall?',
      range: {
        min: 1,
        max: 9,
        labels: [
          'Very bad',
          'Bad',
          'Neither good nor bad',
          'Good',
          'Very good'
        ]
      }
    },
    {
      typeOfQuestion: 'info',
      prompt: 'What is your personal FLAVOR profile, just taste and create it!',
      secondaryPrompt: `
        In the following set of questions you will be asked to select from a pair of flavors. There will be multiple questions comparing pairs of flavors.
        Answer to the best of your ability. 
        There is no right or wrong answer.
      `,
      requiredQuestion: false
    },
    {
      typeOfQuestion: 'paired-questions',
      pairsOptions: {
        minPairs: 4,
        pairs: ['Caramelic', 'Vanilla', 'Roasted', 'Creamy', 'Bitter', 'Sweet']
      },
      prompt: 'Which FLAVOR is more intense?',
      secondaryPrompt: `These comparisons relate to the FLAVOR of the Cereal Bar.`,
      pairs: 'flavor'
    },
    {
      typeOfQuestion: 'info',
      prompt:
        'What is your personal TEXTURE profile, just Taste and create it!',
      secondaryPrompt: `
        In the following set of questions you will be asked to select from a pair of textures. There will be multiple questions comparing pairs of textures.
        Answer to the best of your ability. 
        There is no right or wrong answer.
      `,
      requiredQuestion: false
    },
    {
      typeOfQuestion: 'paired-questions',
      pairsOptions: {
        minPairs: 4,
        pairs: ['Crunchy', 'Moist', 'Grainy', 'Sticky', 'Chewy']
      },
      prompt: 'Which TEXTURE is more intense?',
      secondaryPrompt: `These comparisons relate to the TEXTURE of the Cereal Bar.`,
      pairs: 'texture'
    },
    {
      typeOfQuestion: 'open-answer',
      prompt: 'What other words come to mind when tasting this product?',
      secondaryPrompt: 'Write them bellow',
      requiredQuestion: false
    },
    {
      typeOfQuestion: 'profile',
      prompt: 'Your tasting profile',
      requiredQuestion: false
    },
    {
      typeOfQuestion: 'email',
      prompt: "What's your e-mail?",
      requiredQuestion: false
    }
  ],
  'middle'
)

const allQuestions = [...screeningQuestions, ...productQuestions]

const thankYouText = `Thank you very much for your participation.`

const products = [
  {
    photo: `https://s3-eu-west-1.amazonaws.com/flavorwiki-storage/flavorwiki-demo-product.png`,
    name: 'Quaker Yogurt Granola Bar-Vanilla',
    barcode: 'Barcode: Quaker Yogurt Granola Bar-Vanilla',
    brand: 'Nature Valley',
    reward: 10,
    isAvailable: 'true'
  }
]

const brands = [
  {
    logo:
      'http://www.sna.org.sg/wp-content/uploads/2014/09/Nature-Valley-Logo.png',
    name: 'Nature Valley'
  }
]

const survey = {
  name: 'Cereal Bar Survey - USA',
  title: 'Cereal Bar Survey - USA',
  instructionsText,
  thankYouText,
  state: 'active',
  uniqueName: 'vanilla-cereal-bar',
  authorizationType: 'public',
  surveyLanguage: 'en',
  customButtons: defaultButtons,
  instructionSteps: defaultInstructions,
  country: 'United States of America',
  settings: {
    recaptcha: false,
    emailService: false
  }
}

const seed = async ({ owner, creator }) => {
  sails.log('Creating Cereal Bar Survey...')

  sails.log('Creating Brands...')
  const createdBrands = await createSet(Brand, brands)
  const createdBrandsByName = createdBrands.reduce(
    (hashMap, brand) => Object.assign({}, hashMap, { [brand.name]: brand }),
    {}
  )

  sails.log('Creating Products...')
  const productsWithBrands = products.map(product =>
    Object.assign({}, product, { brand: createdBrandsByName[product.brand].id })
  )
  const createdProducts = await createSet(Product, productsWithBrands)
  const createdProductsId = createdProducts.map(product => product.id)

  sails.log('Creating Surveys...')
  const surveyWithProducts = Object.assign({}, survey)
  surveyWithProducts.products = createdProductsId
  surveyWithProducts.owner = owner
  surveyWithProducts.creator = creator
  surveyWithProducts.lastModifier = creator
  const createdSurveys = await createSet(Survey, [surveyWithProducts])
  const createdSurveyId = createdSurveys[0].id

  sails.log('Creating Questions...')
  const questionsWithSurvey = allQuestions.map(q =>
    Object.assign({}, q, { survey: createdSurveyId })
  )
  const createdQuestions = await createSet(Question, questionsWithSurvey)
  for (let i = 0; i < createdQuestions.length; i++) {
    const currentQuestion = createdQuestions[i]
    const nextQuestion = createdQuestions[i + 1] || { id: null }
    await Question.updateOne({ id: currentQuestion.id }).set({
      nextQuestion: nextQuestion.id
    })
  }

  sails.log('Creating Pairs...')
  const flavorPairQuestion = createdQuestions.find(
    question =>
      question.typeOfQuestion === 'paired-questions' &&
      question.prompt &&
      question.prompt.includes('FLAVOR')
  )
  const flavorPairsWithQuestions = flavorPairs.map(pair =>
    Object.assign({}, pair, { question: flavorPairQuestion.id })
  )
  await createSet(PairQuestion, flavorPairsWithQuestions)

  const texturePairQuestion = createdQuestions.find(
    question =>
      question.typeOfQuestion === 'paired-questions' &&
      question.prompt &&
      question.prompt.includes('TEXTURE')
  )
  const texturePairsWithQuestions = texturePairs.map(pair =>
    Object.assign({}, pair, { question: texturePairQuestion.id })
  )
  await createSet(PairQuestion, texturePairsWithQuestions)

  await generateProfileRelatedQuestions(
    createdQuestions.filter(q => q.type === 'paired-questions'),
    createdQuestions.filter(q => q.type === 'profile')
  )

  return createdSurveys[0]
}

module.exports = seed
