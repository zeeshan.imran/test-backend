/* globals Survey, User, sails */

module.exports = {
  friendlyName: 'Update fixtures',
  description: 'Updating Project Fixtures',

  fn: async function (inputs, exits) {
    sails.log('Running custom shell script... (`update-seed`)')

    const operator = await User.findOne({
      emailAddress: 'operator@flavorwiki.com'
    })

    await Survey.update({
      or: [{ creator: null }, { lastModifier: null }]
    }).set({
      creator: operator.id,
      lastModifier: operator.id
    })

    await Survey.find({
      or: [{ creator: null }, { lastModifier: null }]
    })

    return exits.success()
  }
}
