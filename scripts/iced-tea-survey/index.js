/* globals sails */
/* eslint-disable */
const defaultButtons = require('../../utils/defaultButtons')
const defaultInstructions = require('../../utils/defaultInstructions')

const {
  createSet,
  generatePairs,
  generateQuestionSection,
  generateProfileRelatedQuestions,
} = require('../generators')
/* eslint-enable */

const desiredFlavorPairs = generatePairs([
  'Bitter',
  'Sweet',
  'Fruity',
  'Tannic',
  'Acidic',
  'Peach',
  'Lemon'
])

const flavorPairs = generatePairs([
  'Bitter',
  'Sweet',
  'Fruity',
  'Tannic',
  'Acidic',
  'Peach',
  'Lemon'
])

const pairs = {
  flavor: flavorPairs,
  'desired-flavor': desiredFlavorPairs
}

const screeningQuestions = generateQuestionSection(
  [
    {
      typeOfQuestion: 'open-answer',
      prompt: 'Your Name',
      secondaryPrompt: 'First Name, Last Name'
    },
    {
      typeOfQuestion: 'email',
      prompt: 'Your E-Mail'
    },
    {
      typeOfQuestion: 'open-answer',
      prompt: 'What is your age?'
    },
    {
      typeOfQuestion: 'choose-one',
      prompt: 'What is your gender?',
      options: [
        { label: 'Female', value: 'female' },
        { label: 'Male', value: 'male' },
        { label: 'Other', value: 'other' }
      ]
    },
    {
      typeOfQuestion: 'open-answer',
      prompt: 'What is your occupation?'
    },
    {
      typeOfQuestion: 'choose-one',
      prompt: 'What is your living situation?',
      options: [
        { label: 'Live alone', value: 'live alone' },
        { label: 'With my partner', value: 'with my partner' },
        { label: 'With my family', value: 'with my family' },
        { label: 'With friends (WG)', value: 'with friends' }
      ]
    },
    {
      typeOfQuestion: 'select-and-justify',
      prompt: 'Do you have children?',
      options: [{ label: 'Yes', value: 'yes' }, { label: 'No', value: 'no' }],
      secondaryPrompt: 'If so, how many?'
    },
    {
      typeOfQuestion: 'choose-multiple',
      prompt: 'If you have any children, which are their ages?',
      secondaryPrompt: 'Choose all which apply',
      options: [
        { label: '0-5', value: '0-5' },
        { label: '5-15', value: '5-15' },
        { label: '15-25', value: '15-25' },
        { label: '25+', value: '25+' },
        { label: 'No children', value: 'none' }
      ],
      addCustomOption: true
    },
    {
      typeOfQuestion: 'choose-one',
      prompt: 'How often do you consume packaged beverages?',
      options: [
        { label: '2-3 times a day', value: '2-3 times a day' },
        { label: 'Daily', value: 'Daily' },
        { label: '2-3 times a week', value: '2-3 times a week' },
        { label: 'Once a week', value: 'once a week' },
        { label: 'Once every 2-3 weeks', value: 'once every 2-3 weeks' },
        { label: 'Once a month', value: 'once a month' },
        {
          label: 'Less often than once per month',
          value: 'less often than once per month'
        },
        { label: 'N/A', value: 'N/A' }
      ]
    }
  ],
  'screening'
)

const beginingQuestions = generateQuestionSection(
  [
    {
      typeOfQuestion: 'choose-product',
      prompt: 'Which of these products did you buy?',
      secondaryPrompt: "Or just choose the one you'll be tasting"
    }
  ],
  'begin'
)

const productQuestions = generateQuestionSection(
  [
    {
      typeOfQuestion: 'choose-one',
      prompt: 'How often do you consume this product?',
      options: [
        { label: '2-3 times a day', value: '2-3 times a day' },
        { label: 'Daily', value: 'Daily' },
        { label: '2-3 times a week', value: '2-3 times a week' },
        { label: 'Once a week', value: 'Once a week' },
        { label: 'Once every 2-3 weeks', value: 'Once every 2-3 weeks' },
        { label: 'Once a month', value: 'Once a month' },
        {
          label: 'Less often than once per month',
          value: 'Less often than once per month'
        },
        { label: 'N/A', value: 'N/A' }
      ]
    },
    {
      typeOfQuestion: 'choose-one',
      prompt: 'When do you typically consume this product?',
      secondaryPrompt: 'You may choose more than one,',
      options: [
        { label: 'In the morning', value: 'In the morning' },
        { label: 'Mid morning break', value: 'Mid morning break' },
        { label: 'Lunch', value: 'Lunch' },
        { label: 'Mid afternoon break', value: 'Mid afternoon break' },
        { label: 'Evening', value: 'Evening' },
        { label: 'After sport', value: 'After sport' },
        { label: 'While at work', value: 'While at work' }
      ]
    },
    {
      typeOfQuestion: 'vertical-rating',
      prompt:
        'Before tasting the product, on a scale of 1 to 9 ("Dislike Extremely" to "Like Extremely") how do you expect the product to taste based on the packaging?',
      range: {
        min: 1,
        max: 9,
        labels: [
          'Dislike Extremely',
          'Dislike Very Much',
          'Dislike Moderately',
          'Dislike Slightly',
          'Neither Like nor Dislike',
          'Like Slightly',
          'Like Moderately',
          'Like Very Much',
          'Like Extremely'
        ]
      }
    },
    {
      typeOfQuestion: 'choose-multiple',
      prompt: 'Describe how do you feel after drinking the product?',
      secondaryPrompt: 'You may choose more than one.',
      options: [
        { label: 'Refreshed', value: 'Refreshed' },
        {
          label: 'With a bad taste in my mouth',
          value: 'With a bad taste in my mouth'
        },
        { label: 'Heavy stomach feeling', value: 'Heavy stomach feeling' },
        {
          label: 'Like I want to drink more',
          value: 'Like I want to drink more'
        },
        { label: 'Guilty', value: 'Guilty' },
        { label: 'Happy', value: 'Happy' }
      ],
      addCustomOption: true
    },
    {
      typeOfQuestion: 'info',
      prompt:
        'What is your personal FLAVOR  profile, Just Taste and create it!',
      secondaryPrompt:
        'The next two questions will ask you about what you taste in the tea and what is your “ideal” tea flavor. There will be multiple questions comparing pairs of flavors. Choose the one that best answers the question. There is no right or wrong answer.',
      requiredQuestion: false
    },
    {
      typeOfQuestion: 'paired-questions',
      pairsOptions: {
        minPairs: 1,
        pairs: [
          'Bitter',
          'Sweet',
          'Fruity',
          'Tannic',
          'Acidic',
          'Peach',
          'Lemon'
        ]
      },
      prompt:
        'What Flavor is more intense in the product as you are tasting it?',
      pairsKey: 'flavor'
    },
    {
      typeOfQuestion: 'profile',
      prompt:
        'What Flavor is more intense in the product as you are tasting it?',
      requiredQuestion: false
    },
    {
      typeOfQuestion: 'paired-questions',
      pairsOptions: {
        minPairs: 1,
        pairs: [
          'Bitter',
          'Sweet',
          'Fruity',
          'Tannic',
          'Acidic',
          'Peach',
          'Lemon'
        ]
      },
      prompt:
        'Now create your perfect tea. Instead of telling us what you taste, tell us which flavor you would prefer to be more intense. What flavor would you like to have more intense in the product?',
      pairsKey: 'desired-flavor'
    },
    {
      typeOfQuestion: 'profile',
      prompt:
        'What Flavor would you like to have more intense in this product?',
      requiredQuestion: false
    },
    {
      typeOfQuestion: 'choose-multiple',
      prompt:
        'Are there other flavors you would like to see in a personal Iced Tea?',
      secondaryPrompt: 'Choose or name what you like.',
      options: [
        { label: 'Mango', value: 'mango' },
        { label: 'Tea', value: 'tea' }
      ],
      addCustomOption: true
    },
    {
      typeOfQuestion: 'vertical-rating',
      prompt: 'How much did you enjoyed creating your Personal Flavor Profile?',
      range: {
        min: 1,
        max: 9,
        labels: [
          'Dislike Extremely',
          'Dislike Very Much',
          'Dislike Moderately',
          'Dislike Slightly',
          'Neither Like nor Dislike',
          'Like Slightly',
          'Like Moderately',
          'Like Very Much',
          'Like Extremely'
        ]
      }
    }
  ],
  'middle'
)

const finishingQuestions = generateQuestionSection(
  [
    {
      typeOfQuestion: 'select-and-justify',
      prompt:
        'Would you like to create a personal flavor profile on regular base and while taking part in a personalized Beverage development?',
      options: [{ label: 'Yes', value: 'yes' }, { label: 'No', value: 'no' }],
      secondaryPrompt: 'Why?'
    },
    {
      typeOfQuestion: 'open-answer',
      prompt:
        'What means convenience & personalization for you considering drinks?'
    },
    {
      typeOfQuestion: 'choose-multiple',
      prompt:
        'Which of the following drinks personalization related services would interest you?',
      secondaryPrompt: 'You may choose more than one.',
      options: [
        {
          label:
            'Personal digital “hydration profile” that helps me find drinks right for my taste.',
          value:
            'Personal digital “hydration profile” that helps me find drinks right for my taste.'
        },
        {
          label:
            'Personal digital “hydration profile” that helps me find drinks right for my health and hydration needs.',
          value:
            'Personal digital “hydration profile” that helps me find drinks right for my health and hydration needs.'
        },
        {
          label:
            'Personal digital “hydration profile” that helps me create drinks right for my taste.',
          value:
            'Personal digital “hydration profile” that helps me create drinks right for my taste.'
        },
        {
          label:
            'Personal digital “hydration profile” that helps me create drinks right for my health and hydration needs.',
          value:
            'Personal digital “hydration profile” that helps me create drinks right for my health and hydration needs.'
        },
        {
          label:
            'Personal digital “hydration profile” that monitors how I feel and helps me improve energy and well being through good hydration.',
          value:
            'Personal digital “hydration profile” that monitors how I feel and helps me improve energy and well being through good hydration.'
        }
      ]
    },
    {
      typeOfQuestion: 'open-answer',
      prompt:
        'What other types of personalization for drinks would interest you? (please share your ideas)'
    },
    {
      typeOfQuestion: 'choose-one',
      prompt:
        'Would you pay for the drinks personalization features that interest you?',
      secondaryPrompt: 'Choose one option',
      options: [
        { label: 'Yes, I would pay extra.', value: 'Yes, I would pay extra.' },
        {
          label: 'I would only pay as part of a product offer.',
          value: 'I would only pay as part of a product offer.'
        },
        {
          label: 'I would not pay anything at all.',
          value: 'I would not pay anything at all.'
        }
      ]
    },
    {
      typeOfQuestion: 'select-and-justify',
      prompt: 'Would you consider Personal Beverage deliveries?',
      options: [{ label: 'Yes', value: 'yes' }, { label: 'No', value: 'no' }],
      secondaryPrompt: 'Why?'
    },
    {
      typeOfQuestion: 'choose-multiple',
      prompt: 'What delivery method of drinks would interest you most?',
      secondaryPrompt: 'You may choose more than one.',
      options: [
        { label: 'Home delivery', value: 'Home delivery' },
        { label: 'Office delivery', value: 'Office delivery' },
        { label: 'Shop', value: 'Shop' },
        {
          label: 'School /kindergarten  delivery ',
          value: 'School /kindergarten  delivery '
        },
        { label: 'Ecommerce', value: 'Ecommerce' },
        { label: 'In restaurant', value: 'In restaurant' },
        { label: 'None', value: 'None' }
      ]
    },
    {
      typeOfQuestion: 'choose-one',
      prompt: 'How often would you like delivery?',
      secondaryPrompt: 'Choose one option',
      options: [
        { label: 'Daily', value: 'Daily' },
        { label: 'Twice weekly', value: 'Twice weekly' },
        { label: 'Weekly', value: 'Weekly' },
        { label: 'Monthly', value: 'Monthly' },
        { label: 'Every other Month', value: 'Every other Month' },
        { label: 'Never', value: 'Never' }
      ]
    },
    {
      typeOfQuestion: 'choose-multiple',
      prompt:
        'Which of the following personalized forms of beverage delivery related services would interest you?',
      secondaryPrompt: 'You may choose more than one.',
      options: [
        {
          label:
            'Beverage delivery service that supports my personal hydration needs and preferences.',
          value:
            'Beverage delivery service that supports my personal hydration needs and preferences.'
        },
        {
          label:
            'Alerts that remind me what types of drinks support my daily hydration needs and preferences, when to consume them and where to find them.',
          value:
            'Alerts that remind me what types of drinks support my daily hydration needs and preferences, when to consume them and where to find them.'
        },
        {
          label:
            'Combination of delivery and alerts to support my daily hydration needs.',
          value:
            'Combination of delivery and alerts to support my daily hydration needs.'
        },
        {
          label: 'The above features, but for my family / children.',
          value: 'The above features, but for my family / children.'
        }
      ]
    },
    {
      typeOfQuestion: 'choose-one',
      prompt:
        'How would some form of drinks personalization affect your interest in a drinks delivery service?',
      secondaryPrompt: 'Choose one option',
      options: [
        { label: 'Much more interested', value: 'Much more interested' },
        {
          label: 'Slightly more interested',
          value: 'Slightly more interested'
        },
        {
          label: 'More interested, but only if it did not cost extra',
          value: 'More interested, but only if it did not cost extra'
        },
        {
          label: 'Neither more or less interested',
          value: 'Neither more or less interested'
        },
        { label: 'Less interested', value: 'Less interested' }
      ]
    },
    {
      typeOfQuestion: 'upload-picture',
      prompt:
        'Please take a photo of the product you just tried to verify your participation.'
    }
  ],
  'end'
)

const allQuestions = [
  ...screeningQuestions,
  ...beginingQuestions,
  ...productQuestions,
  ...finishingQuestions
]

const products = [
  {
    photo: 'https://www.coopathome.ch/img/produkte/300_300/RGB/3513841_001.jpg',
    name: 'Lipton Iced Tea',
    barcode: 'a-bar-code-here',
    reward: 10,
    brand: 'Lipton',
    isAvailable: 'true'
  },
  {
    photo: 'https://www.coopathome.ch/img/produkte/300_300/RGB/3050213_001.jpg',
    name: 'Coca-Cola',
    barcode: 'another-bar-code-here',
    reward: 5,
    brand: 'Coca-Cola',
    isAvailable: 'false'
  },
  {
    photo: 'https://www.coopathome.ch/img/produkte/300_300/RGB/3050213_001.jpg',
    name: 'Product 1',
    barcode: 'another-bar-code-here',
    reward: 5,
    brand: 'brand',
    isAvailable: 'true'
  },
  {
    photo: 'https://www.coopathome.ch/img/produkte/300_300/RGB/3050213_001.jpg',
    name: 'Product 2',
    barcode: 'another-bar-code-here',
    reward: 5,
    brand: 'brand',
    isAvailable: 'true'
  }
]

const brands = [
  {
    logo:
      'https://www.conspiracies.net/wp-content/uploads/2017/01/Coca-Cola-Logo-300x300.png',
    name: 'Coca-Cola'
  },
  {
    logo:
      'https://upload.wikimedia.org/wikipedia/en/f/fc/LIPTON_PRIMARY_RGB_BMT.png',
    name: 'Lipton'
  },
  {
    logo:
      'https://upload.wikimedia.org/wikipedia/en/f/fc/LIPTON_PRIMARY_RGB_BMT.png',
    name: 'brand'
  }
]

const survey = {
  name: 'Drinks study - Ice Tea Switzerland',
  title: 'Drinks study - Ice Tea Switzerland',
  instructionsText: '',
  thankYouText: '',
  state: 'active',
  uniqueName: 'iced-tea-switzerland',
  surveyLanguage: 'en',
  authorizationType: 'public',
  referralReward: 10,
  referralRewardCurrency: 'CHF',
  customButtons: defaultButtons,
  instructionSteps: defaultInstructions,
  country: 'Germany',
  settings: {
    recaptcha: true,
    emailService: true
  }
}

const seed = async ({ owner, creator }) => {
  sails.log('Creating Iced Tea Survey...')

  sails.log('Creating Brands...')
  const createdBrands = await createSet(Brand, brands)
  const createdBrandsByName = createdBrands.reduce(
    (hashMap, brand) => Object.assign({}, hashMap, { [brand.name]: brand }),
    {}
  )

  sails.log('Creating Products...')
  const productsWithBrands = products.map(product =>
    Object.assign({}, product, { brand: createdBrandsByName[product.brand].id })
  )
  const createdProducts = await createSet(Product, productsWithBrands)
  const createdProductsId = createdProducts.map(product => product.id)

  sails.log('Creating Surveys...')
  const surveyWithProducts = Object.assign({}, survey)
  surveyWithProducts.products = createdProductsId
  surveyWithProducts.owner = owner
  surveyWithProducts.creator = creator
  surveyWithProducts.lastModifier = creator
  const createdSurveys = await createSet(Survey, [surveyWithProducts])
  const createdSurveyId = createdSurveys[0].id

  sails.log('Creating Questions...')
  const questionsWithSurvey = allQuestions.map(q =>
    Object.assign({}, q, { survey: createdSurveyId })
  )
  const createdQuestions = await createSet(Question, questionsWithSurvey)
  for (let i = 0; i < createdQuestions.length; i++) {
    const currentQuestion = createdQuestions[i]
    const nextQuestion = createdQuestions[i + 1] || { id: null }
    await Question.updateOne({ id: currentQuestion.id }).set({
      nextQuestion: nextQuestion.id
    })
  }

  sails.log('Creating Pairs...')
  const pairedQuestions = createdQuestions.filter(
    question =>
      question.typeOfQuestion === 'paired-questions' && question.pairsKey
  )

  for (question in pairedQuestions) {
    const targetPairs = pairs[question.pairsKey]
    const pairsWithQuestion = targetPairs.map(pair =>
      Object.assign({}, pair, { question: question.id })
    )

    await createSet(PairQuestion, flavorPairsWithQuestions)
  }

  await generateProfileRelatedQuestions(
    createdQuestions.filter(q => q.type === 'paired-questions'),
    createdQuestions.filter(q => q.type === 'profile')
  )
}

module.exports = seed
