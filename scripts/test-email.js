require('../config/env/env')
const emailService = require('../api/services/emailService')

async function sendTestEmail () {
  await emailService.sendEmail({
    receiver: 'dragonnpoulp@gmail.com',
    subject: 'test email template',
    html: await emailService.buildEmailBody(
      `
      <div class="align-center">
        <img class="full-width" src="cid:success" />
      </div>
      <div class="text">Thank you for participating in the tasting with FlavorWiki.</div>
      <div class="text">We will be paying you within 5 business days once your photos are validated.</div>
      <div class="text">If you enjoyed the tasting, please recommend us on our Facebook Page or share this tasting with your friends.</div>
      <div class="btn-group center" style="margin-bottom: 60px;">
        <a class="btn btn-share facebook" href="http://www.facebook.com/share.php?u=__SHARE_LINK__">
          Share on Facebook
        </a>

        <a class="btn btn-share twitter"
          href="https://twitter.com/intent/tweet?text=Just%20finished%20a%20survey%2C%20check%20out&via=FlavorWiki&url=__SHARE_LINK__">
          Share on Twitter
        </a>

        <a class="btn btn-share linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=__SHARE_LINK__">
          Share on Linkedin
        </a>
        <div class="text">Help spread the word!</div>
      </div>
      <div class="happyTasting">Happy Tasting!</div>
      <div class="team">The FlavorWiki Team</div>`
    )
  })
}

sendTestEmail()
